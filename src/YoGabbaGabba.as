package
{
	import com.cupcake.App;
	import com.cupcake.DeviceInfo;
	import com.greensock.TweenMax;
	
	import flash.desktop.NativeApplication;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display3D.Context3DProfile;
	import flash.display3D.Context3DRenderMode;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.geom.Rectangle;
	import flash.net.SharedObject;
	import flash.system.Capabilities;
	
	import starling.core.Starling;
	import starling.events.Event;
	import starling.extensions.SoundManager;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.RectangleUtil;
	import starling.utils.ScaleMode;
    
    [SWF(frameRate="60", backgroundColor="#000000")]
	
    public class YoGabbaGabba extends Sprite
    {
        // Startup image
        [Embed(source="/startup.png")]
        private static var Background:Class;
		
		[Embed(source="/startupHD.png")]
		private static var BackgroundHD:Class;
		
		[Embed( source   = "../assets/fonts/BURBANK BIG REGULAR.OTF",     
  			  fontFamily  = "BURBANK BIG REGULAR",     
  			  embedAsCFF ="false") ]
		public static var FONT_BURBANK_BIG_REGULAR:Class;
		
		[Embed( source   = "../assets/fonts/Latino-Rumba.otf",     
  			  fontFamily  = "Latino_Rumba",     
  			  embedAsCFF ="false") ]
		public static var FONT_Latino_Rumba:Class;
		
		[Embed(source = "../assets/fonts/Coop Flaired.otf",
			fontName = "Coop_Flaired",
			fontFamily="Coop_Flaired",
			mimeType = "application/x-font",
			advancedAntiAliasing = "true",
			embedAsCFF="false")]
		public static const Coop_Flaired:Class;
        
        private var mStarling:Starling;
		private var mStarlingProfile:String = Context3DProfile.BASELINE;
		private var soundManager:SoundManager;
		
		private static var flickFix:Boolean = false;
        
		public static var savedVariablesObject:SharedObject;
		
        public function YoGabbaGabba()
        {
			App.Setup(this);
            // set general properties
			var retinaTest:Boolean = false;
			
			if(retinaTest || ( Capabilities.screenResolutionX == 1536 && Capabilities.screenResolutionY == 2048 && CONFIG::MARKET == "itunes" ) ){
				Costanza.IPAD_RETINA = true;
				Costanza.STAGE_HEIGHT = 768;
				Costanza.STAGE_WIDTH = 1024;
				Costanza.SCALE_FACTOR = 2;
				Costanza.STAGE_OFFSET = -171;
				mStarlingProfile = Context3DProfile.BASELINE_EXTENDED;
			}
            var stageWidth:int   = Costanza.STAGE_WIDTH;
            var stageHeight:int  = Costanza.STAGE_HEIGHT;

            var iOS:Boolean = Capabilities.manufacturer.indexOf("iOS") != -1;

            Starling.multitouchEnabled = true;  // useful on mobile devices
            Starling.handleLostContext = true;  // not necessary on iOS. Saves a lot of memory!

			//shared Object with stored variables
			savedVariablesObject = SharedObject.getLocal("YoGabbaGabbaSaveData");
			setSharedData();
       
            // create a suitable viewport for the screen size
            // 
            // we develop the game in a *fixed* coordinate system of 1366x768; the game might 
            // then run on a device with a different resolution; for that case, we zoom the 
            // viewPort to the optimal size for any display and load the optimal textures.
            
			stage.align = StageAlign.TOP;
			
			Costanza.VIEWPORT_WIDTH = stage.fullScreenWidth;
			Costanza.VIEWPORT_HEIGHT 	= stage.fullScreenHeight - ( CONFIG::MARKET == "nabi" ? Costanza.NABI_OFFSET : 0 );
			Costanza.VIEWPORT_OFFSET = (Costanza.STAGE_WIDTH - Costanza.VIEWPORT_WIDTH)/2;
			
            var viewPort:Rectangle = RectangleUtil.fit(
                new Rectangle(0, 0, stageWidth, stageHeight), 
                new Rectangle(0, 0, Costanza.VIEWPORT_WIDTH, Costanza.VIEWPORT_HEIGHT), 
                ScaleMode.NO_BORDER);
			
			var appDir:File = File.applicationDirectory;
			
			if ( CONFIG::MARKET == "nabi" ) { viewPort.top = 0; }
			Starling.handleLostContext = !iOS;  // not necessary on iOS. Saves a lot of memory!
			
			//check resolution to see if we need mipmapping enabled
			if(stage.fullScreenHeight < 384)//if below half our default size of 768
			{
				Costanza.mipmapsEnabled = true;
			}
			
			DeviceInfo.Init();
			
            // create the AssetManager, which handles all required assets for this resolution
            var assets:AssetManager = new AssetManager(Costanza.SCALE_FACTOR);
            
			assets.useMipMaps = Costanza.mipmapsEnabled;
            assets.verbose = Capabilities.isDebugger;
            assets.enqueue(
				appDir.resolvePath("textures/"+Costanza.SCALE_FACTOR +"x/promo"),
				appDir.resolvePath("textures/"+Costanza.SCALE_FACTOR +"x/Global"),
				appDir.resolvePath("textures/"+Costanza.SCALE_FACTOR +"x/Bubbles"),
				appDir.resolvePath("audio/music"),
                appDir.resolvePath("fonts")
            );
			
            // While Stage3D is initializing, the screen will be blank. To avoid any flickering, 
            // we display a startup image now and remove it below, when Starling is ready to go.
            // This is especially useful on iOS, where "Default.png" (or a variant) is displayed
            // during Startup. You can create an absolute seamless startup that way.
            // 
            // These are the only embedded graphics in this app. We can't load them from disk,
            // because that can only be done asynchronously (resulting in a short flicker).
            // 
            // Note that we cannot embed "Default.png" (or its siblings), because any embedded
            // files will vanish from the application package, and those are picked up by the OS!
            
            var backgroundClass:Class = Costanza.SCALE_FACTOR == 1 ? Background : BackgroundHD;
			var background:Bitmap = new backgroundClass();
            BackgroundHD = null; // no longer needed!
			Background = null; // no longer needed!
			 
            background.x = viewPort.x;
            background.y = viewPort.y;
            background.width  = viewPort.width;
			background.height = viewPort.height;
            background.smoothing = true;
            addChild(background);
			
            // launch Starling
            mStarling = new Starling(Root, stage, viewPort,null,Context3DRenderMode.AUTO, mStarlingProfile);
			mStarling.stage.stageWidth  = stageWidth;  // <- same size on all devices!
            mStarling.stage.stageHeight = stageHeight; // <- same size on all devices!
            mStarling.simulateMultitouch = false;
            //mStarling.enableErrorChecking = Capabilities.isDebugger;
			//mStarling.showStatsAt("center", "bottom");

			
            mStarling.addEventListener(starling.events.Event.ROOT_CREATED, 
                function onRootCreated(event:Object, app:Root):void
                {
                    mStarling.removeEventListener(starling.events.Event.ROOT_CREATED, onRootCreated);
                    removeChild(background);
                    background = null;
                    
                    var bgTexture:Texture = Texture.fromEmbeddedAsset(backgroundClass, false, false, Costanza.SCALE_FACTOR);
                    app.start(bgTexture, assets);
                    mStarling.start();
                });
            
            // When the game becomes inactive, we pause Starling; otherwise, the enter frame event
            // would report a very long 'passedTime' when the app is reactivated. 
            
			soundManager = SoundManager.getInstance();
			
			NativeApplication.nativeApplication.addEventListener( flash.events.Event.ACTIVATE, WindowActivated ); 
			NativeApplication.nativeApplication.addEventListener( flash.events.Event.DEACTIVATE, WindowDeactivated );
			
			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_ANDROID && CONFIG::MARKET != "nabi" ) 
				{ stage.addEventListener( flash.events.Event.RESIZE, StageResized ); }
			
			/*CONFIG::ANDROID {
				if ( DeviceInfo.platform == DeviceInfo.PLATFORM_ANDROID ) { FlickbackConnect.init(); }
				TweenMax.delayedCall( 2, ResetFlickFix );
			}*/
        }
		
		private function WindowActivated( e:flash.events.Event ):void
		{ 
			Starling.current.start();
			soundManager.muteAll(false); 
			TweenMax.resumeAll(); 
			trace( "Window activated" );
			
			/*CONFIG::ANDROID
			{
				if ( flickFix && DeviceInfo.platform == DeviceInfo.PLATFORM_ANDROID )
				{
					flickFix = false;
					FlickbackConnect.showFlickScreen();
					TweenMax.delayedCall( 2, ResetFlickFix );
				}
			}*/
		}
		
		private function WindowDeactivated( e:flash.events.Event ):void
		{ 
			Starling.current.stop(true);
			soundManager.muteAll(true); 
			TweenMax.pauseAll();
			trace( "Window deactivated" );
		}
		
		//private static function ResetFlickFix():void { flickFix = true; }
		
		private function StageResized( e:flash.events.Event ):void
		{
			Costanza.VIEWPORT_WIDTH = stage.stageWidth;
			Costanza.VIEWPORT_HEIGHT = stage.stageHeight;
			Costanza.VIEWPORT_OFFSET = (Costanza.STAGE_WIDTH - Costanza.VIEWPORT_WIDTH)/2;
			
			var viewPort:Rectangle = RectangleUtil.fit(
				new Rectangle(0, 0, Costanza.STAGE_WIDTH, Costanza.STAGE_HEIGHT), 
				new Rectangle(0, 0, Costanza.VIEWPORT_WIDTH , Costanza.VIEWPORT_HEIGHT), 
				ScaleMode.NO_BORDER);
			
			Starling.current.viewPort = viewPort;
			
			// only do this once on nook
			if ( CONFIG::MARKET == "nook" ) { stage.removeEventListener( flash.events.Event.RESIZE, StageResized ); }
		}
		
		private function setSharedData():void
		{
			//set variables from saved data object
			//savedVariablesObject.clear();
			
			//if first time and saved data is undefined, set data, else set vars based on saved data
			
			//VOLUME
			(savedVariablesObject.data.musicVolume >= 0) ? Costanza.musicVolume = savedVariablesObject.data.musicVolume : savedVariablesObject.data.musicVolume = Costanza.musicVolume;
			(savedVariablesObject.data.voiceVolume >= 0) ? Costanza.voiceVolume = savedVariablesObject.data.voiceVolume : savedVariablesObject.data.voiceVolume = Costanza.voiceVolume;
			(savedVariablesObject.data.soundVolume >= 0) ? Costanza.soundVolume = savedVariablesObject.data.soundVolume : savedVariablesObject.data.soundVolume = Costanza.soundVolume;
			
			(savedVariablesObject.data.FOODS) ? Costanza.FOODS = savedVariablesObject.data.FOODS : savedVariablesObject.data.FOODS = Costanza.FOODS;
			(savedVariablesObject.data.characterPlaythroughs) ? Costanza.characterPlaythroughs = savedVariablesObject.data.characterPlaythroughs : savedVariablesObject.data.characterPlaythroughs = Costanza.characterPlaythroughs;

			savedVariablesObject.flush();
			
		}
		
		public static function getAppVersion():String 
		{
			var appXml:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = appXml.namespace();
			var appVersion:String = appXml.ns::versionNumber[0];
			return appVersion;
		}
    }
}