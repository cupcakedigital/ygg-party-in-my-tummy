package
{ 
    import starling.errors.AbstractClassError;

    public class Costanza
    {
        public function Costanza() { throw new AbstractClassError(); }
        
		public static var IPAD_RETINA:Boolean = false;
		public static var STAGE_WIDTH:int  = 1366;
		public static var STAGE_HEIGHT:int = 768;
		public static var STAGE_OFFSET:int = 0;
		public static var SCALE_FACTOR:int = 1;
		
		public static var VIEWPORT_WIDTH:int  = 1024;
		public static var VIEWPORT_HEIGHT:int = 768;
		public static var VIEWPORT_OFFSET:int = 171;
		
		public static const NABI_OFFSET:int	= 23; // 30
		
		public static var mipmapsEnabled:Boolean = false;
		
		public static var FOODS:Array = ["Sandwich","Yogurt","Noodles","Pizza","Sushi","Salad","Fruitsalad"];
		public static var FOODS_BACKUP:Array = ["Sandwich","Yogurt","Noodles","Pizza","Sushi","Salad","Fruitsalad"];
		
		public static var characterPlaythroughs:Array = [0,0,0,0,0];
		
		public static var Tutorial_Completed:Boolean = true;
		
		public static var musicVolume:Number = .15;
		public static var voiceVolume:Number = .75;
		public static var soundVolume:Number = .75;
			
		public static var overallPlays:int = 0;
		
		public static var  StageProportionScale:Number;

		public static const VIDEO_FILES:String			= "Cupcake_Splash";
		public static const VIDEO_WIDTHS:String			= "1366";
    }
}