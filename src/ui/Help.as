package ui
{
	import flash.filesystem.File;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.system.Capabilities;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	import starling.utils.AssetManager;
	
	import utils.Market;

	public class Help extends Sprite
	{
		private var helpAssets:AssetManager;
		private var helpAppDir:File = File.applicationDirectory;
		private var soundmanager:SoundManager;
		
		private var HelpImg:Image;
		private var CreditsImg:Image;
		
		public function Help()
		{
			helpAssets = new AssetManager(Costanza.SCALE_FACTOR);
			Root.currentAssetsProxy = helpAssets;
			helpAssets.verbose = Capabilities.isDebugger;
			
			helpAssets.useMipMaps = Costanza.mipmapsEnabled;
			helpAssets.enqueue(
				helpAppDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Help/Credits.jpg"),
				helpAppDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Help/Help.png")
			);
			
			//dispatchEventWith(HIDE_COMIC,true);
			
			helpAssets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1){
					addObjects();
				};
			});
		}
		
		private function addObjects():void
		{
			HelpImg = new Image(helpAssets.getTexture("Help"));
			this.addChild(HelpImg);

			CreditsImg = new Image(helpAssets.getTexture("Credits"));
			this.addChild(CreditsImg);
			CreditsImg.visible = false;
			CreditsImg.touchable = false;
			
			var closeBTN:Quad = new Quad(100,100);
			closeBTN.alpha = 0;
			closeBTN.x = 1080 + Costanza.STAGE_OFFSET;
			closeBTN.y = HelpImg.y;
			this.addChild(closeBTN);
			closeBTN.addEventListener(TouchEvent.TOUCH,closeTap);
			
			var creditsBTN:Quad = new Quad(100,100, 0xff0000);
			creditsBTN.alpha = 0;
			creditsBTN.x = 485 + Costanza.STAGE_OFFSET;
			creditsBTN.y = 80;
			this.addChild(creditsBTN);
			creditsBTN.addEventListener(TouchEvent.TOUCH, showCredits);
			
			var rateBTN:Quad = new Quad(100,100, 0xff0000);
			rateBTN.alpha = 0;
			rateBTN.x = 595 + Costanza.STAGE_OFFSET;
			rateBTN.y = 80;
			rateBTN.name = "Rate";
			this.addChild(rateBTN);
			rateBTN.addEventListener(TouchEvent.TOUCH, tapButton);
			
			var feedbackBTN:Quad = new Quad(100,100, 0xff0000);
			feedbackBTN.alpha = 0;
			feedbackBTN.x = 705 + Costanza.STAGE_OFFSET;
			feedbackBTN.y = 80;
			feedbackBTN.name = "Feedback";
			this.addChild(feedbackBTN);
			feedbackBTN.addEventListener(TouchEvent.TOUCH, tapButton);
			
			var facebookBTN:Quad = new Quad(100,100, 0xff0000);
			facebookBTN.alpha = 0;
			facebookBTN.x = 825 + Costanza.STAGE_OFFSET;
			facebookBTN.y = 80;
			facebookBTN.name = "Facebook";
			this.addChild(facebookBTN);
			facebookBTN.addEventListener(TouchEvent.TOUCH, tapButton);
			
			var twitterBTN:Quad = new Quad(100,100, 0xff0000);
			twitterBTN.alpha = 0;
			twitterBTN.x = 950 + Costanza.STAGE_OFFSET;
			twitterBTN.y = 80;
			twitterBTN.name = "Twitter";
			this.addChild(twitterBTN);
			twitterBTN.addEventListener(TouchEvent.TOUCH, tapButton);
			
			/*var closeBTN:Quad = new Quad(100,100);
			//closeBTN.alpha = 0;
			closeBTN.x = 1080;
			closeBTN.y = HelpImg.y;
			this.addChild(closeBTN);
			closeBTN.addEventListener(TouchEvent.TOUCH,closeTap);
			
			var closeBTN:Quad = new Quad(100,100);
			//closeBTN.alpha = 0;
			closeBTN.x = 1080;
			closeBTN.y = HelpImg.y;
			this.addChild(closeBTN);
			closeBTN.addEventListener(TouchEvent.TOUCH,closeTap);*/
			
			/*skip = new Button(comicAssets.getTexture("Back_1"),"",comicAssets.getTexture("Back_2"));
			skip.x = 1000;
			skip.y = 660;
			skip.touchable = skip.visible = showskip;
			this.addChild(skip);
			skip.addEventListener(Event.TRIGGERED,skipComic);*/
		}
		
		private function tapButton(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				switch((e.currentTarget as Quad).name)
				{
					case "Rate":
					{
						Market.Open();
						break;
					}
					case "Feedback":
					{
						LaunchURL( "http://www.cupcakedigital.com/app-help/" );
						break;
					}
					case "Facebook":
					{
						LaunchURL( "https://www.facebook.com/cupcakedigital" );
						break;
					}
					case "Twitter":
					{
						LaunchURL( "https://twitter.com/cupcake_digital" );
						break;
					}
				}
			}
		}
		
		private function LaunchURL( url:String ):void { navigateToURL( new URLRequest( url ) ); }
		
		private function showCredits(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				CreditsImg.visible = true;
				CreditsImg.touchable = true;
			}
		}
		
		private function closeTap(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				if(CreditsImg.visible)
				{
					CreditsImg.visible = false;
					CreditsImg.touchable = false;
				}
				else
				{
					//goBack
					destroy();
				}
			}
		}
		
		public function destroy():void
		{
			trace("destroy");
			helpAssets.dispose();
			//soundManager.removeSound("WidgetTheme");
			this.removeChildren(0,this.numChildren-1,true);
			this.parent.removeChild(this,true);
		}
	}
}