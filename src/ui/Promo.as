package ui
{
	import flash.filesystem.File;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.system.Capabilities;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	import starling.utils.AssetManager;
	
	public class Promo extends Sprite
	{
		private var promoAssets:AssetManager;
		private var promoAppDir:File = File.applicationDirectory;
		private var soundmanager:SoundManager;
		
		private var promoImg:Image;
		private var CreditsImg:Image;
		
		public function Promo()
		{
			promoAssets = new AssetManager();
			Root.currentAssetsProxy = promoAssets;
			promoAssets.verbose = Capabilities.isDebugger;
			
			promoAssets.enqueue(
				promoAppDir.resolvePath("textures/Help/PromoDvd.png")
			);
			
			promoAssets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1){
					addObjects();
				};
			});
		}
		
		private function addObjects():void
		{
			promoImg = new Image(promoAssets.getTexture("PromoDvd"));
			this.addChild(promoImg);
			
			var buyBTN:Quad = new Quad(400,200);
			buyBTN.alpha = 0;
			buyBTN.x = 700;
			buyBTN.y = 500;
			this.addChild(buyBTN);
			buyBTN.addEventListener(TouchEvent.TOUCH, buyNow);
			
			var closeBTN:Quad = new Quad(150,150);
			closeBTN.alpha = 0;
			closeBTN.x = 1020;
			closeBTN.y = promoImg.y;
			this.addChild(closeBTN);
			closeBTN.addEventListener(TouchEvent.TOUCH,closeTap);

		}
		
		private function buyNow(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				LaunchURL("http://store.veggietales.com/merry-larry-the-true-light-of-christmas-dvd.html");
			}
		}
		
		private function closeTap(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				//goBack
				destroy();
			}
		}
		
		private function LaunchURL( url:String ):void { navigateToURL( new URLRequest( url ) ); }
		
		public function destroy():void
		{
			promoAssets.dispose();
			//soundManager.removeSound("WidgetTheme");
			this.removeChildren(0,this.numChildren-1,true);
			this.parent.removeChild(this,true);
		}
	}
}