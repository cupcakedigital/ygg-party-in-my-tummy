package ui
{
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.extensions.PDParticleSystem;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.utils.Color;

	public class RoundName extends Sprite
	{
		private var levelText:TextField;
		
		private var particles:PDParticleSystem;
		
		public function RoundName(text:String="")
		{
			particles = new PDParticleSystem(Root.assets.getXml("NameParticle"), Root.assets.getTexture("NameParticle"));
			this.addChild(particles);
			Starling.juggler.add(particles);
			
			levelText = new TextField(800,150, text, "VeggieFont");
			levelText.color = Color.WHITE;
			levelText.fontSize = BitmapFont.NATIVE_SIZE;
			this.addChild(levelText);
			
			particles.x = levelText.width/2;
			particles.y = 50;
		}
		
		public function start():void
		{
			particles.start();
		}
		public function stop(clear:Boolean=true):void
		{
			particles.stop(clear);
		}
		public function setText(str:String):void
		{
			levelText.text = str;
		}
		public function destroy():void
		{
			stop();
			this.removeChild(levelText,true);
			this.removeChild(particles,true);
			this.parent.removeChild(this,true);
		}
	}
}