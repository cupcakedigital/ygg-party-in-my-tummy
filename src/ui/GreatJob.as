package ui
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Sprite;
	import starling.extensions.SoundManager;

	public class GreatJob extends Sprite
	{
		private var bg:Image;
		private var button:Button;
		
		private var larryPop:MovieClip;
		
		public function GreatJob()
		{
			var sm:SoundManager = SoundManager.getInstance();
			
			bg = new Image(Root.assets.getTexture("EndOfLevel_Background"));
			
			addChild(bg);
			
			button = new Button(Root.assets.getTexture("EndOfLevel_Arrow"));
			button.x = bg.x + bg.width - 40;
			button.y = bg.y + bg.height/2;
			
			addChild(button);
			
			larryPop = new MovieClip(Root.assets.getTextures("Larry_Pop_"));
			larryPop.pivotX = larryPop.width/2;
			larryPop.pivotY = larryPop.height/2;
			larryPop.x = bg.x + 50;
			larryPop.y = bg.y + bg.height + 100;
			
			addChild(larryPop);
			
			sm.playSound("End",Costanza.soundVolume);
			sm.playSound("LarryGreatJob",Costanza.soundVolume);
			tapVeg(1);
		}
		
		private function tapVeg(frame:int):void
		{
			TweenMax.to(larryPop,0.2,{scaleY:0.95,scaleX:1.05,onComplete:popBack,onCompleteParams:[frame],ease:Linear.easeInOut});
		}
		
		private function popBack(frame:int):void
		{
			TweenMax.to(larryPop,0.15,{scaleY:1.05,scaleX:.95,onComplete:continuePop,onCompleteParams:[frame], ease:Linear.easeInOut});
		}
		
		private function continuePop(frame:int):void
		{
			TweenMax.to(larryPop,0.1,{scaleY:1,scaleX:1, ease:Linear.easeNone});
			larryPop.currentFrame = frame;
			
			if(frame == 0){frame=1;}
			else{frame=0}
			
			TweenMax.delayedCall(.5,tapVeg,[frame]);
		}
		
		public function destroy():void
		{
			TweenMax.killTweensOf(larryPop);
			this.parent.removeChild(this,true);
		}
		
	}
}