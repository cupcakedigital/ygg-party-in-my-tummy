package ui
{
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.BitmapFont;
	import starling.text.TextField;
	import starling.utils.Color;

	public class Settings extends Sprite
	{
		public static const HIDE_SETTINGS:String = "hideSettings";
		
		private var musicSlider:Image;
		private var soundSlider:Image;
		private var voiceSlider:Image;
		
		private var dragging:Boolean;
		
		private var startX:Number = 180;//min point for slider
		private var endX:Number = 600;//max point for slider
		private var barWidth:Number = endX-startX;
		
		private var title:TextField;
		private var ok_BTN:Image;
		private var ok_Text:TextField;
		
		public function Settings()
		{
			var bg:Image = new Image(Root.assets.getTexture("Gabba"));
			bg.x = 0;
			bg.y = 0;
			this.addChild(bg);
			
			trace(Costanza.musicVolume + "MUSIC VOLUME" );
			
			musicSlider = new Image(Root.assets.getTexture("VolumeDrag"));
			musicSlider.x = startX + (barWidth*(Costanza.musicVolume*5));
			musicSlider.y = 109;
			musicSlider.name = "music";
			
			soundSlider = new Image(Root.assets.getTexture("VolumeDrag"));
			soundSlider.x = startX + (barWidth*Costanza.soundVolume);
			soundSlider.y = musicSlider.y + musicSlider.height + 50;
			soundSlider.name = "sound";
			
			voiceSlider = new Image(Root.assets.getTexture("VolumeDrag"));
			voiceSlider.x = startX + (barWidth*Costanza.voiceVolume);
			voiceSlider.y = soundSlider.y + soundSlider.height + 50;
			voiceSlider.name = "voice";
			
			this.addChild(musicSlider);
			this.addChild(soundSlider);
			this.addChild(voiceSlider);
			
			var ok:Quad = new Quad(250,75);
			ok.x = this.width/2 - ok.width/2;
			ok.y = this.height - ok.height*1.5;
			ok.alpha = 0;
			this.addChild(ok);
			ok.addEventListener(TouchEvent.TOUCH, closeSettings);
			
			musicSlider.addEventListener(TouchEvent.TOUCH,dragSlider);
			soundSlider.addEventListener(TouchEvent.TOUCH,dragSlider);
			voiceSlider.addEventListener(TouchEvent.TOUCH,dragSlider);
			
		}//end constructor
		
		private function closeSettings(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			if(touch.phase == TouchPhase.BEGAN)
			{
				//saves values to sharedobject
				YoGabbaGabba.savedVariablesObject.data.musicVolume = Costanza.musicVolume;
				YoGabbaGabba.savedVariablesObject.data.voiceVolume = Costanza.voiceVolume;
				YoGabbaGabba.savedVariablesObject.data.soundVolume = Costanza.soundVolume;
				
				YoGabbaGabba.savedVariablesObject.flush();
				
				dispatchEventWith(HIDE_SETTINGS,true);
			}
		}
		
		private function dragSlider(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(this);
			if(touch == null){return;}
			var mc:Image = e.currentTarget as Image;
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				dragging = true;
			}
			else if(touch.phase == TouchPhase.MOVED)
			{
				if(dragging)
				{
					mc.x = touch.globalX - this.x;
					if(mc.x > endX){mc.x = endX;}
					if(mc.x < startX){mc.x = startX}
				}
			}
			else if(touch.phase == TouchPhase.ENDED)
			{
				dragging = false;
				setVolume(mc);
			}
		}//end dragSlider
		
		private function setVolume(mc:Image):void
		{
			switch(mc.name)
			{
				case "music":
				{
					Costanza.musicVolume = ((mc.x - startX)/barWidth)*.2;
					trace(mc.x - startX);
					if(mc.x == startX){Costanza.musicVolume=0;}
					break;
				}
				case "sound":
				{
					Costanza.soundVolume = (mc.x - startX)/barWidth;
					if(mc.x == startX){Costanza.soundVolume=0;}
					break;
				}
				case "voice":
				{
					Costanza.voiceVolume = (mc.x - startX)/barWidth;
					if(mc.x == startX){Costanza.voiceVolume=0;}
					break;
				}
			}
		}
	}
}