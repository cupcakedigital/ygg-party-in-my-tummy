package ui
{
	import com.greensock.TweenMax;
	import com.greensock.easing.Back;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.textures.TextureSmoothing;
	import starling.utils.Color;

	public class InstructionsBar extends Sprite
	{
		private var bg:Image;
		private var textField:TextField;
		private var textFieldDrop:TextField;
		
		private var larryPop:Image;
		
		public function InstructionsBar(text:String="", fSize:int=46, topOfScreen:Boolean=false, showLarry:Boolean=false)
		{
			this.touchable = false;
			bg = new Image(Root.assets.getTexture("Instructions BG"));
			bg.smoothing = TextureSmoothing.BILINEAR;
			this.addChild(bg);
			
			textField = new TextField(1000,150, text, VeggieTalesXmas.headerFont.fontName);
			textField.color = Color.WHITE;
			textField.fontSize = fSize;
			textField.y = -25;
			textField.hAlign = "center";
			textField.text = text;
			
			textFieldDrop = new TextField(1000,150, text, VeggieTalesXmas.headerFont.fontName);
			textFieldDrop.color = 0x003405;
			textFieldDrop.fontSize = fSize;
			textFieldDrop.x = textField.x+1;
			textFieldDrop.y = textField.y+1;
			textFieldDrop.hAlign = "center";
			textFieldDrop.text = text;
			this.addChild(textFieldDrop);
			this.addChild(textField);
			
			this.x = Costanza.STAGE_WIDTH/2 - this.width/2;
			if(!topOfScreen)
			{
				this.y = Costanza.STAGE_HEIGHT - bg.height;
			}
			
			if(showLarry)
			{
				larryPop = new Image(Root.assets.getTexture("Larry"));
				larryPop.pivotX = larryPop.width/2;
				larryPop.pivotY = larryPop.height/2;
				//larryPop.rotation = Math.PI/4;
				larryPop.x = bg.x + 150;
				larryPop.y = bg.y;
				addChildAt(larryPop,getChildIndex(bg));
				
				TweenMax.to(larryPop,.8,{y:larryPop.y-60, ease:Back.easeInOut});
			}
		}
		
		public function hideLarry():void
		{
			TweenMax.to(larryPop,.6,{x:larryPop.x-40, y:larryPop.y+60, alpha:0, ease:Back.easeInOut});
		}
		
		public function switchText(txt:String):void
		{
			textField.text = txt;
			textFieldDrop.text = txt;
		}
		
		public function destroy():void
		{
			this.removeChild(bg,true);
			this.removeChild(textField,true);
			this.parent.removeChild(this,true);
		}
	}
}