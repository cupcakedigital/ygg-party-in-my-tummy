package ui
{
	import com.greensock.TweenMax;
	
	import flash.desktop.NativeApplication;
	import flash.desktop.SystemIdleMode;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.system.Capabilities;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.extensions.SoundManager;
	import starling.utils.AssetManager;

	public class Comic extends Sprite
	{
		public static const HIDE_COMIC:String = "hidecomic";
		public static const RESTART:String = "restart";
		
		private var comicAssets:AssetManager;
		private var appDir2:File = File.applicationDirectory;
		private var soundmanager:SoundManager;
		
		private var comicImg:Image;
		private var skip:Button;
		
		//names for img and sound
		private var soundName:String;
		private var imgName:String;
		private var showskip:Boolean;
		
		private var ending:Boolean = false;
		
		public function Comic(imgString:String, soundString:String, showSkip:Boolean=false, end:Boolean=false)
		{
			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE;
			
			ending = end;
			soundName = soundString;
			imgName = imgString;
			showskip = showSkip;
			
			comicAssets = new AssetManager();
			Root.currentAssetsProxy = comicAssets;
			comicAssets.verbose = Capabilities.isDebugger;
			
			comicAssets.enqueue(
				appDir2.resolvePath("textures/LoadScreens/" + imgName + ".png"),
				appDir2.resolvePath("audio/ComicIntros/" + soundName + ".mp3"),
				appDir2.resolvePath("textures/LoadScreens/Back_1.png"),
				appDir2.resolvePath("textures/LoadScreens/Back_2.png")
				
			);
			
			//dispatchEventWith(HIDE_COMIC,true);
			
			comicAssets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1){
					addObjects();
				};
			});
		}
		
		private function addObjects():void
		{
			soundmanager = SoundManager.getInstance();
			
			soundmanager.addSound(soundName,comicAssets.getSound(soundName));
			soundmanager.playSound(soundName,Costanza.voiceVolume);
			
			comicImg = new Image(comicAssets.getTexture(imgName));
			this.addChild(comicImg);
			
			skip = new Button(Root.assets.getTexture("Net"),"",Root.assets.getTexture("Next_Over"));
			skip.x = 1000;
			skip.y = 660;
			skip.touchable = skip.visible = showskip;
			this.addChild(skip);
			skip.addEventListener(starling.events.Event.TRIGGERED,skipComic);
			
			if(skip.visible){TweenMax.delayedCall(3,tweenBTN);}
			
			if(!ending){dispatchEventWith(Root.SCENE_LOADED,true);}
			
			//Starling.current.nativeStage.addEventListener(flash.events.Event.ACTIVATE, focusGained);
		//	Starling.current.nativeStage.addEventListener(flash.events.Event.DEACTIVATE, focusLost);
		}
		
		private function focusGained(e:flash.events.Event):void
		{
			//starling stage restarts when focus is lost and gained.
			//check to see if stage isnt visible, meaning for parents or more apps is visible.
			//If they are, stop the stage again manually, prevents starling buttons from working underneath more apps
		/*	if(skip.alpha == 0)
			{
				trace("asdasdasdasd");
				showSkip();
			}*/
		}
		private function focusLost(e:flash.events.Event):void
		{
			//starling stage restarts when focus is lost and gained.
			//check to see if stage isnt visible, meaning for parents or more apps is visible.
			//If they are, stop the stage again manually, prevents starling buttons from working underneath more apps
		/*	if(skip.alpha == 0)
			{
				trace("asdasdasdasd");
				showSkip();
			}*/
		}
		
		public function showSkip():void
		{
			skip.visible = true;
			skip.touchable = true;
			TweenMax.delayedCall(3,tweenBTN);
		}
		
		private function tweenBTN():void
		{
			TweenMax.to(skip,1,{alpha:.5,yoyo:true,repeat:25});
		}
		
		private function skipComic(e:starling.events.Event):void
		{
			skip.removeEventListener(starling.events.Event.TRIGGERED,skipComic);
			TweenMax.killTweensOf(skip);
			TweenMax.killDelayedCallsTo(tweenBTN);
			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.NORMAL;
			
			if(soundmanager.soundIsPlaying(soundName))
				soundmanager.tweenVolume(soundName,0,0.1);
			
			//if(ending){dispatchEventWith(RESTART,true);}
			//else{
				dispatchEventWith(HIDE_COMIC,true);
				TweenMax.to(this,.5,{alpha:0, onComplete:destroy});
		//	}
		}
		
		public function destroy():void
		{
			TweenMax.killTweensOf(skip);
			TweenMax.killDelayedCallsTo(tweenBTN);
			comicAssets.dispose();
			//soundManager.removeSound("WidgetTheme");
			this.removeChildren(0,this.numChildren-1,true);
			this.parent.removeChild(this,true);
		}
	}
}