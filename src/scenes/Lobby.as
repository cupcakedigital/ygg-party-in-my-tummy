﻿package scenes  {
	
	import com.cupcake.App;
	import com.greensock.TweenMax;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.system.Capabilities;
	import flash.system.System;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	import starling.utils.AssetManager;

	public class Lobby extends Sprite {
	
		// Scene Objects
		
		private var sceneRoot:Sprite;
		
		public static const LOAD_MENU:String = "loadMenu";
		
		private var bg:Image;
		
		// Standard display
		private var elapsedTime:Number = 0;
		private var elapsedFrame:int = 0;
		
		private var stageWidth:int;
		private var stageHeight:int;
		
		private var soundmanager:SoundManager;
		private var assets:AssetManager;
		private var appDir:File = File.applicationDirectory;
	
		// Bubble Events
		
		public static const SET_CHARACTER:String = "setCharacter";
		public static const LOAD_ROOM:String = "loadRoom";
		
		public static const LOAD_NAILS:String = "loadNails";
		public static const LOAD_HAIR:String = "loadHair";
		public static const LOAD_MAKEUP:String = "loadMakeup";
		public static const LOAD_PAINT:String = "loadPaint";
		
		//scene assets
		private var charPositions:Array = [new Point(255,352),new Point(425,355),new Point(606,360),new Point(767,400),new Point(962,380)];
		private var characters:Array = [];
		private var glows:Array = [];
		private var charStrings:Array = ["Muno","Foofa","Plex","Brobee","Toodee"];
		
		private var canTap:Boolean = true;
		
		private var lanceTaps:int = 1;
		
		private var backBtn:Button;
		private var assetsLoaded:Boolean = false;
		
		//private var bubb:BubbleScreen;
		
		public function Lobby(charString:String = "", foodString:String = "")
		{
			//addEventListener(starling.events.Event.ADDED_TO_STAGE, textureCompleteHandler);
			assets = new AssetManager(Costanza.SCALE_FACTOR);
			assets.useMipMaps = Costanza.mipmapsEnabled;
			Root.currentAssetsProxy = assets;
			assets.verbose = Capabilities.isDebugger;
			
			assets.enqueue(
				appDir.resolvePath("textures/"+Costanza.SCALE_FACTOR +"x/lobby"),
				appDir.resolvePath("audio/lobby")
				// Add in room sound if available
			);
			
			assets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1){
					assetsLoaded = true;
					Starling.juggler.delayCall(function():void
					{
						// now would be a good time for a clean-up 
						System.pauseForGCIfCollectionImminent(0);
						System.gc();
						
					}, 0.15);
					addObjects();
				};
			});
			
		}
		
		private function addObjects():void
		{
			addEventListener(Root.DISPOSE,  disposeScene);
			
			soundmanager = SoundManager.getInstance();
			
			for(var j:int=1; j <=5; j++)
			{
				soundmanager.addSound("select_" + j,assets.getSound("select_" + j));
				soundmanager.addSound("selectVO_" + j,assets.getSound("selectVO_" + j));
			}
			soundmanager.addSound("LanceVO_1",assets.getSound("LanceVO_1"));
			soundmanager.addSound("LanceVO_2",assets.getSound("LanceVO_2"));
			soundmanager.addSound("LanceVO_3",assets.getSound("LanceVO_3"));
			
			soundmanager.addSound("Lobby_C_Music",Root.assets.getSound("Lobby_C_Music"));
			soundmanager.playSound("Lobby_C_Music",Costanza.musicVolume,999);
			
			// Create main scene root
			sceneRoot = new Sprite();
			sceneRoot.x = Costanza.STAGE_OFFSET;
			
			addChild(sceneRoot);
			
			// Sprites & Clips
			bg = new Image(assets.getTexture("BG"));
			bg.x = Costanza.STAGE_OFFSET * -1;
			sceneRoot.addChild(bg);
			
			//back button
			backBtn = new Button(assets.getTexture("Back_Arrow"));
			backBtn.x = 180;
			backBtn.y = 10;
			backBtn.addEventListener(starling.events.Event.TRIGGERED,goBack);
			
			for(var i:int=1; i<=5; i++)
			{
				var glow:Image = new Image(assets.getTexture(i.toString() + "_Over"));
				glow.x = charPositions[i-1].x - 4;
				glow.y = charPositions[i-1].y - 4;
				glow.alpha = 0;
				sceneRoot.addChild(glow);
				
				var img:Image = new Image(assets.getTexture(i.toString()));
				img.x = charPositions[i-1].x;
				img.y = charPositions[i-1].y;
				img.name = i.toString();
				img.addEventListener(TouchEvent.TOUCH, tapChar);
				sceneRoot.addChild(img);
				characters.push(img);
				glows.push(glow);
			}
			
			var hitbox:Quad = new Quad(Costanza.STAGE_WIDTH/1.5, Costanza.STAGE_HEIGHT/3);
			hitbox.x = 250;
			hitbox.y = 0;
			hitbox.alpha = 0;
			sceneRoot.addChild(hitbox);
			hitbox.addEventListener(TouchEvent.TOUCH, tapLance);
			
			var videoBtn:Button = new Button(assets.getTexture("Video"));
			videoBtn.x = 300;
			videoBtn.y = Costanza.STAGE_HEIGHT - videoBtn.height;
			videoBtn.addEventListener(starling.events.Event.TRIGGERED, tapVideo);
			sceneRoot.addChild(videoBtn);
			
			var Color_button:Button = new Button(assets.getTexture("Color_button"));
			Color_button.x = 900;
			Color_button.y = Costanza.STAGE_HEIGHT - Color_button.height;
			Color_button.addEventListener(starling.events.Event.TRIGGERED, tapColor);
			sceneRoot.addChild(Color_button);
			
		//	bubb = new BubbleScreen();
		//	sceneRoot.addChild(bubb);
			//bubb.visible = false;
			
			sceneRoot.addChild(backBtn);
			
			soundmanager.playSound("LanceVO_1",Costanza.voiceVolume);
			
			App.backPressed.add(BackPressed);
			// Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED,true);
		}
		
		private function BackPressed():void
		{
			goBack(null);
		}
		
		private function goBack(e:starling.events.Event):void
		{
			if ( canTap )
			{
				soundmanager.getSoundChannel("Lobby_C_Music").stop();
				
				if(soundmanager.soundIsPlaying("LanceVO_1")){soundmanager.getSoundChannel("LanceVO_1").stop();}
				else if(soundmanager.soundIsPlaying("LanceVO_2")){soundmanager.getSoundChannel("LanceVO_2").stop();}
				else if(soundmanager.soundIsPlaying("LanceVO_2")){soundmanager.getSoundChannel("LanceVO_2").stop();}
				
				dispatchEventWith(LOAD_MENU, true, "classic");
				canTap = false;
			}
		}
		
		private function tapVideo(e:starling.events.Event):void
		{
			if ( canTap )
			{
				if(soundmanager.soundIsPlaying("LanceVO_1")){soundmanager.getSoundChannel("LanceVO_1").stop();}
				else if(soundmanager.soundIsPlaying("LanceVO_2")){soundmanager.getSoundChannel("LanceVO_2").stop();}
				else if(soundmanager.soundIsPlaying("LanceVO_2")){soundmanager.getSoundChannel("LanceVO_2").stop();}
				
				soundmanager.getSoundChannel("Lobby_C_Music").stop();
				dispatchEventWith(LOAD_ROOM, true, 6);
				canTap = false;
			}
		}
		
		private function tapColor(e:starling.events.Event):void
		{
			if ( canTap )
			{
				if(soundmanager.soundIsPlaying("LanceVO_1")){soundmanager.getSoundChannel("LanceVO_1").stop();}
				else if(soundmanager.soundIsPlaying("LanceVO_2")){soundmanager.getSoundChannel("LanceVO_2").stop();}
				else if(soundmanager.soundIsPlaying("LanceVO_2")){soundmanager.getSoundChannel("LanceVO_2").stop();}
				
				soundmanager.getSoundChannel("Lobby_C_Music").stop();
				dispatchEventWith(LOAD_ROOM, true, 5);
				canTap = false;
			}
		}
		
		private function tapLance(e:TouchEvent):void
		{
			//var touch:Touch = e.getTouch(stage);
			var touches:Vector.<Touch> = e.getTouches(this, TouchPhase.BEGAN);
			
			if(touches.length == 1 && !soundmanager.soundIsPlaying("LanceVO_" + lanceTaps) && canTap )
			{	
				lanceTaps++;
				if(lanceTaps > 3)
				{ 
					lanceTaps = 1; 
					//Costanza.overallPlays = 8;
				}
				
				soundmanager.playSound("LanceVO_" + lanceTaps, Costanza.voiceVolume);
			}
		}
		
		private function tapChar(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(this, TouchPhase.BEGAN);
			
			//var touch:Touch = e.getTouch(stage);
			//if(touch == null){return;}
			//var img:Image = e.currentTarget as Image;
			
			if(canTap && touches.length == 1)
			{
				var img:Image = e.currentTarget as Image;
				
				soundmanager.playSound("select_" + img.name, Costanza.soundVolume);
				soundmanager.playSound("selectVO_" + img.name, Costanza.voiceVolume);
				var obj:Object = new Object();
				obj.charString = charStrings[int(img.name)-1];
				
				//checks this character as played, and adds one to overall playcount
				Costanza.characterPlaythroughs[int(img.name)-1] = 1;
				Costanza.overallPlays++;
				
				if(Costanza.FOODS.length == 0)
				{
					Costanza.FOODS = Costanza.FOODS_BACKUP;
				}
				
				var rnd:int = Math.random()*Costanza.FOODS.length-1;
				obj.foodString = Costanza.FOODS.splice(rnd,1);
				
				if(soundmanager.soundIsPlaying("LanceVO_1")){soundmanager.getSoundChannel("LanceVO_1").stop();}
				else if(soundmanager.soundIsPlaying("LanceVO_2")){soundmanager.getSoundChannel("LanceVO_2").stop();}
				else if(soundmanager.soundIsPlaying("LanceVO_2")){soundmanager.getSoundChannel("LanceVO_2").stop();}
				
				soundmanager.getSoundChannel("Lobby_C_Music").stop();
				
				dispatchEventWith(SET_CHARACTER, true, obj);
				TweenMax.to(glows[int(img.name)-1],1,{alpha:1, onComplete:startGame});
				canTap = false;
				
				//save new variables
				YoGabbaGabba.savedVariablesObject.data.FOODS = Costanza.FOODS;
				YoGabbaGabba.savedVariablesObject.data.characterPlaythroughs = Costanza.characterPlaythroughs;
				YoGabbaGabba.savedVariablesObject.flush();
			}
		}
		
		private function startGame():void
		{
			dispatchEventWith(LOAD_ROOM, true,0);
		}
		
		public function destroyScene():void
		{
			sceneRoot.removeChildren(0,sceneRoot.numChildren-1,true);
		}
		
		private function disposeScene():void
		{
			assets.dispose();
			App.backPressed.remove(BackPressed);
			sceneRoot.removeChildren(0,sceneRoot.numChildren-1,true);
		}
	}
}