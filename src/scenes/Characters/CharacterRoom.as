﻿package scenes.Characters  {
	
	import com.cupcake.App;
	import com.cupcake.DeviceInfo;
	import com.greensock.TweenMax;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.system.Capabilities;
	
	import dragonBones.Armature;
	import dragonBones.Bone;
	import dragonBones.animation.WorldClock;
	import dragonBones.factorys.StarlingFactory;
	import dragonBones.objects.ObjectDataParser;
	import dragonBones.objects.SkeletonData;
	import dragonBones.textures.StarlingTextureAtlas;
	
	import feathers.controls.Button;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	
	import utils.ParticleSystemJP.ParticleEmitterJP;
	
	public class CharacterRoom extends Sprite {
		
		// Scene Objects
		private var sceneRoot:Sprite;
		private var bg:Image;
		private var backButton:Button;
		private var soundManager:SoundManager;
		
		private var currentCharacter:String;
		
		private var assets:AssetManager;
		private var appDir:File = File.applicationDirectory;
		
		// Standard display
		private var stageWidth:int;
		private var stageHeight:int;
		
		
		// Bubble Events
		public static const LOAD_LOBBY:String = "loadLobby";
		public static const SET_CHARACTER:String = "setCharacter";
		public static const LOAD_ROOM:String = "loadRoom";
		
		private var soundTap:int = 1;
		
		// Dragon Bones
		
		private var factory:StarlingFactory;
		private var armatures:Array = [];
		
		//scene vars
		private var characterString:String;
		private var foodString:String;
		private var charTaps:int = 0;
		
		//arrays for face positions
		private var brobeeArray:Array = [new Point(400,360),new Point(865,300),new Point(895,320),new Point(120,300)];
		private var foofaArray:Array = [new Point(95,455),new Point(900,380),new Point(980,540)];
		private var munoArray:Array = [new Point(920,280),new Point(1140,150),new Point(285,250)];
		private var plexArray:Array = [new Point(80,420),new Point(980,260),new Point(1138,260)];
		private var toodeeArray:Array = [new Point(1086,480),new Point(280,250)];
		
		private var tapped:Boolean = false;
		
		private var treeTaps:int = 1;
		private var numTreeTaps:int;
		private var flowerTapNum:int = 1;
		
		private var particle:ParticleEmitterJP;
		
		private var particlesArray:Array = new Array();
		
		private var starBurst:Image;
		
		private var bubble2:Image;
		
		private var foodIndex:int;
		private var food2Index:int;
		
		//array of names for character animations
		private var charAnimations:Array = ["cheer","dance","dance2"];
		
		private var foodTextureAtlas:StarlingTextureAtlas;
		private var textureAtlas:StarlingTextureAtlas;
		
		private var assetsLoaded:Boolean = false;
		
		public function CharacterRoom(charString:String = "Brobee", foodStr:String = "Sandwich") {
			
			addEventListener(Root.DISPOSE,  disposeScene);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.ACTIVATE,screenActivated);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.DEACTIVATE,screenDeactivated);
			
			characterString = charString;
			foodString = foodStr;
			
			trace("STAGE OFFSET" + Costanza.STAGE_OFFSET + foodString + characterString);
			
			// Create new asset manager and load content
			assets = new AssetManager(Costanza.SCALE_FACTOR);
			assets.useMipMaps = Costanza.mipmapsEnabled;
			assets.verbose = Capabilities.isDebugger;
			assets.enqueue(
				appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR +"x/CharacterRoom/" + characterString),
				appDir.resolvePath("audio/ARoomSounds/" + characterString),
				appDir.resolvePath("audio/ARoomSounds/Global"),
				appDir.resolvePath("audio/Trees_" + characterString),
				appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR +"x/CharacterRoom/ThoughtBubble.png"),
				appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR +"x/CharacterRoom/ThoughtBubble_2.png"),
				appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR +"x/CharacterRoom/FoodDB"),
				appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Bursts/" + characterString + "_Burst.png")
			);
			
			assets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1){
					assetsLoaded = true;
					trace("finished loading character");
					addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrameHandler);
					addObjects();
				};
				
			});
			
		}
		
		private function screenActivated(e:flash.events.Event):void
		{
			trace("----------------------------SCENE ACTIVATED-----------------------------");
			if(!assetsLoaded)
			{
				assets.enqueue(
					appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR +"x/CharacterRoom/" + characterString),
					appDir.resolvePath("audio/ARoomSounds/" + characterString),
					appDir.resolvePath("audio/ARoomSounds/Global"),
					appDir.resolvePath("audio/Trees_" + characterString),
					appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR +"x/CharacterRoom/ThoughtBubble.png"),
					appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR +"x/CharacterRoom/ThoughtBubble_2.png"),
					appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR +"x/CharacterRoom/FoodDB"),
					appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Bursts/" + characterString + "_Burst.png")
				);
				assets.loadQueue(function onProgress(ratio:Number):void
				{
					if (ratio == 1){
						assetsLoaded = true;
						trace("finished loading character");
						addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrameHandler);
						addObjects();
					};
					
				});
			}
		}
		
		private function screenDeactivated(e:flash.events.Event):void
		{
			trace("----------------------------SCENE DEACTIVATED-----------------------------");
			if(!assetsLoaded)
			{
				assets.purgeQueue();
			}
		}
		
		private function addObjects():void
		{
			// Create main scene root
			sceneRoot = new Sprite();
			sceneRoot.x = Costanza.STAGE_OFFSET;
			
			addChild(sceneRoot);
			
			soundManager = SoundManager.getInstance();
			
			//sfx
			soundManager.addSound("Tap_1", assets.getSound(characterString + "_Tap_1"));
			soundManager.addSound("Tap_2", assets.getSound(characterString + "_Tap_2"));
			
			soundManager.addSound("Music",assets.getSound("Music"));
			soundManager.playSound("Music",Costanza.musicVolume,999);
			
			if(foodString == "Pizza"){soundManager.addSound("Food",assets.getSound("Pizza"));}
			else if(foodString == "Sandwich"){soundManager.addSound("Food",assets.getSound("Sandwich"));}
			else if(foodString == "Sushi"){soundManager.addSound("Food",assets.getSound("Sushi"));}
			else{soundManager.addSound("Food",assets.getSound("Bowl"));}
			
			soundManager.addSound("Burst", Root.assets.getSound("Burst"));
			soundManager.addSound("soYummy", assets.getSound("soYummy"));
			
			loadSounds();
			
			var bg:Image = new Image(assets.getTexture("BG"));
			bg.x = Costanza.STAGE_OFFSET * -1;
			sceneRoot.addChild(bg);
			
			// DragonBones Setup
			factory = new StarlingFactory();
			factory.generateMipMaps = Costanza.mipmapsEnabled;
			
			var skeletonData:SkeletonData = ObjectDataParser.parseSkeletonData(assets.getObject("skeletonChar" + characterString));
			factory.addSkeletonData(skeletonData);
			
			var texture:Texture = assets.getTexture("textureChar" + characterString);
			textureAtlas = new StarlingTextureAtlas(texture, assets.getObject("textureChar" + characterString),true);
			factory.addTextureAtlas(textureAtlas);
			
			var foodSkeletonData:SkeletonData = ObjectDataParser.parseSkeletonData(assets.getObject("foodSkeletonChar"));
			factory.addSkeletonData(foodSkeletonData);
			
			var foodTexture:Texture = assets.getTexture("foodTextureChar");
			foodTextureAtlas = new StarlingTextureAtlas(foodTexture, assets.getObject("foodTextureChar"),true);
			factory.addTextureAtlas(foodTextureAtlas);
			
			var numFaces:int = 3;
			
			if(characterString == "Brobee"){numFaces++;}
			else if(characterString == "Muno" || characterString == "Toodee"){numFaces--;}
			
			starBurst = new Image(assets.getTexture(characterString + "_Burst"));
			
			starBurst.pivotX = starBurst.width/2;
			starBurst.pivotY = starBurst.height/2;
			starBurst.x = Costanza.STAGE_WIDTH/2 - Costanza.STAGE_OFFSET;
			starBurst.y = Costanza.STAGE_HEIGHT/2;
			
			starBurst.scaleX = 0;
			starBurst.scaleY = 0;
			
			for(var i:int = 1; i <= 21; i++)
			{
				particlesArray.push(new Image(Root.assets.getTexture("ygg-confetti_000" + i)));
			}
			
			particle = new ParticleEmitterJP(particlesArray,false);
			
			particle.x = Costanza.STAGE_WIDTH/2 - Costanza.STAGE_OFFSET;
			particle.y = Costanza.STAGE_HEIGHT/2;
			
			buildArmatures(numFaces);
			
			App.backPressed.add(BackPressed);
	
			// Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED,true);
			
			addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
			//TweenMax.delayedCall(5,pulseBubble);
		}
		
		private function loadSounds():void
		{
			numTreeTaps = 6;
			if(characterString == "Muno"){numTreeTaps--;}
			else if(characterString == "Toodee"){numTreeTaps = 3;}
			
			for(var i:int=1; i <= numTreeTaps; i++)
			{
				soundManager.addSound("Tree_" + i, assets.getSound("Tree_" + i));
			}
			
			if(characterString == "Foofa")
			{
				soundManager.addSound("Flower_1", assets.getSound("Flower_1"));
				soundManager.addSound("Flower_2", assets.getSound("Flower_2"));
				soundManager.addSound("Flower_3", assets.getSound("Flower_3"));
			}
		}
		
		private function onAddedToStage(evt:starling.events.Event):void
		{
			// Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED,true);
		}
		
		private function pulseBubble():void
		{
			TweenMax.to(bubble2,.3,{alpha:1,yoyo:true, repeat:2, onComplete:donePulse});
			armatures[foodIndex].animation.gotoAndPlay("yummy");
			if(foodString == "Sushi"){armatures[food2Index].animation.gotoAndPlay("yummy");}
		}
		
		private function donePulse():void
		{
			TweenMax.to(bubble2, .5, {alpha:0});
			TweenMax.delayedCall(1.8,pulseBubble);
		}
		
		private function onButtonTriggered():void
		{
			//dispatchEventWith(LOAD_LOBBY, true);
			if(characterString == "Foofa")
			{
				characterString = "Brobee";
			}
			else if(characterString == "Muno")
			{
				characterString = "Foofa";
			}
			else if(characterString == "Plex")
			{
				characterString = "Muno";
			}
			else if(characterString == "Toodee")
			{
				characterString = "Plex";
			}
			
			dispatchEventWith(SET_CHARACTER, true, characterString);
			dispatchEventWith(LOAD_ROOM, true, 3);
		}
		
		private function onButtonTriggeredNext():void
		{
			dispatchEventWith(SET_CHARACTER, true, characterString);
			dispatchEventWith(LOAD_ROOM, true, 1);
		}
		
		private function touchHandler(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.BEGAN);
			
			if(touches.length != 1){return;}
			
			var touch:Touch = touches[0];
			if(touch == null){return;}
			var currSprite:Sprite = e.currentTarget as Sprite;
			var index:int = int(currSprite.name);
			var currArmature:Armature = armatures[index];
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				if(index == 0)//character
				{
					if(charTaps == charAnimations.length){charTaps = 0;}
					
					if(currArmature.animation.movementID == "idle" || currArmature.animation.movementID == "look")
					{
						currArmature.animation.gotoAndPlay(charAnimations[charTaps]); charTaps++;
						
						soundManager.playSound("Tap_" + soundTap,Costanza.voiceVolume);
						soundTap++;
						if(soundTap > 2){soundTap = 1;}
					}
					
					
				}
				else if(index == 1 || (foodString == "Sushi" && index == 2))//food
				{
					TweenMax.killDelayedCallsTo(pulseBubble);
					TweenMax.killTweensOf(bubble2);
					TweenMax.to(bubble2,0.4,{alpha:0});
					soundManager.playSound("Food",Costanza.voiceVolume);
					armatures[1].animation.gotoAndPlay("yummy");
					if(foodString == "Sushi"){armatures[2].animation.gotoAndPlay("yummy");}
					
					if(!tapped)
					{
						tapped = true;
						
						if(soundManager.soundIsPlaying("Tap_1"))
						{
							soundManager.getSoundChannel("Tap_1").stop();	
						}
						if(soundManager.soundIsPlaying("Tap_2"))
						{
							soundManager.getSoundChannel("Tap_2").stop();	
						}
						
						armatures[0].animation.gotoAndPlay("cheer");
						soundManager.playSound("Burst",Costanza.soundVolume);
						soundManager.playSound("soYummy",Costanza.voiceVolume);
						particle.CreateBurst(70,500,500,5,true,true);
						if(!DeviceInfo.isSlow)
						{
							TweenMax.delayedCall(.5,particle.CreateBurst,[70,500,500,5,true,true]);
							TweenMax.delayedCall(1,particle.CreateBurst,[70,500,500,5,true,true]);
							TweenMax.delayedCall(1.5,particle.CreateBurst,[70,500,500,5,true,true]);
						}
						TweenMax.delayedCall(.2,tweenBurst);
						TweenMax.delayedCall(4,nextPage);
					}
				}
				else//one of the faces
				{
					if(currArmature.animation.movementID == "face1"){armatures[index].animation.gotoAndPlay("face2");}
					else if(currArmature.animation.movementID == "face2"){armatures[index].animation.gotoAndPlay("face3");}
					else{armatures[index].animation.gotoAndPlay("face1");}
					
					soundManager.playSound("Tree_" + treeTaps, Costanza.soundVolume);
					treeTaps++;
					if(treeTaps > numTreeTaps)
					{
						treeTaps = 1;
					}
				}
			}
		}
		
		private function tweenBurst():void
		{
			TweenMax.to(starBurst,1,{scaleX:10,scaleY:10, alpha:0});
		}
		
		private function flowerTap(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.BEGAN);
			
			if(touches.length != 1){trace("too long" + touches.length);return;}
			
			var touch:Touch = touches[0];
			if(touch == null){return;}
			var currSprite:Sprite = e.currentTarget as Sprite;
			var index:int = int(currSprite.name);
			var currArmature:Armature = armatures[index];
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				if(currArmature.animation.movementID == "face1"){armatures[index].animation.gotoAndPlay("face2");}
				else if(currArmature.animation.movementID == "face2"){armatures[index].animation.gotoAndPlay("face3");}
				else{armatures[index].animation.gotoAndPlay("face1");}
				
				soundManager.playSound("Flower_" + flowerTapNum, Costanza.soundVolume);
				
				flowerTapNum++;
				if(flowerTapNum > 3){flowerTapNum = 1;}
			}
		}
		
		private function buildArmatures(num:int):void
		{
			var bubble:Image = new Image(assets.getTexture("ThoughtBubble"));
			bubble.x = 230;
			
			bubble2 = new Image(assets.getTexture("ThoughtBubble_2"));
			bubble2.x = 230;
			bubble2.alpha = 0;
			
			var posArray:Array;
			switch(characterString)
			{
				case "Brobee":
				posArray = brobeeArray; break;
				case "Foofa":
				posArray = foofaArray; break;
				case "Muno":
				posArray = munoArray; break;
				case "Plex":
				posArray = plexArray; break;
				case "Toodee":
				posArray = toodeeArray; break;
			}
			
			//add character
			var charArmature:Armature = factory.buildArmature("Characters/" + characterString);
			charArmature.animation.gotoAndPlay("idle",5);
			charArmature.display.x = Costanza.STAGE_WIDTH/2 - Costanza.STAGE_OFFSET;
			if(characterString == "Brobee" || characterString == "Toodee")
			{
				charArmature.display.y = Costanza.STAGE_HEIGHT - charArmature.display.height/2;
			}
			else
			{
				charArmature.display.y = Costanza.STAGE_HEIGHT/2;
			}
			
			WorldClock.clock.add(charArmature);
			armatures.push(charArmature);
			//sprite for armature
			var charArmatureClip:Sprite = charArmature.display as Sprite;
			charArmatureClip.name = String(armatures.length-1);//set name to index
			charArmatureClip.addEventListener(TouchEvent.TOUCH, touchHandler);
			
			
			//------------
			//add food
			//-------------
			var imgSwap:Boolean = false;
			var sammichArmature:Armature;
			if(foodString == "Yogurt" || foodString == "Salad" || foodString == "Fruitsalad" || foodString == "Noodles")
			{
				sammichArmature = factory.buildArmature("Characters/Bowl");
				imgSwap = true;
			}
			else
			{
				sammichArmature = factory.buildArmature("Characters/" + foodString);
			}
			sammichArmature.animation.gotoAndPlay("idle");
			sammichArmature.display.x = bubble.x + bubble.width/2;
			sammichArmature.display.y = bubble.y + bubble.height/2;
			WorldClock.clock.add(sammichArmature);
			armatures.push(sammichArmature);
			//sprite for armature
			var sammichArmatureClip:Sprite = sammichArmature.display as Sprite;
			sammichArmatureClip.name = String(armatures.length-1);
			foodIndex = armatures.length-1;
			sammichArmatureClip.addEventListener(TouchEvent.TOUCH, touchHandler);
			
			//swaps texture to match current food and character
			if(imgSwap)
			{
				var _image:Image = factory.getTextureDisplay("Pieces/Bowls/" + characterString) as Image;
				// assign image to bone.display for chaging clothes effect. (using bone.display to dispose)
				var _bone:Bone = sammichArmature.getBone("body"); _bone.display.dispose();
				_bone.display = _image;
				
				var _image2:Image = factory.getTextureDisplay("Pieces/Food/" + foodString) as Image;
				// assign image to bone.display for chaging clothes effect. (using bone.display to dispose)
				var _bone2:Bone = sammichArmature.getBone("food"); _bone2.display.dispose();
				_bone2.display = _image2;
			}
			
			
			if(foodString == "Sushi")
			{
				var sammichArmature2:Armature = factory.buildArmature("Characters/" + foodString + "_2");
				sammichArmature2.animation.gotoAndPlay("idle");
				sammichArmature2.display.x = bubble.x +  + 100;
				sammichArmature2.display.y = bubble.y + bubble.height/2;
				WorldClock.clock.add(sammichArmature2);
				armatures.push(sammichArmature2);
				//sprite for armature
				var sammichArmatureClip2:Sprite = sammichArmature2.display as Sprite;
				sammichArmatureClip2.name = String(armatures.length-1);
				food2Index = armatures.length-1;
				sammichArmatureClip2.addEventListener(TouchEvent.TOUCH, touchHandler);
			}
			
			//--------
			//faces
			//--------
			for(var i:int = 1; i <= num; i++)
			{
				// Build the armatures
				var tmpArm:Armature = factory.buildArmature("Characters/Face_" + i);
				tmpArm.display.x = posArray[i-1].x;
				tmpArm.display.y = posArray[i-1].y;
				WorldClock.clock.add(tmpArm);
				armatures.push(tmpArm);
				tmpArm.animation.gotoAndPlay("face1");
				
				//sprite for armature
				var tmpArmClip:Sprite = tmpArm.display as Sprite;
				tmpArmClip.name = String(armatures.length-1);
				if(characterString == "Foofa" && i == 1)
					tmpArmClip.addEventListener(TouchEvent.TOUCH, flowerTap);
				else
					tmpArmClip.addEventListener(TouchEvent.TOUCH, touchHandler);
				
				sceneRoot.addChild(tmpArmClip);//add image after image hole
			}
			
			sceneRoot.addChild(bubble);
			sceneRoot.addChild(bubble2);
			sceneRoot.addChild(sammichArmatureClip);
			if(sammichArmatureClip2){sceneRoot.addChild(sammichArmatureClip2);}
			sceneRoot.addChild(starBurst);//add starburst behind char
			sceneRoot.addChild(particle);
			starBurst.touchable = false;
			particle.touchable = false;
			sceneRoot.addChild(charArmatureClip);
			
		}
		
		private function nextPage():void
		{
			soundManager.getSoundChannel("Music").stop();
			dispatchEventWith(LOAD_ROOM, true, 1);
		}
		
		private function BackPressed():void
		{
			dispatchEventWith("loadLobby", true);
		}
		
		private function onEnterFrameHandler(evt:EnterFrameEvent):void
		{
			WorldClock.clock.advanceTime(-1);
		}
		
		public function disposeScene():void
		{
			assets.removeTexture("textureChar" + characterString,true);
			assets.removeTexture("foodTextureChar",true);
			App.backPressed.remove(BackPressed);
			TweenMax.killAll(true);
			textureAtlas.dispose();
			foodTextureAtlas.dispose();
			factory.dispose(true);
			
			soundManager.stopAllSounds();
			
			var len:int = armatures.length;
			for (var i:int = 0; i < len; i++)
			{
				WorldClock.clock.remove(armatures[i]);
				armatures[i].dispose();
			}
			armatures.length = 0;
			sceneRoot.removeChildren(0,sceneRoot.numChildren-1,true);
			
			assets.dispose();
		}
		
		
	}
}