package scenes.Matching
{
	import com.cupcake.App;
	import com.greensock.TweenMax;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.system.Capabilities;
	
	import dragonBones.Armature;
	import dragonBones.animation.WorldClock;
	import dragonBones.events.FrameEvent;
	import dragonBones.factorys.StarlingFactory;
	import dragonBones.objects.ObjectDataParser;
	import dragonBones.objects.SkeletonData;
	import dragonBones.textures.StarlingTextureAtlas;
	
	import feathers.controls.Button;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	
	import utils.ParticleSystemJP.ParticleEmitterJP;

	public class MatchingRoom extends Sprite
	{
		private var sceneRoot:Sprite;
		private var backButton:Button;
		private var soundManager:SoundManager;
		
		private var currentCharacter:String;
		
		private var assets:AssetManager;
		private var appDir:File = File.applicationDirectory;
		
		
		// Standard display
		private var stageWidth:int;
		private var stageHeight:int;
		
		
		// Bubble Events
		public static const LOAD_LOBBY:String = "loadLobby";
		public static const SET_CHARACTER:String = "setCharacter";
		public static const LOAD_ROOM:String = "loadRoom";
		
		
		// Dragon Bones
		
		private var factory:StarlingFactory;
		private var armatures:Array = [];
		
		private var hitbox:Quad;
		
		private var tapped:Boolean = false;
		private var dragging:Boolean = false;
		private var tweening:Boolean = false;
		
		private var startPnt:Point = new Point(0,0);
		
		//scene vars
		private var characterString:String;
		private var returnPoint:Point = new Point(0,0);//point for saving start pos of objects
		private var arrayHitBoxes:Array = [];
		private var arrayFood:Array = [];
		private var correctCount:int = 0;
		private var totalPieces:int = 4;
		
		private var particle:ParticleEmitterJP;
		
		private var particlesArray:Array = new Array();
		
		private var starBurst:Image;
		
		private var foodPositions:Array = [new Point(1020,100),new Point(260,480),new Point(260,275),new Point(1050,500),new Point(320,80)];
		
		private var draggingSprite:Sprite;
		
		private var textureAtlas:StarlingTextureAtlas;
		
		private var brobeePositions:Array = [
			[new Point(450,400),new Point(400,220),new Point(660,180),new Point(660,400),new Point(850,300)],
			[new Point(450,400),new Point(400,220),new Point(700,400),new Point(650,200)],
			[new Point(450,400),new Point(360,260),new Point(700,190),new Point(720,400)],
			[new Point(500,400),new Point(380,250),new Point(720,210),new Point(700,430)],
			[new Point(520,400),new Point(360,280),new Point(670,200),new Point(800,420)],
			[new Point(450,400),new Point(400,220),new Point(660,180),new Point(720,420)],
			[new Point(450,400),new Point(400,220),new Point(660,180),new Point(720,320)]
		];
		
		private var foofaPositions:Array = [
			[new Point(400,200),new Point(420,460),new Point(670,150),new Point(640,475),new Point(860,350)],
			[new Point(450,500),new Point(800,200),new Point(750,450),new Point(400,170)],
			[new Point(750,430),new Point(400,220),new Point(440,420),new Point(660,160)],
			[new Point(430,460),new Point(700,500),new Point(760,150),new Point(400,190)],
			[new Point(470,450),new Point(750,180),new Point(400,220),new Point(700,440)],
			[new Point(480,440),new Point(400,220),new Point(700,160),new Point(680,450)],
			[new Point(760,400),new Point(400,220),new Point(680,150),new Point(420,410)]
		];
		
		private var munoPositions:Array = [
			[new Point(450,120),new Point(700,275),new Point(550,500),new Point(340,450),new Point(980,250)],
			[new Point(420,470),new Point(750,260),new Point(970,260),new Point(370,80)],
			[new Point(420,500),new Point(400,120),new Point(700,280),new Point(900,250)],
			[new Point(420,500),new Point(400,120),new Point(700,280),new Point(900,250)],
			[new Point(930,250),new Point(420,100),new Point(700,280),new Point(420,530)],
			[new Point(340,450),new Point(400,120),new Point(530,500),new Point(900,250)],
			[new Point(550,480),new Point(400,120),new Point(340,450),new Point(850,200)]
		];
		
		private var plexPositions:Array = [
			[new Point(380,170),new Point(370,400),new Point(750,150),new Point(560,300),new Point(740,360)],
			[new Point(420,360),new Point(400,170),new Point(660,380),new Point(620,180)],
			[new Point(420,320),new Point(380,170),new Point(660,220),new Point(660,380)],
			[new Point(450,400),new Point(400,220),new Point(660,180),new Point(660,400)],
			[new Point(450,350),new Point(400,180),new Point(660,180),new Point(660,400)],
			[new Point(450,350),new Point(400,220),new Point(670,160),new Point(700,390)],
			[new Point(420,350),new Point(370,180),new Point(550,170),new Point(640,300)]
		];
		
		private var toodeePositions:Array = [
			[new Point(420,220),new Point(370,410),new Point(580,470),new Point(820,200),new Point(620,120)],
			[new Point(380,400),new Point(400,220),new Point(580,520),new Point(620,160)],
			[new Point(420,430),new Point(500,160),new Point(400,290),new Point(720,160)],
			[new Point(550,480),new Point(530,180),new Point(420,320),new Point(750,200)],
			[new Point(460,480),new Point(500,160),new Point(550,330),new Point(780,240)],
			[new Point(550,450),new Point(550,160),new Point(400,290),new Point(760,200)],
			[new Point(520,460),new Point(540,140),new Point(400,290),new Point(680,140)]
		];
		//scene objects
		private var bg:Image;
		
		private var foodString:String;
		
		private var assetsLoaded:Boolean = false;
		
		public function MatchingRoom(charString:String="", foodStr:String="")
		{
			addEventListener(Root.DISPOSE,  disposeScene);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.ACTIVATE,screenActivated);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.DEACTIVATE,screenDeactivated);
			
			characterString = charString;
			foodString = foodStr;
			
			if(foodString == "Fruitsalad"){foodString = "FruitSalad";}
			if(foodString == "Noodles"){foodString = "Noodle";}
			
			// Create new asset manager and load content
			assets = new AssetManager(Costanza.SCALE_FACTOR);
			assets.useMipMaps = Costanza.mipmapsEnabled;
			assets.verbose = Capabilities.isDebugger;
			assets.enqueue(
				appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR +"x/MatchingRoom/" + foodString),
				appDir.resolvePath("audio/DRoomSounds/" + characterString),
				appDir.resolvePath("audio/DRoomSounds/Global"),
				appDir.resolvePath("audio/Food_Names/" + foodString),
				appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/MatchingRoom/BGs/BG_" + characterString + ".png"),
				appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Bursts/" + characterString + "_Burst.png")
			);
			
			assets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1){
					assetsLoaded = true;
					trace("finished loading character");
					addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrameHandler);
					addObjects();
				};
				
			});
		}
		
		private function screenActivated(e:flash.events.Event):void
		{
			trace("----------------------------SCENE ACTIVATED-----------------------------");
			if(!assetsLoaded)
			{
				assets.enqueue(
					appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR +"x/MatchingRoom/" + foodString),
					appDir.resolvePath("audio/DRoomSounds/" + characterString),
					appDir.resolvePath("audio/DRoomSounds/Global"),
					appDir.resolvePath("audio/Food_Names/" + foodString),
					appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/MatchingRoom/BGs/BG_" + characterString + ".png"),
					appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Bursts/" + characterString + "_Burst.png")
				);
				assets.loadQueue(function onProgress(ratio:Number):void
				{
					if (ratio == 1){
						assetsLoaded = true;
						trace("finished loading character");
						addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrameHandler);
						addObjects();
					};
					
				});
			}
		}
		
		private function screenDeactivated(e:flash.events.Event):void
		{
			trace("----------------------------SCENE DEACTIVATED-----------------------------");
			if(!assetsLoaded)
			{
				assets.purgeQueue();
			}
		}
		
		private function addObjects():void
		{
			// Create main scene root
			sceneRoot = new Sprite();
			sceneRoot.x = Costanza.STAGE_OFFSET;
			
			addChild(sceneRoot);
			
			soundManager = SoundManager.getInstance();
			
			loadSounds();
			
			//sfx
			soundManager.addSound("Done", assets.getSound("Done"));
			if(characterString == "Plex")
				soundManager.addSound("Correct", assets.getSound("Correct_Plex"));
			else
				soundManager.addSound("Correct", assets.getSound("Correct"));
			
			soundManager.addSound("Wrong", assets.getSound("Wrong"));
			soundManager.addSound("Song", assets.getSound("Song"));
			
			soundManager.addSound("Burst", Root.assets.getSound("Burst"));
			
			var bg:Image = new Image(assets.getTexture("BG_" + characterString));
			bg.x = Costanza.STAGE_OFFSET * -1;
			sceneRoot.addChild(bg);
			
			// DragonBones Setup
			factory = new StarlingFactory();
			
			var skeletonData:SkeletonData = ObjectDataParser.parseSkeletonData(assets.getObject("skeletonMatching" + foodString));
			factory.addSkeletonData(skeletonData);
			
			var texture:Texture = assets.getTexture("textureMatching" + foodString);
			textureAtlas = new StarlingTextureAtlas(texture, assets.getObject("textureMatching" + foodString),true);
			factory.addTextureAtlas(textureAtlas);
			
			if(foodString == "Sandwich"){totalPieces++;}
			
			starBurst = new Image(assets.getTexture(characterString + "_Burst"));
			
			starBurst.pivotX = starBurst.width/2;
			starBurst.pivotY = starBurst.height/2;
			starBurst.x = Costanza.STAGE_WIDTH/2 - Costanza.STAGE_OFFSET;
			starBurst.y = Costanza.STAGE_HEIGHT/2;
			
			starBurst.scaleX = 0;
			starBurst.scaleY = 0;
			
			for(var i:int = 1; i <= 21; i++)
			{
				particlesArray.push(new Image(Root.assets.getTexture("ygg-confetti_000" + i)));
			}
			
			particle = new ParticleEmitterJP(particlesArray,false);
			
			particle.x = Costanza.STAGE_WIDTH/2 - Costanza.STAGE_OFFSET;
			particle.y = Costanza.STAGE_HEIGHT/2;
				
			buildArmatures(totalPieces);
			
			App.backPressed.add(BackPressed);
			
			// Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED,true);
			
			addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function loadSounds():void
		{
			switch(foodString)
			{
				case "Sandwich":
				{
					soundManager.addSound("Food_1", assets.getSound("Food_0"));
					soundManager.addSound("Food_4", assets.getSound("Food_1"));
					soundManager.addSound("Food_2", assets.getSound("Food_2"));
					soundManager.addSound("Food_3", assets.getSound("Food_3"));
					soundManager.addSound("Food_0", assets.getSound("Food_4"));
					break;
				}
				case "Yogurt":
				{
					soundManager.addSound("Food_2", assets.getSound("Food_0"));
					soundManager.addSound("Food_3", assets.getSound("Food_1"));
					soundManager.addSound("Food_0", assets.getSound("Food_2"));
					soundManager.addSound("Food_1", assets.getSound("Food_3"));
					break;
				}
				case "Pizza":
				{
					soundManager.addSound("Food_3", assets.getSound("Food_0"));
					soundManager.addSound("Food_0", assets.getSound("Food_1"));
					soundManager.addSound("Food_2", assets.getSound("Food_2"));
					soundManager.addSound("Food_1", assets.getSound("Food_3"));
					break;
				}
				case "Noodle":
				{
					soundManager.addSound("Food_3", assets.getSound("Food_0"));
					soundManager.addSound("Food_2", assets.getSound("Food_1"));
					soundManager.addSound("Food_1", assets.getSound("Food_2"));
					soundManager.addSound("Food_0", assets.getSound("Food_3"));
					break;
				}
				case "Sushi":
				{
					soundManager.addSound("Food_3", assets.getSound("Food_0"));
					soundManager.addSound("Food_2", assets.getSound("Food_1"));
					soundManager.addSound("Food_0", assets.getSound("Food_2"));
					soundManager.addSound("Food_1", assets.getSound("Food_3"));
					break;
				}
				case "Salad":
				{
					soundManager.addSound("Food_2", assets.getSound("Food_0"));
					soundManager.addSound("Food_3", assets.getSound("Food_1"));
					soundManager.addSound("Food_1", assets.getSound("Food_2"));
					soundManager.addSound("Food_0", assets.getSound("Food_3"));
					break;
				}
				case "FruitSalad":
				{
					soundManager.addSound("Food_2", assets.getSound("Food_0"));
					soundManager.addSound("Food_0", assets.getSound("Food_1"));
					soundManager.addSound("Food_3", assets.getSound("Food_2"));
					soundManager.addSound("Food_1", assets.getSound("Food_3"));
					break;
				}
			}
		}
		
		private function foodTouch(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(stage);
			
			for each (var touch:Touch in touches)
			{
				if(touch == null){return;}
				var currentSprite:Sprite = e.currentTarget as Sprite;
				
				if(draggingSprite != null && currentSprite != draggingSprite){trace("RETURNED");return;}
				
				var index:int = int(currentSprite.name);
				
				switch(touch.phase)
				{
					case TouchPhase.BEGAN:
					{
						if(!tweening && !dragging)
						{
							draggingSprite = currentSprite;
							draggingSprite.parent.setChildIndex(draggingSprite,draggingSprite.parent.numChildren-1);
							armatures[index].animation.gotoAndPlay("selected");
							dragging = true;
							returnPoint.setTo(draggingSprite.x,draggingSprite.y);
							
							draggingSprite.x = touch.globalX - Costanza.STAGE_OFFSET;
							draggingSprite.y = touch.globalY;
						}
						break;
					}
					case TouchPhase.MOVED:
					{
						if(dragging)
						{
							draggingSprite.x = touch.globalX - Costanza.STAGE_OFFSET;
							draggingSprite.y = touch.globalY;
						}
						break;
					}
					case TouchPhase.ENDED:
					{
						if(dragging)
						{
							dragging = false;
							if(draggingSprite.getBounds(draggingSprite.parent).intersects(arrayHitBoxes[index].getBounds(arrayHitBoxes[index].parent)))
							{
								soundManager.playSound("Food_" + draggingSprite.name, Costanza.voiceVolume);
								//soundManager.playSound("Correct");
								correctCount++;
								armatures[index].animation.gotoAndPlay("correct");
								TweenMax.to(draggingSprite,.5,{x:arrayHitBoxes[index].x + draggingSprite.width/2,y:arrayHitBoxes[index].y + draggingSprite.height/2});
								TweenMax.to(arrayHitBoxes[index],.4,{alpha:0, onComplete:checkDone});
								draggingSprite.removeEventListener(TouchEvent.TOUCH,foodTouch);
							}
							else
							{
								soundManager.playSound("Wrong",Costanza.soundVolume);
								tweening = true;
								armatures[index].animation.gotoAndPlay("wrong");
							}
						}
						break;
					}
				}
			}
		}
		
		private function frameEventHandler(evt:FrameEvent):void
		{
			if(evt.frameLabel == "wrongDone")
			{
				var index:int = int(evt.currentTarget.name);
				TweenMax.to(arrayFood[index],.5,{x:returnPoint.x,y:returnPoint.y, onComplete:setTweening});
				//tweening = false;
			}
		}
		
		private function setTweening():void
		{
			tweening = false;
			draggingSprite = null;
		}
		
		private function checkDone():void
		{
			draggingSprite = null;
			if(correctCount == totalPieces)
			{
				for(var i:int = 0; i < totalPieces; i++)
				{
					armatures[i].animation.gotoAndPlay("dance");
				}
				soundManager.playSound("Song",Costanza.soundVolume);
				particle.CreateBurst(70,500,500,5,true,true);
				TweenMax.delayedCall(.2,tweenBurst);
				TweenMax.delayedCall(15,playDone);
				soundManager.playSound("Burst",Costanza.soundVolume);
			}
		}
		
		private function playDone():void
		{
			soundManager.playSound("Done",Costanza.voiceVolume);
			TweenMax.delayedCall(3,nextPage);
		}
		
		private function tweenBurst():void
		{
			TweenMax.to(starBurst,1,{scaleX:10,scaleY:10, alpha:0});
		}
		
		private function onAddedToStage(evt:starling.events.Event):void
		{
			// Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED,true);
		}
		
		private function onButtonTriggered():void
		{
			//dispatchEventWith(LOAD_LOBBY, true);
			dispatchEventWith(SET_CHARACTER, true, characterString);
			dispatchEventWith(LOAD_ROOM, true, 2);
		}
		
		private function onEnterFrameHandler(evt:EnterFrameEvent):void
		{
			WorldClock.clock.advanceTime(-1);
		}
		
		public function disposeScene():void
		{
			assets.removeTexture("textureMatching" + foodString,true);
			
			TweenMax.killAll(true);
			textureAtlas.dispose();
			factory.dispose(true);
			App.backPressed.remove(BackPressed);
			var len:int = armatures.length;
			for (var i:int = 0; i < len; i++)
			{
				WorldClock.clock.remove(armatures[i]);
				armatures[i].dispose();
			}
			armatures.length = 0;
			sceneRoot.removeChildren(0,sceneRoot.numChildren-1,true);
			
			assets.dispose();
		}
		
		private function nextPage():void
		{
			var cleanup:Boolean = true;
			for(var i:int = 0; i < Costanza.characterPlaythroughs.length && cleanup; i++)
			{
				if(Costanza.characterPlaythroughs[i] == 0){cleanup = false;}
			}
			
			if(cleanup || Costanza.overallPlays >= 7)
			{
				Costanza.overallPlays = 0;
				Costanza.characterPlaythroughs = [0,0,0,0,0];
				dispatchEventWith(LOAD_ROOM, true, 4);
			}
			else
			{
				dispatchEventWith("loadLobby", true);
			}
		}
		
		private function BackPressed():void
		{
			dispatchEventWith("loadLobby", true);
		}
		
		private function buildArmatures(num:int):void
		{
			for(var i:int = 1; i <= num; i++)
			{
				// Build the armatures
				var tmpArm:Armature = factory.buildArmature("Characters/Food_" + i);
				tmpArm.animation.gotoAndPlay("idle");
				tmpArm.display.x = foodPositions[i-1].x;
				tmpArm.display.y = foodPositions[i-1].y;
				tmpArm.name = (i-1).toString();
				WorldClock.clock.add(tmpArm);
				armatures.push(tmpArm);
				tmpArm.addEventListener(FrameEvent.MOVEMENT_FRAME_EVENT, frameEventHandler);//listens for frame events
				//sprite for armature
				var tmpArmClip:Sprite = tmpArm.display as Sprite;
				tmpArmClip.name = (i-1).toString();
				tmpArmClip.addEventListener(TouchEvent.TOUCH, foodTouch);
				arrayFood.push(tmpArmClip);
				
				trace("NAMENAME" + tmpArmClip.name);
				
				var tmpArr:Array = setArray();
				
				trace(tmpArr[i-1]);
				//image for dragging to
				var tmpImg:Image = new Image(assets.getTexture("Hole_" + i));
				tmpImg.x = tmpArr[i-1].x;
				tmpImg.y = tmpArr[i-1].y;
				arrayHitBoxes.push(tmpImg);
				sceneRoot.addChild(tmpImg);
				
				sceneRoot.addChild(tmpArmClip);//add image after image hole
			}
			sceneRoot.addChild(starBurst);//add starburst behind char
			sceneRoot.addChild(particle);
			starBurst.touchable = false;
			particle.touchable = false;
		}
		
		private function setArray():Array
		{
			var index:int;
			if(foodString == "Sandwich"){index = 0;}
			else if(foodString == "Yogurt"){index = 1;}
			else if(foodString == "Noodles"){index = 2;}
			else if(foodString == "Pizza"){index = 3;}
			else if(foodString == "Sushi"){index = 4;}
			else if(foodString == "Salad"){index = 5;}
			else{index = 6;}
			
			var tmpArray:Array = [];
			switch(characterString)
			{
				case "Brobee":
					tmpArray = brobeePositions;
					break;
				case "Foofa":
					tmpArray = foofaPositions;
					break;
				case "Muno":
					tmpArray = munoPositions;
					break;
				case "Plex":
					tmpArray = plexPositions;
					break;
				case "Toodee":
					tmpArray = toodeePositions;
					break;
			}
			
			return tmpArray[index];
		}
	}
}