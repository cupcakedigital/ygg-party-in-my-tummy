package scenes.CleanUp
{
	import com.cupcake.App;
	import com.greensock.TweenMax;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.system.Capabilities;
	
	import dragonBones.Armature;
	import dragonBones.animation.WorldClock;
	import dragonBones.factorys.StarlingFactory;
	import dragonBones.objects.ObjectDataParser;
	import dragonBones.objects.SkeletonData;
	import dragonBones.textures.StarlingTextureAtlas;
	
	import feathers.controls.Button;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.PDParticleSystem;
	import starling.extensions.SoundManager;
	import starling.textures.Texture;
	import starling.utils.AssetManager;

	public class CleanUp extends Sprite
	{
		// Scene Objects
		private var sceneRoot:Sprite;
		private var bg:Image;
		private var backButton:Button;
		private var soundManager:SoundManager;
		
		private var currentCharacter:String;
		
		private var assets:AssetManager;
		private var appDir:File = File.applicationDirectory;
		
		// Standard display
		private var stageWidth:int;
		private var stageHeight:int;
		
		
		// Bubble Events
		public static const LOAD_LOBBY:String = "loadLobby";
		public static const SET_CHARACTER:String = "setCharacter";
		public static const LOAD_ROOM:String = "loadRoom";
		
		private var soundTap:int = 1;
		
		// Dragon Bones
		
		private var factory:StarlingFactory;
		private var armatures:Array = [];
		
		private var canArmature:Armature;
		private var canArmatureClip:Sprite;
		
		//scene vars
		private var characterString:String;
		private var charTaps:int = 1;
		
		private var charPositions:Array = [new Point(770,560),new Point(400,550),new Point(550,530),new Point(850,230),new Point(250,550)];
		private var trashPositions:Array = [new Point(750,560),new Point(560,100),new Point(880,120),new Point(370,80),new Point(420,580),new Point(1100,120)];
		
		private var trashArray:Array = [];
		
		private var hitbox:Quad;
		
		private var dragging:Boolean = false;
		
		private var trashCleaned:int = 0;
		
		private var trashSparkle:PDParticleSystem;
		
		private var textureAtlas:StarlingTextureAtlas;
		
		private var assetsLoaded:Boolean = false;
		
		public function CleanUp(charString:String, foodString:String)
		{
			addEventListener(Root.DISPOSE,  disposeScene);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.ACTIVATE,screenActivated);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.DEACTIVATE,screenDeactivated);
			
			characterString = charString;
			
			trace("STAGE OFFSET" + Costanza.STAGE_OFFSET);
			//Costanza.SCALE_FACTOR = 2;
			
			// Create new asset manager and load content
			assets = new AssetManager(Costanza.SCALE_FACTOR);
			assets.useMipMaps = Costanza.mipmapsEnabled;
			assets.verbose = Capabilities.isDebugger;
			assets.enqueue(
				appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR +"x/CleanUp"),
				appDir.resolvePath("audio/CleanUp"),
				appDir.resolvePath("audio/ARoomSounds/Brobee/Brobee_Tap_1.mp3"),
				appDir.resolvePath("audio/ARoomSounds/Foofa/Foofa_Tap_1.mp3"),
				appDir.resolvePath("audio/ARoomSounds/Muno/Muno_Tap_1.mp3"),
				appDir.resolvePath("audio/ARoomSounds/Plex/Plex_Tap_1.mp3"),
				appDir.resolvePath("audio/ARoomSounds/Toodee/Toodee_Tap_1.mp3"),
				appDir.resolvePath("particles/TrashSparkle.pex"),
				appDir.resolvePath("particles/starSparklesTexture.png")
				
			);
			
			assets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1){
					assetsLoaded = true;
					trace("finished loading character");
					addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrameHandler);
					addObjects();
				};
				
			});
		}
		
		private function screenActivated(e:flash.events.Event):void
		{
			trace("----------------------------SCENE ACTIVATED-----------------------------");
			if(!assetsLoaded)
			{
				assets.enqueue(
					appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR +"x/CleanUp"),
					appDir.resolvePath("audio/CleanUp"),
					appDir.resolvePath("audio/ARoomSounds/Brobee/Brobee_Tap_1.mp3"),
					appDir.resolvePath("audio/ARoomSounds/Foofa/Foofa_Tap_1.mp3"),
					appDir.resolvePath("audio/ARoomSounds/Muno/Muno_Tap_1.mp3"),
					appDir.resolvePath("audio/ARoomSounds/Plex/Plex_Tap_1.mp3"),
					appDir.resolvePath("audio/ARoomSounds/Toodee/Toodee_Tap_1.mp3"),
					appDir.resolvePath("particles/TrashSparkle.pex"),
					appDir.resolvePath("particles/starSparklesTexture.png")
				);
				assets.loadQueue(function onProgress(ratio:Number):void
				{
					if (ratio == 1){
						assetsLoaded = true;
						trace("finished loading character");
						addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrameHandler);
						addObjects();
					};
					
				});
			}
		}
		
		private function screenDeactivated(e:flash.events.Event):void
		{
			trace("----------------------------SCENE DEACTIVATED-----------------------------");
			if(!assetsLoaded)
			{
				assets.purgeQueue();
			}
		}
		
		private function addObjects():void
		{
			trace("audio/" + characterString + "CharacterRoom");
			// Create main scene root
			sceneRoot = new Sprite();
			sceneRoot.x = Costanza.STAGE_OFFSET;
			
			addChild(sceneRoot);
			
			soundManager = SoundManager.getInstance();
			
			//------------------
			//ADD SOUNDS
			//------------------
			soundManager.addSound("Brobee_1", assets.getSound("Brobee_Tap_1"));
			
			soundManager.addSound("Foofa_1", assets.getSound("Foofa_Tap_1"));
			
			soundManager.addSound("Muno_1", assets.getSound("Muno_Tap_1"));
			
			soundManager.addSound("Plex_1", assets.getSound("Plex_Tap_1"));
			
			soundManager.addSound("Toodee_1", assets.getSound("Toodee_Tap_1"));
			
			soundManager.addSound("Throw_Out", assets.getSound("Throw_Out"));
			
			soundManager.addSound("Throw_Out_VO_1", assets.getSound("Throw_Out_VO_1"));
			soundManager.addSound("Throw_Out_VO_2", assets.getSound("Throw_Out_VO_2"));
			soundManager.addSound("Throw_Out_VO_3", assets.getSound("Throw_Out_VO_3"));
			soundManager.addSound("Throw_Out_VO_4", assets.getSound("Throw_Out_VO_4"));
			soundManager.addSound("Throw_Out_VO_5", assets.getSound("Throw_Out_VO_5"));
			
			soundManager.addSound("Can_Tap_1", assets.getSound("Can_Tap_1"));
			soundManager.addSound("Can_Tap_2", assets.getSound("Can_Tap_2"));
			soundManager.addSound("Can_Tap_3", assets.getSound("Can_Tap_3"));
			soundManager.addSound("Can_Tap_4", assets.getSound("Can_Tap_4"));
			
			soundManager.addSound("CleanUpLoop", assets.getSound("CleanUpLoop"));
			
			soundManager.playSound("CleanUpLoop",Costanza.musicVolume,999);
			
			var bg:Image = new Image(assets.getTexture("BG"));
			bg.x = Costanza.STAGE_OFFSET * -1;
			sceneRoot.addChild(bg);
			
			trashSparkle = new PDParticleSystem(assets.getXml("TrashSparkle"), assets.getTexture("starSparklesTexture"));
			
			sceneRoot.addChild(trashSparkle);
			Starling.juggler.add(trashSparkle);
			
			// DragonBones Setup
			factory = new StarlingFactory();
			
			var skeletonData:SkeletonData = ObjectDataParser.parseSkeletonData(assets.getObject("skeletonCleanup"));
			factory.addSkeletonData(skeletonData);
			
			var texture:Texture = assets.getTexture("textureCleanup");
			textureAtlas = new StarlingTextureAtlas(texture, assets.getObject("textureCleanup"),true);
			factory.addTextureAtlas(textureAtlas);
			
			var numFaces:int = 3;
			
			buildArmatures(numFaces);
			
			// Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED,true);
			
			addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
			
			App.backPressed.add(BackPressed);
			
			TweenMax.delayedCall(5, sparkleTrash);
		}
		
		private function sparkleTrash():void
		{
			for(var i:int = 0; i < trashArray.length; i++)
			{
				if(trashArray[i].scaleX == 1)
				{
					trashSparkle.emitterX = trashArray[i].x; 
					trashSparkle.emitterY = trashArray[i].y; 
					trashSparkle.start();
				}
			}
		}
		
		private function onAddedToStage(evt:starling.events.Event):void
		{
			// Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED,true);
		}
		
		private function onButtonTriggered():void
		{
			//dispatchEventWith(LOAD_LOBBY, true);
			//dispatchEventWith(SET_CHARACTER, true, characterString);
			dispatchEventWith(LOAD_ROOM, true, 3);
		}
		
		private function onButtonTriggeredNext():void
		{
			dispatchEventWith(SET_CHARACTER, true, characterString);
			dispatchEventWith(LOAD_ROOM, true, 1);
		}
		
		private function dragHandler(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(stage);
			
			for each (var touch:Touch in touches)
			{
				if(touch == null){return;}
				var currentImage:Image = e.currentTarget as Image;
				var index:int = int(currentImage.name);
				
				switch(touch.phase)
				{
					case TouchPhase.BEGAN:
					{
						if(!dragging)
						{
							TweenMax.killDelayedCallsTo(sparkleTrash);
							trashSparkle.stop();
							dragging = true;
							currentImage.x = touch.globalX - Costanza.STAGE_OFFSET;
							currentImage.y = touch.globalY;
						}
						break;
					}
					case TouchPhase.MOVED:
					{
						if(dragging)
						{
							currentImage.x = touch.globalX - Costanza.STAGE_OFFSET;
							currentImage.y = touch.globalY;
						}
						break;
					}
					case TouchPhase.ENDED:
					{
						if(dragging)
						{
							dragging = false;
							if(currentImage.getBounds(currentImage.parent).intersects(hitbox.getBounds(hitbox.parent)))
							{
								trashCleaned++;
								soundManager.playSound("Throw_Out", Costanza.soundVolume);
								var num:int = trashCleaned;
								if(num > 5){num = 1;}
								soundManager.playSound("Throw_Out_VO_" + num, Costanza.voiceVolume);
								canArmature.animation.gotoAndPlay("idle");
								currentImage.removeEventListener(TouchEvent.TOUCH,dragHandler);
								sceneRoot.setChildIndex(currentImage,sceneRoot.getChildIndex(canArmatureClip)-1);
								TweenMax.to(currentImage,0.4,{bezierThrough:[{x:hitbox.x + hitbox.width/2, y:hitbox.y},{x:canArmatureClip.x,y:canArmatureClip.y + canArmatureClip.height/2, scaleX:.4, scaleY:.4}],orientToBezier:false,onComplete:setVisible,onCompleteParams:[currentImage]});
								
								if(trashCleaned == 6)
								{
									TweenMax.delayedCall(4,backToLobby);
								}
							}
							else
							{
								TweenMax.to(currentImage,0.5,{x:trashPositions[index].x, y:trashPositions[index].y});
							}
							
							TweenMax.delayedCall(5, sparkleTrash);
						}
						break;
					}
				}
			}
		}
		
		private function setVisible(img:Image):void
		{
			img.visible = false;
		}
		
		private function BackPressed():void
		{
			backToLobby();
		}
		
		private function backToLobby():void
		{
			soundManager.getSoundChannel("CleanUpLoop").stop();
			dispatchEventWith("loadLobby", true);
		}
		
		private function canTouch(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.BEGAN);
			
			if(touches.length != 1){trace("too long" + touches.length);return;}
			
			var touch:Touch = touches[0];
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				canArmature.animation.gotoAndPlay("bounce" + charTaps);
				soundManager.playSound("Can_Tap_" + charTaps,Costanza.soundVolume);
				charTaps++;
				if(charTaps > 4)
				{
					charTaps=1;
				}
			}
			
		}
		
		private function tapChar(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.BEGAN);
			
			if(touches.length != 1){trace("too long" + touches.length);return;}
			
			var touch:Touch = touches[0];
			if(touch == null){return;}
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				switch((e.currentTarget as Sprite).name)
				{
					case "1":
					{
						if(!soundManager.soundIsPlaying("Foofa_1"))
						{
							soundManager.playSound("Foofa_1");
						}
						break;
					}
					case "2":
					{
						if(!soundManager.soundIsPlaying("Muno_1"))
						{
							soundManager.playSound("Muno_1");
						}
						break;
					}
					case "3":
					{
						if(!soundManager.soundIsPlaying("Plex_1"))
						{
							soundManager.playSound("Plex_1");
						}
						break;
					}
					case "4":
					{
						
						if(!soundManager.soundIsPlaying("Toodee_1"))
						{
							soundManager.playSound("Toodee_1");
						}
						break;
					}
					case "0":
					{
						
						if(!soundManager.soundIsPlaying("Brobee_1"))
						{
							soundManager.playSound("Brobee_1");
						}
						break;
					}
				}
			}
		}
		
		private function buildArmatures(num:int):void
		{
			for(var i:int = 1; i <= 5; i++)
			{
				//add character
				var charArmature:Armature = factory.buildArmature("Characters/Character_" + i);
				charArmature.display.x = charPositions[i-1].x;
				charArmature.display.y = charPositions[i-1].y;
				charArmature.animation.gotoAndPlay("dance");
				WorldClock.clock.add(charArmature);
				armatures.push(charArmature);
				//sprite for armature
				var charArmatureClip:Sprite = charArmature.display as Sprite;
				charArmatureClip.name = String(armatures.length-1);//set name to index
				sceneRoot.addChild(charArmatureClip);
				charArmatureClip.addEventListener(TouchEvent.TOUCH,tapChar);
			}
			
			var ol1:Image = new Image(assets.getTexture("OL1"));
			ol1.x = 100;
			ol1.y = 500;
			sceneRoot.addChild(ol1);
			
			var ol2:Image = new Image(assets.getTexture("OL2"));
			ol2.x = 900;
			ol2.y = 500;
			sceneRoot.addChild(ol2);
			
			//add trash can
			canArmature = factory.buildArmature("Characters/GarbageCan");
			canArmature.display.x = 1030;
			canArmature.display.y = 360;
			canArmature.animation.gotoAndPlay("idle");
			WorldClock.clock.add(canArmature);
			armatures.push(canArmature);
			//sprite for armature
			canArmatureClip = canArmature.display as Sprite;
			canArmatureClip.name = "Can";//set name to index
			sceneRoot.addChild(canArmatureClip);
			canArmatureClip.addEventListener(TouchEvent.TOUCH, canTouch);
			
			//hitbox for garbage dropping
			hitbox = new Quad(canArmature.display.width,canArmature.display.height);
			hitbox.x = canArmature.display.x - canArmature.display.width/3;
			hitbox.y = canArmature.display.y - canArmature.display.height/2;
			hitbox.alpha = 0;
			sceneRoot.addChild(hitbox);
			
			//trash
			for(var j:int = 1; j <= 6; j++)
			{
				var img:Image = new Image(assets.getTexture("Trash_" + j));
				img.pivotX = img.width/2;
				img.pivotY = img.height/2;
				img.x = trashPositions[j-1].x;
				img.y = trashPositions[j-1].y;
				img.name = String(j-1);
				img.addEventListener(TouchEvent.TOUCH,dragHandler);
				sceneRoot.addChild(img);
				trashArray.push(img);
			}
		}
		
		private function onEnterFrameHandler(evt:EnterFrameEvent):void
		{
			WorldClock.clock.advanceTime(-1);
		}
		
		public function disposeScene():void
		{
			assets.removeTexture("textureCleanup",true);
			TweenMax.killAll(true);
			textureAtlas.dispose();
			Starling.juggler.purge();
			factory.dispose(true);
			App.backPressed.remove(BackPressed);
			var len:int = armatures.length;
			for (var i:int = 0; i < len; i++)
			{
				WorldClock.clock.remove(armatures[i]);
				armatures[i].dispose();
			}
			armatures.length = 0;
			sceneRoot.removeChildren(0,sceneRoot.numChildren-1,true);
			
			assets.dispose();
		}
	}
}