package scenes.FeedCharacter  {
	
	import com.cupcake.App;
	import com.cupcake.DeviceInfo;
	import com.greensock.TweenMax;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.system.Capabilities;
	
	import dragonBones.Armature;
	import dragonBones.Bone;
	import dragonBones.animation.WorldClock;
	import dragonBones.factorys.StarlingFactory;
	import dragonBones.objects.ObjectDataParser;
	import dragonBones.objects.SkeletonData;
	import dragonBones.textures.StarlingTextureAtlas;
	
	import feathers.controls.Button;
	
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	
	import utils.ParticleSystemJP.ParticleEmitterJP;
	
	public class FeedingRoom extends Sprite {
		
		// Scene Objects
		private var sceneRoot:Sprite;
		private var bg:Image;
		private var backButton:Button;
		
		private var currentCharacter:String;
		
		private var assets:AssetManager;
		private var appDir:File = File.applicationDirectory;
		
		// Standard display
		private var stageWidth:int;
		private var stageHeight:int;
		
		// Bubble Events
		public static const LOAD_LOBBY:String = "loadLobby";
		public static const SET_CHARACTER:String = "setCharacter";
		public static const LOAD_ROOM:String = "loadRoom";
		
		// Dragon Bones
		private var factory:StarlingFactory;
		private var armatures:Array = [];
		private var sushiArmatures:Array = [];
		
		//scene vars
		private var characterString:String;
		private var foodString:String;
		private var charTaps:int = 0;
		private var dragging:Boolean = false;
		private var tweening:Boolean = false;
		private var startPnt:Point;
		private var startPnt2:Point;
		
		private var dragPiece:Sprite;
		
		private var utensil:MovieClip;
		
		private var hitbox:Quad;
		
		private var soundmanager:SoundManager;
		
		//arrays for face positions
		private var brobeeArray:Array = [new Point(400,360),new Point(865,300),new Point(895,320),new Point(120,300)];//good
		private var foofaArray:Array = [new Point(900,380),new Point(980,540),new Point(95,455)];//good
		private var munoArray:Array = [new Point(920,280),new Point(1140,150)];//good
		private var plexArray:Array = [new Point(80,420),new Point(980,260),new Point(1138,260)];//good
		private var toodeeArray:Array = [new Point(1086,480),new Point(280,250)];
		
		private var hitboxYPositions:Array = [200,310,300,400,300];
		
		private var foodPositionsBrobee:Array = [new Point(350,430),new Point(350,400),new Point(350,400),new Point(380,450),new Point(370,500),new Point(350,400),new Point(350,400)];
		private var foodPositionsFoofa:Array = [new Point(450,430),new Point(430,400),new Point(430,400),new Point(480,450),new Point(450,480),new Point(430,400),new Point(430,400)];
		private var foodPositionsMuno:Array = [new Point(430,430),new Point(410,400),new Point(410,400),new Point(460,450),new Point(430,480),new Point(410,400),new Point(410,400)];
		private var foodPositionsPlex:Array = [new Point(950,450),new Point(930,430),new Point(930,430),new Point(980,470),new Point(940,510),new Point(930,430),new Point(930,430)];
		private var foodPositionsToodee:Array = [new Point(420,500),new Point(380,450),new Point(380,450),new Point(430,480),new Point(400,540),new Point(380,450),new Point(380,450)];
		
		private var spoon:Boolean = false;
		
		private var feedCount:int = 0;
		
		private var bowlArmature:Armature;
		
		private var treeTaps:int = 1;
		private var numTreeTaps:int;
		private var flowerTapNum:int = 1;
		
		private var particle:ParticleEmitterJP;
		
		private var particlesArray:Array = new Array();
		
		private var starBurst:Image;
		
		private var foodIndex:int;
		private var food2Index:int;
		
		private var draggingSprite:Sprite;
		
		private var foodTextureAtlas:StarlingTextureAtlas;
		private var textureAtlas:StarlingTextureAtlas;
		
		private var assetsLoaded:Boolean = false;
		
		public function FeedingRoom(charString:String = "Brobee", foodStr:String = "Sandwich") {
			
			addEventListener(Root.DISPOSE,  disposeScene);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.ACTIVATE,screenActivated);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.DEACTIVATE,screenDeactivated);
			
			characterString = charString;
			foodString = foodStr;
			
			// Create new asset manager and load content
			assets = new AssetManager(Costanza.SCALE_FACTOR);
			assets.useMipMaps = Costanza.mipmapsEnabled;
			assets.verbose = Capabilities.isDebugger;
			assets.enqueue(
				appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR +"x/FeedRoom/" + characterString),
				appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR +"x/CharacterRoom/" + characterString + "/assets/BG.png"),
				appDir.resolvePath("audio/CRoomSounds/" + characterString),
				appDir.resolvePath("audio/CRoomSounds/Global"),
				appDir.resolvePath("audio/Trees_" + characterString),
				appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR +"x/FeedRoom/FoodDB"),
				appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Bursts/" + characterString + "_Burst.png")
			);
			
			assets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1){
					assetsLoaded = true;
					trace("finished loading character");
					addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrameHandler);
					addObjects();
				};
				
			});
			
		}
		
		private function screenActivated(e:flash.events.Event):void
		{
			trace("----------------------------SCENE ACTIVATED-----------------------------");
			if(!assetsLoaded)
			{
				assets.enqueue(
					appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR +"x/FeedRoom/" + characterString),
					appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR +"x/CharacterRoom/" + characterString + "/assets/BG.png"),
					appDir.resolvePath("audio/CRoomSounds/" + characterString),
					appDir.resolvePath("audio/CRoomSounds/Global"),
					appDir.resolvePath("audio/Trees_" + characterString),
					appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR +"x/FeedRoom/FoodDB"),
					appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Bursts/" + characterString + "_Burst.png")
				);
				assets.loadQueue(function onProgress(ratio:Number):void
				{
					if (ratio == 1){
						assetsLoaded = true;
						trace("finished loading character");
						addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrameHandler);
						addObjects();
					};
					
				});
			}
		}
		
		private function screenDeactivated(e:flash.events.Event):void
		{
			trace("----------------------------SCENE DEACTIVATED-----------------------------");
			if(!assetsLoaded)
			{
				assets.purgeQueue();
			}
		}
		
		private function addObjects():void
		{
			soundmanager = SoundManager.getInstance();
			// Create main scene root
			sceneRoot = new Sprite();
			sceneRoot.x = Costanza.STAGE_OFFSET;
			
			addChild(sceneRoot);
			
			var bg:Image = new Image(assets.getTexture("BG"));
			bg.x = Costanza.STAGE_OFFSET * -1;
			sceneRoot.addChild(bg);
			
			loadSounds();
			
			soundmanager.addSound("Chew", assets.getSound("Chew"));
			if(characterString == "Plex")
			{
				soundmanager.addSound("Open", assets.getSound("Open"));
			}
			
			soundmanager.addSound("Chew_Woah_1", assets.getSound("Chew_Woah_1"));
			if(characterString != "Toodee"){soundmanager.addSound("Chew_Woah_2", assets.getSound("Chew_Woah_2"));}
			
			soundmanager.addSound("Lobby_C_Music",Root.assets.getSound("Lobby_C_Music"));
			soundmanager.playSound("Lobby_C_Music",Costanza.musicVolume,999);
			
			soundmanager.addSound("Burst", Root.assets.getSound("Burst"));
			
			soundmanager.addSound("Food_VO_01", assets.getSound("Food_VO_01"));
			soundmanager.addSound("Food_VO_02", assets.getSound("Food_VO_02"));
			
			// DragonBones Setup
			factory = new StarlingFactory();
			
			var skeletonData:SkeletonData = ObjectDataParser.parseSkeletonData(assets.getObject("skeletonFeed" + characterString));
			factory.addSkeletonData(skeletonData);
			
			var texture:Texture = assets.getTexture("textureFeed" + characterString);
			textureAtlas = new StarlingTextureAtlas(texture, assets.getObject("textureFeed" + characterString),true);
			factory.addTextureAtlas(textureAtlas);
			
			var foodSkeletonData:SkeletonData = ObjectDataParser.parseSkeletonData(assets.getObject("foodSkeleton"));
			factory.addSkeletonData(foodSkeletonData);
			
			var foodTexture:Texture = assets.getTexture("foodTexture");
			foodTextureAtlas = new StarlingTextureAtlas(foodTexture, assets.getObject("foodTexture"),true);
			factory.addTextureAtlas(foodTextureAtlas);
			
			var numFaces:int = 3;
			
			if(characterString == "Brobee"){numFaces++;}
			else if(characterString == "Toodee" || characterString == "Muno"){numFaces--;}
			
			starBurst = new Image(assets.getTexture(characterString + "_Burst"));
			
			starBurst.pivotX = starBurst.width/2;
			starBurst.pivotY = starBurst.height/2;
			starBurst.x = Costanza.STAGE_WIDTH/2 - Costanza.STAGE_OFFSET;
			starBurst.y = Costanza.STAGE_HEIGHT/2;
			
			sceneRoot.addChild(starBurst);
			starBurst.scaleX = 0;
			starBurst.scaleY = 0;
			
			for(var i:int = 1; i <= 21; i++)
			{
				particlesArray.push(new Image(Root.assets.getTexture("ygg-confetti_000" + i)));
			}
			
			particle = new ParticleEmitterJP(particlesArray,false);
			
			particle.x = Costanza.STAGE_WIDTH/2 - Costanza.STAGE_OFFSET;
			particle.y = Costanza.STAGE_HEIGHT/2;
			
			buildArmatures(numFaces);
			
			App.backPressed.add(BackPressed);
			
			// Main scene back/exit button
			
			// Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED,true);
			
			addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
			
			TweenMax.delayedCall(5, animateFood);
		}
		
		private function animateFood():void
		{
			if(spoon)
			{
				bowlArmature.animation.gotoAndPlay("select1");
			}
			else
			{
				armatures[foodIndex].animation.gotoAndPlay("selected");
				armatures[food2Index].animation.gotoAndPlay("selected");
			}
		}
		
		private function loadSounds():void
		{
			numTreeTaps = 6;
			if(characterString == "Muno"){numTreeTaps--;}
			else if(characterString == "Toodee"){numTreeTaps = 3;}
			
			for(var i:int=1; i <= numTreeTaps; i++)
			{
				soundmanager.addSound("Tree_" + i, assets.getSound("Tree_" + i));
			}
			
			if(characterString == "Foofa")
			{
				soundmanager.addSound("Flower_1", assets.getSound("Flower_1"));
				soundmanager.addSound("Flower_2", assets.getSound("Flower_2"));
				soundmanager.addSound("Flower_3", assets.getSound("Flower_3"));
			}
		}
		
		private function onAddedToStage(evt:starling.events.Event):void
		{
			// Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED,true);
		}
		
		private function onButtonTriggered():void
		{
			//dispatchEventWith(LOAD_LOBBY, true);
			dispatchEventWith(SET_CHARACTER, true, characterString);
			dispatchEventWith(LOAD_ROOM, true, 1);
		}
		
		private function onButtonTriggeredNext():void
		{
			dispatchEventWith(SET_CHARACTER, true, characterString);
			dispatchEventWith(LOAD_ROOM, true, 3);
		}
		
		private function faceTouch(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.BEGAN);
			
			if(touches.length != 1){return;}
			
			var touch:Touch = touches[0];
			if(touch == null){return;}
			var currSprite:Sprite = e.currentTarget as Sprite;
			var index:int = int(currSprite.name);
			var currArmature:Armature = armatures[index];
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				if(currArmature.animation.movementID == "face1"){currArmature.animation.gotoAndPlay("face2");}
				else if(currArmature.animation.movementID == "face2"){currArmature.animation.gotoAndPlay("face3");}
				else if(currArmature.animation.movementID == "face3"){currArmature.animation.gotoAndPlay("face1");}
				
				soundmanager.playSound("Tree_" + treeTaps,Costanza.soundVolume);
				treeTaps++;
				if(treeTaps > numTreeTaps)
				{
					treeTaps = 1;
				}
			}
		}
		
		private function tapChar(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.BEGAN);
			
			if(touches.length != 1){trace("too long" + touches.length);return;}
			
			var touch:Touch = touches[0];
			if(touch == null){return;}
			var currSprite:Sprite = e.currentTarget as Sprite;
			var index:int = int(currSprite.name);
			var currArmature:Armature = armatures[index];
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				if(currArmature.animation.movementID != "happy" && currArmature.animation.movementID != "chew")
				{
					currArmature.animation.gotoAndPlay("look");
				}
			}
		}
		
		private function flowerTap(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.BEGAN);
			
			if(touches.length != 1){trace("too long" + touches.length);return;}
			
			var touch:Touch = touches[0];
			if(touch == null){return;}
			var currSprite:Sprite = e.currentTarget as Sprite;
			var index:int = int(currSprite.name);
			var currArmature:Armature = armatures[index];
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				if(currArmature.animation.movementID == "face1"){armatures[index].animation.gotoAndPlay("face2");}
				else if(currArmature.animation.movementID == "face2"){armatures[index].animation.gotoAndPlay("face3");}
				else{armatures[index].animation.gotoAndPlay("face1");}
				
				soundmanager.playSound("Flower_" + flowerTapNum, Costanza.soundVolume);
				
				flowerTapNum++;
				if(flowerTapNum > 3){flowerTapNum = 1;}
			}
		}
		
		private function touchHandler(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(stage);
			
			for each (var touch:Touch in touches)
			{
				if(touch == null){return;}
				var currSprite:Sprite = e.currentTarget as Sprite;
				
				if(currSprite == null){return;}
				
				var index:int = int(currSprite.name);
				var currArmature:Armature = armatures[index];
				
				if(spoon)
				{
					switch(foodString)
					{
						case "Noodles":(dragPiece.getChildAt(0) as MovieClip).currentFrame = 2;break;
						case "Salad":(dragPiece.getChildAt(0) as MovieClip).currentFrame = 1;break;
						case "Yogurt":(dragPiece.getChildAt(0) as MovieClip).currentFrame = 2;break;
						case "Fruitsalad":(dragPiece.getChildAt(0) as MovieClip).currentFrame = 1;break;
					}
					currSprite = dragPiece;
				}
				if(foodString == "Sushi" || foodString == "Sandwich"){currArmature = sushiArmatures[index];}
				
				if(draggingSprite != null && currSprite != draggingSprite){return;}
				
				switch(touch.phase)
				{
					case TouchPhase.BEGAN:
					{
						if(!dragging && feedCount < 2)
						{
							draggingSprite = currSprite;
							TweenMax.killDelayedCallsTo(animateFood);
							
							if(soundmanager.soundIsPlaying("Food_VO_01")){soundmanager.getSoundChannel("Food_VO_01").stop();}
							else if(soundmanager.soundIsPlaying("Food_VO_02")){soundmanager.getSoundChannel("Food_VO_02").stop();}
							//else if(soundmanager.soundIsPlaying("Chew_Woah_1")){soundmanager.getSoundChannel("Chew_Woah_1").stop();}
							
							soundmanager.playSound("Food_VO_0" + (feedCount+1), Costanza.voiceVolume);
							armatures[0].animation.gotoAndPlay("mouthOpen");
							
							if(characterString == "Plex")
							{
								if(soundmanager.soundIsPlaying("Open")){soundmanager.getSoundChannel("Open").stop();}
								soundmanager.playSound("Open",Costanza.soundVolume);
							}
							
							if(dragPiece.alpha == 0){dragPiece.alpha=1;}
							if(spoon){dragPiece.visible=true; TweenMax.killTweensOf(dragPiece); TweenMax.killDelayedCallsTo(tweenFood);TweenMax.killDelayedCallsTo(tweenUtensil);}
							dragging = true;
							if(bowlArmature){bowlArmature.animation.gotoAndPlay("select" + (feedCount+1));}
							currArmature.animation.gotoAndPlay("selected");
							
							draggingSprite.x = touch.globalX - Costanza.STAGE_OFFSET;
							draggingSprite.y = touch.globalY;
						}
						break;
					}
					case TouchPhase.MOVED:
					{
						if(!draggingSprite)
						{
							dragging = false;
						}
						else if(dragging)
						{
							draggingSprite.x = touch.globalX - Costanza.STAGE_OFFSET;
							draggingSprite.y = touch.globalY;
						}
						break;
					}
					case TouchPhase.ENDED:
					{
						if(dragging && draggingSprite)
						{
							if(draggingSprite.getBounds(draggingSprite.parent).intersects(hitbox.getBounds(hitbox.parent)))
							{
								tweening = true;
								armatures[0].animation.gotoAndPlay("chew");
								soundmanager.playSound("Chew", Costanza.soundVolume);
	
								if(spoon)
								{
									feedCount++;
									tweenFood(draggingSprite,1,startPnt.x,startPnt.y);
									//TweenMax.delayedCall(.5,tweenFood,[currSprite,1,startPnt.x,startPnt.y]);
									if(feedCount == 2)
									{
										//go to next
										TweenMax.delayedCall(2.5,doneFeeding);
									}
								}
								else
								{
									feedCount++;
									tweenFood(draggingSprite,0,hitbox.x,hitbox.y);
									//TweenMax.delayedCall(.5,tweenFood,[currSprite,0,hitbox.x,hitbox.y]);
									//go to next
									if(feedCount == 2)
									{
										TweenMax.delayedCall(2.5,doneFeeding);
									}
									
								}
								if(soundmanager.soundIsPlaying("Chew_Woah_1")){soundmanager.getSoundChannel("Chew_Woah_1").stop();}
								
								if(characterString == "Toodee" || feedCount == 1)
								{
									soundmanager.playSound("Chew_Woah_1",Costanza.voiceVolume);
								}
								else
								{
									if(soundmanager.soundIsPlaying("Chew_Woah_2")){soundmanager.getSoundChannel("Chew_Woah_2").stop();}
									soundmanager.playSound("Chew_Woah_2",Costanza.voiceVolume);
								}
							}
							else
							{
								//tween back to start
								tweening = true;
								if((foodString == "Sushi" || foodString == "Sandwich") && index > 0)
								{
									TweenMax.to(draggingSprite,.5,{x:startPnt2.x,y:startPnt2.y, onComplete:setTweening, onCompleteParams:[draggingSprite]});
								}
								else
								{
									TweenMax.to(draggingSprite,.5,{x:startPnt.x,y:startPnt.y, onComplete:setTweening, onCompleteParams:[draggingSprite]});
								}
								armatures[0].animation.gotoAndPlay("idle2");
							}
							dragging = false;
						}
						break;
					}
				}
			}
		}
		
		private function doneFeeding():void
		{
			particle.CreateBurst(70,500,500,5,true,true);
			if(!DeviceInfo.isSlow)
			{
				TweenMax.delayedCall(.5,particle.CreateBurst,[70,500,500,5,true,true]);
				TweenMax.delayedCall(1,particle.CreateBurst,[70,500,500,5,true,true]);
				TweenMax.delayedCall(1.5,particle.CreateBurst,[70,500,500,5,true,true]);
			}
			TweenMax.delayedCall(.2,tweenBurst);
			TweenMax.delayedCall(4,nextPage);
			armatures[0].animation.gotoAndPlay("happy");
			soundmanager.playSound("Burst",Costanza.soundVolume);
		}
		
		private function tweenBurst():void
		{
			TweenMax.to(starBurst,1,{scaleX:10,scaleY:10, alpha:0});
		}
		
		private function tweenFood(currSprite:Sprite,num:int, _x:int,_y:int):void
		{
			if(spoon)
			{
				(currSprite.getChildAt(0) as MovieClip).currentFrame = 0;
				//armatures[int(currSprite.name)].animation.gotoAndPlay("empty");
			}
			tweenUtensil(currSprite,num,_x,_y);
			//TweenMax.delayedCall(1,tweenUtensil,[currSprite,num,_x,_y]);
		}
		
		private function tweenUtensil(currSprite:Sprite,num:int, _x:int,_y:int):void
		{
			TweenMax.to(currSprite,.5,{x:_x,y:_y,scaleX:num,scaleY:num,onComplete:setTweening, onCompleteParams:[currSprite,true]});
		}
		
		private function setTweening(spr:Sprite, eaten:Boolean=false):void
		{
			if(bowlArmature){bowlArmature.animation.gotoAndPlay("idle" + (feedCount+1));}
			tweening = false;
			if(spoon){spr.visible=false;}
			else if(eaten){spr.y = -100; }
			draggingSprite = null;
		}
		
		private function buildArmatures(num:int):void
		{
			var posArray:Array;
			switch(characterString)
			{
				case "Brobee":
					posArray = brobeeArray; break;
				case "Foofa":
					posArray = foofaArray; break;
				case "Muno":
					posArray = munoArray; break;
				case "Plex":
					posArray = plexArray; break;
				case "Toodee":
					posArray = toodeeArray; break;
			}
			
			//add character
			var charArmature:Armature = factory.buildArmature("Characters/" + characterString);
			charArmature.animation.gotoAndPlay("idle1",5);
			charArmature.display.x = Costanza.STAGE_WIDTH/2 - Costanza.STAGE_OFFSET;
			if(characterString == "Brobee")
			{
				charArmature.display.y = Costanza.STAGE_HEIGHT - charArmature.display.height/2;
			}
			else if(characterString == "Plex" || characterString == "Toodee")
			{
				charArmature.display.y = Costanza.STAGE_HEIGHT - charArmature.display.height/4;
			}
			else
			{
				charArmature.display.y = Costanza.STAGE_HEIGHT/2;
			}
			WorldClock.clock.add(charArmature);
			armatures.push(charArmature);
			//charArmature.animation.gotoAndPlay("idle1");
			//sprite for armature
			var charArmatureClip:Sprite = charArmature.display as Sprite;
			charArmatureClip.name = String(armatures.length-1);//set name to index
			charArmatureClip.addEventListener(TouchEvent.TOUCH, tapChar);
			
			//faces
			for(var i:int = 1; i <= num; i++)
			{
				// Build the armatures
				var tmpArm:Armature = factory.buildArmature("Characters/Face_" + i);
				tmpArm.display.x = posArray[i-1].x;
				tmpArm.display.y = posArray[i-1].y;
				WorldClock.clock.add(tmpArm);
				armatures.push(tmpArm);
				tmpArm.animation.gotoAndPlay("face1");
				
				//sprite for armature
				var tmpArmClip:Sprite = tmpArm.display as Sprite;
				tmpArmClip.name = String(armatures.length-1);
				if(characterString == "Foofa" && i == 3)
					tmpArmClip.addEventListener(TouchEvent.TOUCH, flowerTap);
				else
					tmpArmClip.addEventListener(TouchEvent.TOUCH, faceTouch);
				//arrayFood.push(tmpArmClip);
				
				sceneRoot.addChild(tmpArmClip);//add image after image hole
			}
			
			var index:int;
			switch(characterString)
			{
				case "Brobee":{index = 0; break;}
				case "Foofa":{index = 1; break;}
				case "Muno":{index = 2; break;}
				case "Plex":{index = 3; break;}
				case "Toodee":{index = 4; break;}
			}
			
			sceneRoot.addChild(starBurst);//add starburst behind char
			sceneRoot.addChild(particle);
			starBurst.touchable = false;
			particle.touchable = false;
			sceneRoot.addChild(charArmatureClip);
			buildFood(index);
			//sceneRoot.addChild(foodArmatureClip);
			
			hitbox = new Quad(100*Costanza.SCALE_FACTOR, 60*Costanza.SCALE_FACTOR);
			hitbox.pivotX = hitbox.width/2;
			hitbox.pivotY = hitbox.height/2;
			hitbox.x = charArmatureClip.x;
			hitbox.y = hitboxYPositions[index];
			hitbox.alpha = 0;
			sceneRoot.addChild(hitbox);
		}
		
		private function setFoodPosition():Point
		{
			var currArray:Array;
			var index:int;
			switch(characterString)
			{
				case "Brobee":{currArray = foodPositionsBrobee; break;}
				case "Foofa":{currArray = foodPositionsFoofa; break;}
				case "Muno":{currArray = foodPositionsMuno; break;}
				case "Plex":{currArray = foodPositionsPlex; break;}
				case "Toodee":{currArray = foodPositionsToodee; break;}
			}
			
			switch(foodString)
			{
				case "Sandwich":{index=0; break;}
				case "Yogurt":{index=1; break;}
				case "Noodles":{index=2; break;}
				case "Pizza":{index=3; break;}
				case "Sushi":{index=4; break;}
				case "Salad":{index=5; break;}
				case "Fruitsalad":{index=6; break;}
			}
			
			return currArray[index];
		}
		
		private function buildFood(i:int):void
		{
			var foodPoint:Point = setFoodPosition();
			//*************
			var foodArmature:Armature;
			//add food
			if(foodString == "Yogurt" || foodString == "Salad" || foodString == "Fruitsalad" || foodString == "Noodles")
			{
				foodArmature = factory.buildArmature("Characters/Bowl");
				spoon = true;
				bowlArmature = foodArmature;
			}
			else
			{
				foodArmature = factory.buildArmature("Characters/" + foodString);
			}
			//var foodArmature:Armature = factory.buildArmature("Characters/" + foodString);
			foodArmature.animation.gotoAndPlay("idle");
			foodArmature.display.x = foodPoint.x;
			foodArmature.display.y = foodPoint.y;
			WorldClock.clock.add(foodArmature);
			armatures.push(foodArmature);
			foodIndex = armatures.length-1;
			//sprite for armature
			var foodArmatureClip:Sprite = foodArmature.display as Sprite;
			foodArmatureClip.name = String(armatures.length-1);//set name to index
			foodArmatureClip.addEventListener(TouchEvent.TOUCH, touchHandler);
			
			startPnt = new Point(foodArmature.display.x,foodArmature.display.y);
			
			dragPiece = foodArmatureClip;
			
			//dragPiece
			if(spoon)
			{
				var _image:Image = factory.getTextureDisplay("Pieces/Bowls/" + characterString) as Image;
				// assign image to bone.display for chaging clothes effect. (using bone.display to dispose)
				var _bone:Bone = foodArmature.getBone("body"); _bone.display.dispose();
				_bone.display = _image;
				
				var _image2:Image = factory.getTextureDisplay("Pieces/Food/" + foodString) as Image;
				// assign image to bone.display for chaging clothes effect. (using bone.display to dispose)
				var _bone2:Bone = foodArmature.getBone("Yogurt"); _bone2.display.dispose();
				_bone2.display = _image2;
				
				var utensilArmClip:Sprite = new Sprite();
				
				if(foodString == "Salad" || foodString == "Noodles"){utensil = new MovieClip(assets.getTextures("Chopsticks_"));}
				else{utensil = new MovieClip(assets.getTextures("Spoon_"));}
				
				utensil.pivotX = utensil.width;
				utensilArmClip.addChild(utensil);
				dragPiece = utensilArmClip;
				dragPiece.visible = false;
				
				utensilArmClip.x = foodArmature.display.x + utensil.width;
				utensilArmClip.y = foodArmature.display.y - utensilArmClip.height;
				
				startPnt.setTo(utensilArmClip.x,utensilArmClip.y);
			}
			else if(foodString == "Sushi" || foodString == "Sandwich" || foodString == "Pizza")
			{
				var str:String;
				foodString == "Sushi" ? str = "Sushi_2" : str = foodString;
				//add food
				var foodArmature2:Armature = factory.buildArmature("Characters/" + str);
				foodArmature2.animation.gotoAndPlay("idle");
				foodString == "Sushi" ? (foodArmature2.display.x = foodPoint.x + foodArmature2.display.width) : (foodArmature2.display.x = foodPoint.x + 60);
				foodArmature2.display.y = foodPoint.y;
				WorldClock.clock.add(foodArmature2);
				sushiArmatures.push(foodArmature);
				foodIndex = sushiArmatures.length-1;
				sushiArmatures.push(foodArmature2);
				foodArmatureClip.name = "0";
				food2Index = sushiArmatures.length-1;
				//sprite for armature
				var foodArmatureClip2:Sprite = foodArmature2.display as Sprite;
				foodArmatureClip2.name = "1";//set name to index
				foodArmatureClip2.addEventListener(TouchEvent.TOUCH, touchHandler);
				
				startPnt2 = new Point(foodArmature2.display.x,foodArmature2.display.y);
				sceneRoot.addChild(foodArmatureClip2);
			}
			
			//add pizza
			if(foodString == "Pizza")
			{
				foodArmatureClip.pivotX = foodArmature.display.width/2;
				foodArmatureClip.pivotY = foodArmature.display.height/2;
				
				foodArmatureClip2.pivotX = foodArmature2.display.width/2;
				foodArmatureClip2.pivotY = foodArmature2.display.height/2;
			}
			
			sceneRoot.addChild(foodArmatureClip);
			
			if(utensilArmClip){sceneRoot.addChild(utensilArmClip);}
		}
		
		private function nextPage():void
		{
			soundmanager.getSoundChannel("Lobby_C_Music").stop();
			dispatchEventWith(LOAD_ROOM, true, 3);
		}
		
		private function onEnterFrameHandler(evt:EnterFrameEvent):void
		{
			WorldClock.clock.advanceTime(-1);
		}
		
		private function BackPressed():void
		{
			dispatchEventWith("loadLobby", true);
		}
		
		public function disposeScene():void
		{
			assets.removeTexture("textureFeed" + characterString,true);
			assets.removeTexture("foodTexture",true);
			App.backPressed.remove(BackPressed);
			TweenMax.killAll(true);
			foodTextureAtlas.dispose();
			textureAtlas.dispose();
			factory.dispose(true);
			var len:int = armatures.length;
			for (var i:int = 0; i < len; i++)
			{
				WorldClock.clock.remove(armatures[i]);
				armatures[i].dispose();
			}
			for (var j:int = 0; j < sushiArmatures.length; j++)
			{
				WorldClock.clock.remove(sushiArmatures[j]);
				sushiArmatures[j].dispose();
			}
			sushiArmatures.length = 0;
			armatures.length = 0;
			sceneRoot.removeChildren(0,sceneRoot.numChildren-1,true);
			
			assets.dispose();
		}
		
		
	}
}
