package scenes
{
    import com.cupcake.App;
    import com.cupcake.DeviceInfo;
    import com.greensock.TweenMax;
    import com.greensock.easing.Bounce;
    import com.greensock.events.LoaderEvent;
    import com.greensock.loading.ImageLoader;
    import com.greensock.loading.LoaderMax;
    
    import flash.desktop.NativeApplication;
    import flash.display.Bitmap;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.filesystem.File;
    import flash.system.Capabilities;
    import flash.system.System;
    
    import starling.core.Starling;
    import starling.display.Button;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.events.Event;
    import starling.extensions.PDParticleSystem;
    import starling.extensions.SoundManager;
    import starling.filters.BlurFilter;
    import starling.textures.Texture;
    import starling.utils.AssetManager;
    
    import ui.Help;
    import ui.Settings;
    
    import utils.ForParentsScreen;
    import utils.MoreApps;

    public class Menu extends starling.display.Sprite
    {
        public static const START_GAME:String = "startGame";
		private var soundManager:SoundManager;
		private var canTap:Boolean = true;
		
		private var soundmanager:SoundManager;
		private var assets:AssetManager;
		private var appDir:File = File.applicationDirectory;
		
		private var moreApps:Button;
		private var parentsApps:Button;
		
		private var settingsPopUp:Settings;
		
		private var btn:Button;
		private var optionsApps:Button;
		
		private var sceneRoot:starling.display.Sprite;
		private var assetsLoaded:Boolean = false;
		
		private var appsSparkle:PDParticleSystem;	
		private var appBtnSprite:flash.display.Sprite;
		private var appImageDisplay:Button;
		private var loader:LoaderMax;
		private var ldr:ImageLoader;
		
        public function Menu(charString:String = "", foodstr:String = "")
        {
			sceneRoot = new starling.display.Sprite();
			addChild(sceneRoot);
			
			addEventListener(Settings.HIDE_SETTINGS,  hideSettings);
			
			assets = new AssetManager(Costanza.SCALE_FACTOR);
			assets.useMipMaps = Costanza.mipmapsEnabled;
			Root.currentAssetsProxy = assets;
			assets.verbose = Capabilities.isDebugger;
			
			assets.enqueue(
				appDir.resolvePath("textures/"+Costanza.SCALE_FACTOR +"x/menu"),
				appDir.resolvePath("audio/Menu"),
				appDir.resolvePath("particles/MoreAppsSparkleTexture.png"),
				appDir.resolvePath("particles/MoreAppsSparkle.pex")
				// Add in room sound if available
			);
			
			assets.loadQueue(function onProgress(ratio:Number):void
			{
				assetsLoaded = true;
				if (ratio == 1){
					Starling.juggler.delayCall(function():void
					{
						// now would be a good time for a clean-up 
						System.pauseForGCIfCollectionImminent(0);
						System.gc();
						
					}, 0.15);
					init();
				};
			});
			
           // init();
        }
		
		public function MoreAppsLoaded():void
		{
			if ( MoreApps.moreAppsLoaded && MoreApps.apps.length > 0 )
			{
				moreApps.visible = true;
				moreApps.touchable = true;
				
				parentsApps.visible = true;
				parentsApps.touchable = true;
				
				TweenMax.delayedCall(3,loadSparklyAppButton);
			}
		}
		
		private function loadSparklyAppButton():void
		{
			loader = new LoaderMax( { onComplete:showSparklyAppButton } );
			ldr = new ImageLoader("app-storage:/" + MoreApps.apps[0].altFile );
			trace("app-storage:/" + MoreApps.apps[0].altFile);
			loader.append( ldr ); 
			loader.load();
		}
		
		private function showSparklyAppButton(e:LoaderEvent):void
		{
			trace( "Showing sparkly app button" );
			var bmd:Bitmap = ldr.rawContent;
			
			appImageDisplay			= new Button(Texture.fromBitmap(bmd));
			appImageDisplay.filter	= BlurFilter.createDropShadow();
			appImageDisplay.name	= "TossOut";
			appImageDisplay.x		= 475 + Costanza.STAGE_OFFSET;
			appImageDisplay.y		= 768;
			appImageDisplay.addEventListener(starling.events.Event.TRIGGERED, onButtonTriggered);
			sceneRoot.addChildAt(appImageDisplay,sceneRoot.getChildIndex(moreApps));
			TweenMax.to(appImageDisplay,.8,{y:640,delay:0.6,ease:Bounce.easeOut, onComplete:showSparkles});
			
			loader.dispose(true);
			loader = null;
			
			function showSparkles():void
			{
				appsSparkle.emitterX = appImageDisplay.x + appImageDisplay.width/2; 
				appsSparkle.emitterY = appImageDisplay.y + appImageDisplay.height; 
				appsSparkle.start();
				appsSparkle.parent.setChildIndex(appsSparkle,sceneRoot.getChildIndex(appImageDisplay)+1);
			}
		}
        
        private function init():void
        {
			addEventListener(Root.DISPOSE,  disposeScene);
			
			//MoreApps.Setup( MoreAppsLoaded, "en");
			soundManager = SoundManager.getInstance();
			
			soundManager.addSound("Play_BTN", assets.getSound("Play_BTN"));
			
			soundManager.addSound("Menu_Music",assets.getSound("Menu_Music"));
			soundManager.playSound("Menu_Music",Costanza.musicVolume,999);
           
			var bg:Image = new Image(assets.getTexture("Menu_BG"));
			bg.x = 0;
			sceneRoot.addChild(bg);
			
			trace(bg.width);
			
			btn = new Button(assets.getTexture("Play_BTN"));
			btn.pivotX = btn.width/2;
			btn.pivotY = btn.height/2;
			btn.x = Costanza.STAGE_WIDTH/2;
			btn.y = Costanza.STAGE_HEIGHT/2;
			btn.name = "Play";
			btn.addEventListener(starling.events.Event.TRIGGERED,onButtonTriggered);
			sceneRoot.addChild(btn);
			
			moreApps = new Button(assets.getTexture("Apps_BTN"));
			moreApps.pivotX = moreApps.width/2;
			moreApps.visible = moreApps.touchable = false;
			moreApps.x = btn.x;
			moreApps.y = Costanza.STAGE_HEIGHT - moreApps.height;
			moreApps.name = "MoreApps";
			moreApps.addEventListener(starling.events.Event.TRIGGERED, onButtonTriggered);
			sceneRoot.addChild(moreApps);
			
			optionsApps = new Button(assets.getTexture("Options_BTN"));
			optionsApps.pivotX = optionsApps.width/2;
			optionsApps.x = 300 + Costanza.STAGE_OFFSET;
			optionsApps.y = Costanza.STAGE_HEIGHT - optionsApps.height;
			optionsApps.name = "Options";
			optionsApps.addEventListener(starling.events.Event.TRIGGERED, onButtonTriggered);
			sceneRoot.addChild(optionsApps);
			
			parentsApps = new Button(assets.getTexture("Parents_BTN"));
			parentsApps.pivotX = parentsApps.width/2;
			parentsApps.x = 1050 + Costanza.STAGE_OFFSET;
			parentsApps.y = Costanza.STAGE_HEIGHT - parentsApps.height;
			parentsApps.name = "ForParents";
			parentsApps.visible = parentsApps.touchable = false;
			parentsApps.addEventListener(starling.events.Event.TRIGGERED, onButtonTriggered);
			sceneRoot.addChild(parentsApps);
			
			settingsPopUp = new Settings();
			settingsPopUp.x = Costanza.STAGE_WIDTH/2 - settingsPopUp.width/2;
			settingsPopUp.y = Costanza.STAGE_HEIGHT/2 - settingsPopUp.height/2;
			settingsPopUp.visible = false;
			settingsPopUp.touchable = false;
			sceneRoot.addChild(settingsPopUp);
			
			Starling.current.nativeStage.addEventListener(flash.events.Event.ACTIVATE, focusGained);
			
			appsSparkle = new PDParticleSystem(assets.getXml("MoreAppsSparkle"), assets.getTexture("MoreAppsSparkleTexture"));
			sceneRoot.addChild(appsSparkle);
			Starling.juggler.add(appsSparkle);
			
			App.backPressed.add(BackPressed);
			
			dispatchEventWith(Root.SCENE_LOADED,true);
        }
		
		private function BackPressed():void
		{
			App.Log( "Handling back pressed in main menu" );			
			
			if ( settingsPopUp.touchable ) { hideSettings(); }
			else if ( DeviceInfo.platform == DeviceInfo.PLATFORM_DESKTOP ) { App.ExitApp(); }
			else { App.CheckClose(); }
		}
		
		private function focusGained(e:flash.events.Event):void
		{
			//starling stage restarts when focus is lost and gained.
			//check to see if stage isnt visible, meaning for parents or more apps is visible.
			//If they are, stop the stage again manually, prevents starling buttons from working underneath more apps
			if(!Starling.current.stage3D.visible)
			{
				Starling.current.stop();
			}
		}
		
		private function hideSettings():void
		{
			settingsPopUp.visible = !settingsPopUp.visible;
			settingsPopUp.touchable = !settingsPopUp.touchable;
			btn.touchable = parentsApps.touchable = optionsApps.touchable = moreApps.touchable = !settingsPopUp.visible;
			
			soundManager.setVolume("Menu_Music",Costanza.musicVolume);
		}
		
		private function startGame(e:Button):void
		{
			soundManager.getSoundChannel("Menu_Music").stop();
			e.removeEventListener(starling.events.Event.TRIGGERED,onButtonTriggered);
			dispatchEventWith(START_GAME, true, "classic");
		}
        
        private function onButtonTriggered(e:starling.events.Event):void
        {
			if(canTap)
			{
				switch((e.currentTarget as Button).name)
				{
					case "Play":
					{
						canTap = false;
						soundManager.playSound("Play_BTN",Costanza.voiceVolume);
						TweenMax.delayedCall(3,startGame,[e.currentTarget as Button]);
						Starling.current.nativeOverlay.removeChildren();
						removeEventListener(Settings.HIDE_SETTINGS,  hideSettings);
						break;
					}
					case "MoreApps": case "TossOut":
					{
						//canTap = false;
						Starling.current.stop();
						var moreAppsHolder:flash.display.Sprite = new flash.display.Sprite();
						moreAppsHolder.x = 171 + Costanza.STAGE_OFFSET;
						Starling.current.nativeOverlay.addChild(moreAppsHolder);
						MoreApps.Show(moreAppsHolder);
						break;
					}
					case "ForParents": 
					{
						//canTap = false;
						Starling.current.stop();
						var forParentsHolder:flash.display.Sprite = new flash.display.Sprite();
						forParentsHolder.x = 171 + Costanza.STAGE_OFFSET;
						Starling.current.nativeOverlay.addChild(forParentsHolder);
						ForParentsScreen.Show(forParentsHolder,this);
						break;
					}
					case "Options":
					{
						//canTap = false;
						hideSettings();
						break;
					}
				}
			}
        }
		
		public function showHelpPage():void
		{
			var help:Help = new Help();
			addChild(help);
		}
		
		public function destroyScene():void
		{
			this.removeChildren(0,this.numChildren-1,true);
		}
		
		private function disposeScene():void
		{
			assets.dispose();
			App.backPressed.remove(BackPressed);
			sceneRoot.removeChildren(0,sceneRoot.numChildren-1,true);
		}
    }
}