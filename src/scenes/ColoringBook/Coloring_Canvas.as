package scenes.ColoringBook{
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageQuality;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.getDefinitionByName;
	
	//This Class is the canvas we draw on.
	//And also hold are possible undos.
	public class Coloring_Canvas extends Sprite{
		
		//Now for some reason to make the getByDef work
		//we need to have them atleast present.
		//So whenever you make a new book be sure to add your available pages.
		//I have no Idea how to prevent this and seems to be an FB issue.
		private var Rio_l1:YGGParty_Page_1;
		private var Rio_l2:YGGParty_Page_2;
		private var Rio_l3:YGGParty_Page_3;
		private var Rio_l4:YGGParty_Page_4;
		private var Rio_l5:YGGParty_Page_5;
		private var Rio_l6:YGGParty_Page_6;
		
		//Canvas.
		private var Canvas_Layers:Array;	
		private var Lines:Sprite;
		private var Lines_Image:Bitmap;
		private var Color_Transform:ColorTransform;		
		
		//Properties.
		private var Current_Page:int;
		private var Current_Layer:int;
		
		//Current Layer.
		private var Current_Layer_Data:BitmapData;
		private var m:Matrix;
		
		//---------------------------------------------------------------------------------
		//Master functions.
		//---------------------------------------------------------------------------------
		public function Coloring_Canvas(){			
			super();			
		}
		public function Load_Page(Prefix:String, Page:int):void{
							
			//Init Canvas.
			if(Canvas_Layers == null){
				Canvas_Layers = new Array();
			}else{Dispose_Canvases();}			
			
			//Set Current Page.
			Current_Page = Page;	
			
			//Set Layer.
			Current_Layer = -1;
			
			
			
			//Setup Laer properties.
			var Page_Name:String = ("YGGParty" + "_Page_" + Current_Page.toString());
			var Page_Class:Class;
			var Page_Clip:MovieClip;	
			
			//Create the Class for the layer.
			try{Page_Class 	= getDefinitionByName(Page_Name) as Class;}
			catch(error:Error){trace("Does Not Exist!"); return;}
			m = new Matrix();
			m.scale(Costanza.SCALE_FACTOR, Costanza.SCALE_FACTOR);
			
			//Get the class created.
			Page_Clip = new Page_Class();
			
			//Go trought the clip.
			for(var X:int = 0; X < Page_Clip.numChildren - 1; X++){
				
				var Layer_Name:String 		= ("p" + ((Canvas_Layers.length) + 1).toString());
				var LayerSprite:Sprite 		= Sprite(Page_Clip.getChildByName(Layer_Name));
				
				if(LayerSprite != null){	
				
					
					var Layer_Image_Holder:Sprite 	= new Sprite();
					var Layer_Image:Bitmap 			= new Bitmap();
					Layer_Image.bitmapData 			= new BitmapData(	(LayerSprite.getChildAt(0).width  * Costanza.SCALE_FACTOR),
																		(LayerSprite.getChildAt(0).height * Costanza.SCALE_FACTOR),
																		true, 0x000000);
					Layer_Image.bitmapData.drawWithQuality(LayerSprite.getChildAt(0), m, null, null, null, true, StageQuality.BEST);	
					Layer_Image.x = Math.round(LayerSprite.getChildAt(0).x);
					Layer_Image.y = Math.round(LayerSprite.getChildAt(0).y);	
					
					Layer_Image_Holder.addChild(Layer_Image);
					
					Layer_Image_Holder.x = Math.round(LayerSprite.x) * Costanza.SCALE_FACTOR;
					Layer_Image_Holder.y = Math.round(LayerSprite.y) * Costanza.SCALE_FACTOR;	
					
					Canvas_Layers.push(Layer_Image_Holder);
					
				}else{	
					
					Lines_Image = new Bitmap();
					Lines_Image.bitmapData = new BitmapData((Sprite(Page_Clip.getChildByName("Lines")).getChildAt(0).width * Costanza.SCALE_FACTOR),
															(Sprite(Page_Clip.getChildByName("Lines")).getChildAt(0).height * Costanza.SCALE_FACTOR),
															true, 0x000000);
					Lines_Image.bitmapData.drawWithQuality(Sprite(Page_Clip.getChildByName("Lines")).getChildAt(0), m, null, null, null, true, StageQuality.BEST);	
					
					Lines_Image.smoothing = true;
					
					Lines_Image.x = Math.round(Sprite(Page_Clip.getChildByName("Lines")).getChildAt(0).x);
					Lines_Image.y = Math.round(Sprite(Page_Clip.getChildByName("Lines")).getChildAt(0).y);						
					
					Lines = new Sprite();
					Lines.addChild(Lines_Image);
					Lines.x = Math.round(Sprite(Page_Clip.getChildByName("Lines")).x) * Costanza.SCALE_FACTOR;
					Lines.y = Math.round(Sprite(Page_Clip.getChildByName("Lines")).y) * Costanza.SCALE_FACTOR;						
					
				}				
				
			}			
			
			//Destroy patern.
			Page_Clip 	= null;
			Page_Class 	= null;				
			
		}
		public function Dispose_Canvases():void{
			
			if(Lines != null){
				Lines.removeChild(Lines_Image);
				Lines = null;
				Lines_Image.bitmapData.dispose();
				Lines_Image = null;
			}
			
			for(var X:int = 0; X < Canvas_Layers.length; X++){
				Bitmap(Canvas_Layers[X].getChildAt(0)).bitmapData.dispose();
				Canvas_Layers[X].removeChildAt(0);
				Canvas_Layers[X] = null;
				Canvas_Layers.splice(X, 1);
				X--;
			}	
			
			Canvas_Layers.length = 0;
			
		}
		
		public function Get_Lines():Sprite{
			return Lines;
		}
		public function Select_Layer(Location:Point):int{
			
			//Scan.
			for(var X:int = 0; X < Canvas_Layers.length; X++){				
				if(Layer_Colision(Location, null, X)){		
					
					if(Current_Layer_Data != null){
						Current_Layer_Data.dispose();
						Current_Layer_Data = null;
					}
					
					Current_Layer = X;					
					return X;
				}
			}
			
			//No luck.
			Current_Layer = -1;
			return -1;
			
		}
		public function Get_Layer(Selection:int):Sprite{
			return Canvas_Layers[Selection];
		}
		public function Get_Layer_Section(Selection:int, Rec:Rectangle = null, Position:Point = null):Bitmap{			
			
			if(Rec == null){
				Rec = new Rectangle(0, 0, Bitmap(Canvas_Layers[Current_Layer].getChildAt(0)).width, Bitmap(Canvas_Layers[Current_Layer].getChildAt(0)).height);
			}
			
			if(Position == null){
				Position = new Point(0, 0);
			}else{
				Position.x = (Position.x - (Rec.width / 2));
				Position.y = (Position.y - (Rec.height / 2));
			}		
			
			var Section:Bitmap 	= new Bitmap();
			Section.bitmapData 	= new BitmapData(Rec.width, Rec.height, true, 0x000000);			
			
			Section.bitmapData.copyPixels(Bitmap(Canvas_Layers[Current_Layer].getChildAt(0)).bitmapData, new Rectangle(Position.x, Position.y, Rec.width, Rec.height), new Point(0, 0));				
			
			Section.smoothing 	= true;
			
			return Section;
			
		}
		public function Layer_Colision(Position:Point, Target:Sprite = null, Location:int = 0):Boolean{
			
			//Set location.			
			var sX:int;
			var sY:int;
			
			if(Target == null){
				
				//Set location.			
				sX =  (Position.x - Canvas_Layers[Location].x);
				sY =  (Position.y - Canvas_Layers[Location].y);
				
				//Check for colision alpha.
				if(Bitmap(Canvas_Layers[Location].getChildAt(0)).bitmapData.getPixel(sX, sY).toString(16) != "0"){			
					//clean.
					return true;
				}else{				
					//Clean.
					return false;				
				}
				
			}
			
			//Set location.			
			sX =  (Position.x - Target.x);
			sY =  (Position.y - Target.y);
			
			
			//Are colision testes color.
			var BitmapColision:BitmapData = new BitmapData(1, 1, true, 0x000000);
			
			//Frame of draw.
			var bRec:Rectangle = new Rectangle(0, 0, 1, 1);
			
			//Translation matrix.
			var bMatrix:Matrix = new Matrix();
			bMatrix.translate(-sX, -sY);
			
			//Draw the pixel.
			BitmapColision.draw(Target, bMatrix, null, null, bRec, false);		
			
			//Check for colision alpha.
			if(BitmapColision.getPixel(0, 0).toString(16) != "0"){			
				//clean.
				BitmapColision.dispose();
				return true;	
			}else{				
				//Clean.
				BitmapColision.dispose();
				return false;				
			}
			
		}
		//---------------------------------------------------------------------------------	
		
	}
}