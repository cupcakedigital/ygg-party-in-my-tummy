package scenes.ColoringBook{
	
	import com.cupcake.App;
	import com.cupcake.DeviceInfo;
	import com.greensock.TweenMax;
	
	import flash.desktop.NativeApplication;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.display.StageQuality;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TouchEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.media.CameraRoll;
	import flash.system.Capabilities;
	import flash.system.System;
	import flash.ui.Multitouch;
	import flash.ui.MultitouchInputMode;
	import flash.utils.ByteArray;
	
	import scenes.ColoringBook.ColoringInterface.CB_Interface_HUD;
	import scenes.ColoringBook.ColoringInterface.CB_Loading_Screen;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.extensions.SoundManager;
	import starling.utils.AssetManager;
	
	import utils.PNGEncoder;
	
	public class Coloring_Book_Scene extends starling.display.Sprite{
		
		//For Loading Sounds.
		private var assets:AssetManager;
		private var soundManager:SoundManager;
		private var appDir:File = File.applicationDirectory;
		
		//For saving images to cam.
		private var camSave:Boolean;	
		private var camRoll:CameraRoll;
		
		//Are Scene Holder.
		private var Scene_Holder:flash.display.Sprite;
		private var Scene_Scale:Number;
		
		//Available colors.
		//Available colors.
		private var Drawing_Colors:Vector.<uint> = new <uint>[0xfd8ca1,
			0xffcdde,
			0xff3630,
			0xfa5a55,
			0xa340ad,
			0xbd4bc9,
			0xd756e5,
			0xef60fe,
			0x13b5ea,
			0xbfe7e8,
			0x41bcff,
			0x86d4fe,
			0x7fcd31,
			0x348341,
			0x70c39c,
			0x82e2b5,
			0xf3fd2a,
			0xf58929,
			0xf79e27,
			0xd89336,
			0xf6a83f,
			0xffffff,
			0xd5cec6,
			0x000000];	
			
		//Position Saving.
		private var Last_Mouse_Position:Point;
		private var Current_Mouse_Position:Point;		
		
		//Props.
		private var Page_Pack:String;
		private var TouchArray:Array;	
		private var Full_App_Width:int;
		private var Full_App_Height:int;		
		private var Update_Phase:String;
		private var PixColo:BitmapData;		
		
		//Art Assets.
		private var Core_Assets:External_PNG_Sheet_Manager;
		private var Sticker_Assets:External_PNG_Sheet_Manager;
		private var Pages_Icons_Assets:External_PNG_Page_Manager;
		
		//Multytouch Values.
		private var Squeeze_Last:Number;
		private var Squeeze_Current:Number;
		private var Rotation_Last:Number;
		
		//Page Props.
		private var Current_Coloring_Page:int;
		private var Next_Coloring_Page:int;		
		private var Selected_Swatch:int;
		private var Selected_Color:int;
		private var Selected_Alpha:Number;
		private var Selected_Scale:Number;
		private var Selected_Layer:int;
		private var Swatchs_Holder:Array;
		private var Swatchs_Draw_Holder:Array;
		
		//Draw Holder.
		private var Drawing_Canvas_Holder:flash.display.Sprite;	
		private var Drawing_Canvas_Bitmap:Bitmap;
		private var Drawing_Canvas_Sticker:flash.display.Sprite;
		private var Drawing_Canvas_Layers:Array;	
		private var Drawing_Undos:Array;
		private var Stickers:Array;		
		
		//Canvas layers.	
		private var Canvas:Coloring_Canvas;	
		
		//Layer of Draw.
		private var Draw_Layer_Holder:flash.display.Sprite;
		private var Draw_Layer_Mask:flash.display.Sprite;
		private var Draw_Layer_Draw:flash.display.Sprite;
		private var Draw_Layer_Data:Bitmap;
		private var Draw_Layer_Data_Mask:Bitmap;
		private var Draw_Layer_X:int;
		private var Draw_Layer_Y:int;	
		
		//Glitter Particle.
		private var Glitter_Map:Array;
		private var GlitCounter:int;
		private var GiltGo:Boolean;
		private var Glitter_Layers_Info:Array;
		
		//Hud.
		private var Interface_Hud:CB_Interface_HUD;
		private var Page_Selection_Hud:Coloring_Tools_Pages;
		
		//Loading Screen.
		private var sLoading:CB_Loading_Screen;		
		
		private var num:int = 0;
		
		private var First_Time:Boolean;
		private var First_Down:String;
		private var Art_Extention:String;
		
		private var assetsLoaded:Boolean = false;
		
		private var canTap:Boolean = true;
		
		//<id>com.cupcakedigital.ssc.internal.colouring</id>
		
		public function Coloring_Book_Scene(PagePack:String, PagePack:String){
			
			super();	
			
			this.x = Costanza.STAGE_OFFSET;
			
			Art_Extention			= ("x" + Costanza.SCALE_FACTOR.toString() + "/");
			First_Down				= "Nothing";
			First_Time				= true;
			Page_Pack 				= "Rio";					
			Full_App_Width 			= (1366 * Costanza.SCALE_FACTOR);
			Full_App_Height 		= (768  * Costanza.SCALE_FACTOR);// - ( CONFIG::MARKET == "nabi" ? Costanza.NABI_OFFSET : 0 );
			Multitouch.inputMode 	= MultitouchInputMode.TOUCH_POINT;
			TouchArray 				= new Array();			
			PixColo 				= new BitmapData(1, 1, true, 0xffffff);				
			
			//Setup the scene.
			Scene_Holder 	= new flash.display.Sprite();
			//Scene_Holder.scrollRect = new Rectangle(0, 0, 1366, 768);
			//Scene_Holder.x 	= -Costanza.VIEWPORT_OFFSET;			
			
			//Setup aspect.
			var scaleX:Number = (Costanza.VIEWPORT_WIDTH / Full_App_Width);
			var scaleY:Number = (Costanza.VIEWPORT_HEIGHT / Full_App_Height);			
			if(scaleY > scaleX){Scene_Scale = scaleY;}				
			else{Scene_Scale = scaleX;}			
			
			Scene_Holder.scaleX 	= Scene_Holder.scaleY = Scene_Scale;			
			Scene_Holder.x 			= -(((Full_App_Width * Scene_Scale) - Costanza.VIEWPORT_WIDTH) / 2);
			
			//Scene_Holder.scaleX = Scene_Holder.scaleY = 0.5;	
			//Scene_Holder.x = 300;
			
			Costanza.StageProportionScale = (((Costanza.STAGE_WIDTH * Scene_Scale) - Costanza.VIEWPORT_WIDTH) / 2) + (Costanza.STAGE_OFFSET / 2);
			if(Costanza.StageProportionScale < 0){Costanza.StageProportionScale = -Costanza.StageProportionScale;}
			trace(Scene_Holder.x + " Scene position");
			
			Starling.current.nativeStage.addChild(Scene_Holder);			
			//Starling.current.nativeStage.quality = StageQuality.HIGH_16X16;
			
			sLoading = new CB_Loading_Screen();
			Scene_Holder.addChild(sLoading);
			
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.ACTIVATE,screenActivated);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.DEACTIVATE,screenDeactivated);
			
			assets 					= new AssetManager();
			Root.currentAssetsProxy = assets;
			assets.verbose 			= Capabilities.isDebugger;					
			assets.enqueue(appDir.resolvePath("audio/ColoringSFX"));
			assets.loadQueue(function onProgress(ratio:Number):void{
				if (ratio == 1){
					assetsLoaded = true;
					Starling.juggler.delayCall(function():void{
						// now would be a good time for a clean-up 
						System.pauseForGCIfCollectionImminent(0);
						System.gc();						
					}, 0.15);
					FirstPass();
				};
				
			});		
			
		}
		//TEMP HACK UNTIL ADOBE FIXES AIR BUG
		private function screenActivated(e:flash.events.Event):void
		{
			trace("----------------------------SCENE ACTIVATED-----------------------------");
			if(!assetsLoaded)
			{
				assets.enqueue(appDir.resolvePath("audio/ColoringSFX"));
				assets.loadQueue(function onProgress(ratio:Number):void{
					if (ratio == 1){
						assetsLoaded = true;
						Starling.juggler.delayCall(function():void{
							// now would be a good time for a clean-up 
							System.pauseForGCIfCollectionImminent(0);
							System.gc();						
						}, 0.15);
						FirstPass();
					};
					
				});		
			}
		}
		private function screenDeactivated(e:flash.events.Event):void
		{
			trace("----------------------------SCENE DEACTIVATED-----------------------------");
			if(!assetsLoaded)
			{
				assets.purgeQueue();
			}
		}
		private function FirstPass():void{
			
			NativeApplication.nativeApplication.removeEventListener(flash.events.Event.ACTIVATE,screenActivated);
			NativeApplication.nativeApplication.removeEventListener(flash.events.Event.DEACTIVATE,screenDeactivated);
			//sound Setup.
			soundManager = SoundManager.getInstance();		
			soundManager.addSound("SFX_Take_Picture", 		assets.getSound("Take_Picture_SFX"));			
			soundManager.addSound("Slide_Bar", 				assets.getSound("Slide_SFX"));
			soundManager.addSound("Click_Mouse", 			assets.getSound("Selection_SFX"));	
			
			soundManager.addSound("SFX_Brush_Button", 		assets.getSound("brush button"));
			soundManager.addSound("SFX_Bucket_Button", 		assets.getSound("bucket button"));
			soundManager.addSound("SFX_Chalk_Button", 		assets.getSound("chalk button"));
			soundManager.addSound("SFX_Crayon_Button", 		assets.getSound("crayon button"));
			soundManager.addSound("SFX_Glitter_Button", 	assets.getSound("glitter button"));
			soundManager.addSound("SFX_Home_Button", 		assets.getSound("home button"));
			soundManager.addSound("SFX_Pages_Button", 		assets.getSound("pages button"));
			soundManager.addSound("SFX_Sticker_Button", 	assets.getSound("stickers button"));
			
			soundManager.addSound("SFX_Color_Pick_Button", 	assets.getSound("select individual crayons"));
			soundManager.addSound("SFX_More_Button", 		assets.getSound("more button down"));
			soundManager.addSound("SFX_Zin_Button", 		assets.getSound("zoom in button"));
			soundManager.addSound("SFX_Zout_Button", 		assets.getSound("zoom out button"));
			soundManager.addSound("SFX_Select_Sticker", 	assets.getSound("select sticker"));
			soundManager.addSound("SFX_Select_Page", 		assets.getSound("select individual pages"));			
			
			soundManager.addSound("SFX_Draw_Brush", 		assets.getSound("painting sfx"));
			soundManager.addSound("SFX_Draw_Bucket", 		assets.getSound("bucket paint sfx"));
			soundManager.addSound("SFX_Draw_Crayon", 		assets.getSound("crayon sfx"));
			soundManager.addSound("SFX_Draw_Chalk", 		assets.getSound("chalk sfx"));
			soundManager.addSound("SFX_Draw_Spray", 		assets.getSound("spray sfx"));
			soundManager.addSound("SFX_Draw_Eraser", 		assets.getSound("eraser sfx"));
			
			soundManager.stopAllSounds();
			//soundManager.playSound(Page_Pack, Costanza.musicVolume,999);				
			
			//Setup Native Display.
			addEventListener(Root.DISPOSE,  disposeScene);
			Starling.current.stage3D.visible = false;
			Starling.current.stop();			
			Starling.current.nativeStage.addEventListener(flash.events.Event.ACTIVATE, ReFocus);
			Starling.current.nativeStage.addEventListener(flash.events.Event.ENTER_FRAME, Prep_Loader);		
			//NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE;
			
			App.backPressed.add(BackPressed);
			
			//Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED, true);			
			
		}
		private function Prep_Loader(e:flash.events.Event):void{
			trace("Enter frame ok");
			Starling.current.nativeStage.removeEventListener(flash.events.Event.ENTER_FRAME, Prep_Loader);
			Load_Sticker_Assets();//Loadup core assets for the scene.	
		}
		private function Load_Sticker_Assets():void{
			Sticker_Assets 	= new External_PNG_Sheet_Manager("textures/ColoringBookTexture/" + Art_Extention, Load_Core_Assets);			
			Sticker_Assets.Append_Assets("Sticker_Big", ".png");			
			Sticker_Assets.Load_Assets();
		}		
		private function Load_Core_Assets():void{
			Core_Assets = new External_PNG_Sheet_Manager("textures/ColoringBookTexture/" + Art_Extention, Process_Pages_Icons);
			Core_Assets.Append_Assets("CB_Coloring_Interface_Elements", ".png");		
			Core_Assets.Append_Assets("Page_Selection_Hud_Items", 		".png");			
			Core_Assets.Append_Assets(("Page_Selection_" + Page_Pack), 	".png");	
			Core_Assets.Append_Assets("Sticker_Small", 					".png");
			Core_Assets.Load_Assets();
		}
		private function Process_Pages_Icons():void{
			Pages_Icons_Assets = new External_PNG_Page_Manager(Core_Assets_Loaded);
			Pages_Icons_Assets.Prep_Page_Selection_Manager(Core_Assets, Page_Pack);
		}
		private function Core_Assets_Loaded():void{
			
			//Update Mode.
			Update_Phase 			= "Idle";
			
			//Setup positions.
			Last_Mouse_Position 	= new Point(0, 0);
			Current_Mouse_Position 	= new Point(0, 0);	
			
			//Camroll.
			camSave 				= false;
			camRoll 				= new CameraRoll();
			if((camRoll != null) && CameraRoll.supportsAddBitmapData){camSave = true;}				
			
				
			Initialize_Glitter_Particles();	
			Initialize_Coloring_Page();
			
			Interface_Hud = new CB_Interface_HUD(Core_Assets, Drawing_Colors);
			Scene_Holder.addChildAt(Interface_Hud, 0);
			
			Page_Selection_Hud = new Coloring_Tools_Pages(Core_Assets, Pages_Icons_Assets, Page_Pack);
			//Scene_Holder.addChild(Page_Selection_Hud);
			
			Initialize_Swatches();
			
			Core_Assets.Purge_Assets();		
			
			//Next_Coloring_Page = Math.ceil(Math.random() * 8);
			Load_Canvas_From_File();			
			
			//trace("We Are Clear");
			Starling.current.nativeStage.addEventListener(flash.events.MouseEvent.MOUSE_DOWN, mDown);
			Starling.current.nativeStage.addEventListener(flash.events.Event.ENTER_FRAME,  Update);
			Starling.current.nativeStage.addEventListener(flash.events.MouseEvent.MOUSE_MOVE, mMove);
			Starling.current.nativeStage.addEventListener(flash.events.MouseEvent.MOUSE_UP, mUp);
			Starling.current.nativeStage.addEventListener(TouchEvent.TOUCH_MOVE, MultyTapperMoved);
			Starling.current.nativeStage.addEventListener(TouchEvent.TOUCH_BEGIN, MultyTapperBegin);
			Starling.current.nativeStage.addEventListener(TouchEvent.TOUCH_END, MultyTapperEnded);			
			
			//var mem:String = Number( System.totalMemory / 1024 / 1024 ).toFixed( 2 ) + "Mb";
			//trace( mem + " Current Mem");
			
		}
		private function ExitGame():void{			
			
			//NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.NORMAL;	
			
			//Remove Listeners.			
			Starling.current.nativeStage.removeEventListener(flash.events.MouseEvent.MOUSE_DOWN, mDown);
			Starling.current.nativeStage.removeEventListener(flash.events.Event.ENTER_FRAME,  Update);
			Starling.current.nativeStage.removeEventListener(flash.events.MouseEvent.MOUSE_MOVE, mMove);
			Starling.current.nativeStage.removeEventListener(flash.events.MouseEvent.MOUSE_UP, mUp);
			Starling.current.nativeStage.removeEventListener(TouchEvent.TOUCH_MOVE, MultyTapperMoved);
			Starling.current.nativeStage.removeEventListener(TouchEvent.TOUCH_BEGIN, MultyTapperBegin);
			Starling.current.nativeStage.removeEventListener(TouchEvent.TOUCH_END, MultyTapperEnded);
			Starling.current.nativeStage.removeEventListener(flash.events.Event.ACTIVATE, ReFocus);	
			//NativeApplication.nativeApplication.removeEventListener(Event.DEACTIVATE, CloseApp);	
			
			//Remove scene holder from native stage.
			Starling.current.nativeStage.removeChild(Scene_Holder);		
			//Starling.current.nativeStage.removeChildren();
			
			//Starling.current.stage3D.visible = true;
			//Starling.current.start();
			
			assets.purge();
			
			Scene_Holder.removeChild(Interface_Hud);
			Interface_Hud.Destroy();
			
			if(Page_Selection_Hud.Get_Active()){
				Scene_Holder.removeChild(Page_Selection_Hud);
			}
			Page_Selection_Hud.Dispose_Bar();
			
			//Pages_Icons_Assets.
			Pages_Icons_Assets.Purge_Assets();
			Pages_Icons_Assets.Kill_Me();
			
			//Destroy swatches.
			Dispose_Swatches();
			
			//Clear Page.
			Clear_Coloring_Page();		
			
			//Destroy the render state canvas.
			Drawing_Canvas_Bitmap.bitmapData.dispose();
			Drawing_Canvas_Bitmap = null;
			
			PixColo.dispose();
			Clear_Glitter_Grid();
			
			//Clear Asset Holders.
			Core_Assets.Purge_Assets();	
			Core_Assets.Kill_Me();
			Sticker_Assets.Purge_Assets();
			Sticker_Assets.Kill_Me();
			
			//Clear Sounds.
			soundManager.stopAllSounds();
			
			Scene_Holder.removeChild(sLoading);
			sLoading.Destroy();
			
			App.backPressed.remove(BackPressed);
			
			Starling.current.stage3D.visible = true;
			Starling.current.start();
			
			
			this.addEventListener(flash.events.Event.ENTER_FRAME,  CloseFinal);
						
		}
		private function CloseFinal(e:starling.events.EnterFrameEvent):void{
			this.removeEventListener(flash.events.Event.ENTER_FRAME,  CloseFinal);
			//Starling.current.stage3D.visible = true;
			//Starling.current.start();
			dispatchEventWith(Root.LOAD_LOBBY, true);
		}
		private function disposeScene():void{
			
		}
		private function ReFocus(e:flash.events.Event):void{
			if(!Starling.current.stage3D.visible){
				trace("Refocused Native Display");
				Starling.current.stop();
			}
		}
		private function CloseApp(event:Event):void{
			Save_To_File();
		}	
		
		
		private function Update_Mouse():void{
			
			Last_Mouse_Position.x 	= Current_Mouse_Position.x;
			Last_Mouse_Position.y 	= Current_Mouse_Position.y;
			
			Current_Mouse_Position.x = Starling.current.nativeStage.mouseX;
			Current_Mouse_Position.y = Starling.current.nativeStage.mouseY;	
			
		}	
		private function mDown(e:flash.events.MouseEvent):void{
			
			First_Down = "Nothing";
			Update_Mouse();			
			
			if(TouchArray.length >= 1){return;}
			
			if(Page_Selection_Hud.Get_Active()){
				Page_Selection_Hud.Click_Bar(Current_Mouse_Position);
				return;
			}			
			
			switch(Interface_Hud.Down(Current_Mouse_Position)){				
				case "Scale_1":{
					Selected_Scale = 0.5;
					Set_Swatchs();
					soundManager.playSound("SFX_Brush_Button", Costanza.musicVolume);	
					return;
				}break;
				case "Scale_2":{
					Selected_Scale = 1.0;
					Set_Swatchs();
					soundManager.playSound("SFX_Brush_Button", Costanza.musicVolume);	
					return;
				}break;
				case "Scale_3":{
					Selected_Scale = 1.5;
					Set_Swatchs();
					soundManager.playSound("SFX_Brush_Button", Costanza.musicVolume);	
					return;
				}break;
				case "Home":{
					First_Down = "Home";
					soundManager.playSound("SFX_Home_Button", Costanza.musicVolume);
					return;
				}break;
				case "Page":{
					First_Down = "Page";
					soundManager.playSound("SFX_Pages_Button", Costanza.musicVolume);	
					return;
				}break;
				case "Brush":{
					Selected_Swatch = 0;
					Selected_Color 	= Interface_Hud.Get_Selected_Color();
					Set_Swatchs();
					soundManager.playSound("SFX_Brush_Button", Costanza.musicVolume);	
					return;
				}break;
				case "Bucket":{
					Selected_Swatch = 4;
					Selected_Color 	= Interface_Hud.Get_Selected_Color();
					Set_Swatchs();
					soundManager.playSound("SFX_Bucket_Button", Costanza.musicVolume);	
					return;
				}break;
				case "Crayon":{
					Selected_Swatch = 1;
					Selected_Color 	= Interface_Hud.Get_Selected_Color();
					Set_Swatchs();
					soundManager.playSound("SFX_Crayon_Button", Costanza.musicVolume);	
					return;
				}break;
				case "Spray":{
					Selected_Swatch = 2;
					Selected_Color 	= Interface_Hud.Get_Selected_Color();
					Set_Swatchs();
					soundManager.playSound("SFX_Glitter_Button", Costanza.musicVolume);	
					return;
				}break;
				case "Chalk":{
					Selected_Swatch = 3;
					Selected_Color 	= Interface_Hud.Get_Selected_Color();
					Set_Swatchs();
					soundManager.playSound("SFX_Chalk_Button", Costanza.musicVolume);	
					return;
				}break;
				case "Eraser":{
					Selected_Swatch = 1;
					Selected_Color 	= -1;
					Set_Swatchs();
					soundManager.playSound("SFX_Brush_Button", Costanza.musicVolume);	
					return;
				}break;
				case "Color_Scroll":{
					Selected_Color = Interface_Hud.Get_Selected_Color();
					Set_Swatchs();					
					soundManager.playSound("SFX_Color_Pick_Button", Costanza.musicVolume);	
					Update_Phase = "Scrolling";
					return;
				}break;			
				case "Sticker":{					
					if(Interface_Hud.Get_Zoom() == "In"){
						Set_Zoom(true);
						Interface_Hud.Reset_Zoom();
					}
					soundManager.playSound("SFX_Sticker_Button", Costanza.musicVolume);	
					return;
				}break;	
				case "Undo":{
					soundManager.playSound("Click_Mouse", Costanza.soundVolume);
					Using_Undo();
					return;
				}break;
				case "Recycle":{
					soundManager.playSound("Click_Mouse", Costanza.soundVolume);
					return;
				}break;
				case "Camera":{			
					soundManager.playSound("SFX_Take_Picture", Costanza.soundVolume);
					var Picture:BitmapData = new BitmapData(((1366 * Costanza.SCALE_FACTOR) - (Costanza.VIEWPORT_OFFSET * 2)), (768 * Costanza.SCALE_FACTOR), false, 0xffffff);					
					var DM:Matrix = new Matrix();
					DM.translate(-Costanza.VIEWPORT_OFFSET, 0);
					Picture.draw(Drawing_Canvas_Holder, DM);
					if(camSave){camRoll.addBitmapData(Picture);}
					Picture.dispose();
					Picture = null;
					return;
				}break;
				
				case "ZoomIn":{	
					soundManager.playSound("SFX_Zin_Button", Costanza.soundVolume);
					return;
				}break;
				case "ZoomOut":{
					soundManager.playSound("SFX_Zout_Button", Costanza.soundVolume);
					return;
				}break;		
				
				case "Yes":{
					soundManager.playSound("Click_Mouse", Costanza.soundVolume);
					Disposing_Undos();
					Disposing_Stickers();
					Clear_Glitter_Grid();
					Drawing_Canvas_Bitmap.bitmapData.fillRect(Drawing_Canvas_Bitmap.bitmapData.rect, 0xffffff);
					return;
				}break;
				case "No":{
					soundManager.playSound("Click_Mouse", Costanza.soundVolume);
					return;
				}break;
				
				case "More":{
					soundManager.playSound("SFX_More_Button", Costanza.soundVolume);
					return;
				}break;
				
			}	
			
			//Check see if hit a sticker.
			if(Interface_Hud.Get_Mode() == "Sticker"){
				if(Click_Sticker(Current_Mouse_Position)){					
					//Interface_Hud.Hide_Bottom_Hud();
					Interface_Hud.Show_Trash();
					soundManager.playSound("SFX_Select_Sticker", Costanza.soundVolume);
					Update_Phase = "Update_Stickers";
					return;
				}
			}
			
			//Paint.
			if(Interface_Hud.Get_Mode() == "Brush" || Interface_Hud.Get_Mode() == "Crayon" || Interface_Hud.Get_Mode() == "Spray" || Interface_Hud.Get_Mode() == "Chalk" || Interface_Hud.Get_Mode() == "Eraser"){
				if(Interface_Hud.Down(Current_Mouse_Position) == "Nothing"){
					if(Choose_Coloring_Layer(Current_Mouse_Position)){					
						//trace("Paint");
						Adding_Undo();
						//Interface_Hud.Hide_Bottom_Hud();
						Scene_Holder.removeChild(Interface_Hud);
						//Interface_Hud
						
						Update_Phase = "Painting";
						return;
					}
				}
			}
			
			//Full Draw.
			if(Interface_Hud.Get_Mode() == "Bucket"){
				if(Interface_Hud.Down(Current_Mouse_Position) == "Nothing"){
					if(Choose_Coloring_Layer(Current_Mouse_Position)){						
						soundManager.playSound("SFX_Draw_Bucket", Costanza.soundVolume);
						Adding_Undo();
						Paint_Coloring_Full();
						Paste_Draw_Layer();
						Update_Phase = "Painting_Full";
						return;
					}
				}				
			}		
			
		}
		private function mMove(e:flash.events.MouseEvent):void{
			Interface_Hud.Move(Current_Mouse_Position, Last_Mouse_Position);			
		}
		private function Update(e:flash.events.Event):void{
			
			Update_Mouse();	
			
			if(Page_Selection_Hud.Get_Active()){
				Page_Selection_Hud.Move_Bar((Current_Mouse_Position.x - Last_Mouse_Position.x));
				return;
			}	
			
			switch(Update_Phase){				
				
				case "Idle":{
					Update_Glitter_Grid();
				}break;
				case "NewPagePreLoad":{
					Update_Phase = "NewPage";
					return;
				}break;
				case "NewPage":{
					
					//Page_Selection_Hud.Deactivate_Bar();
					Scene_Holder.addChildAt(Interface_Hud, 0);	
					Scene_Holder.addChildAt(Drawing_Canvas_Holder, 0);	
					Scene_Holder.removeChild(Page_Selection_Hud);
					Next_Coloring_Page = (Page_Selection_Hud.Get_Selection() + 1);
					Load_New_Coloring_Page();
				
					Update_Phase = "Loading_New_Page";
					return;
				}break;
				
				
				
				case "ExitPrep":{
					Update_Phase = "ExitToMain";
					return;
				}break;
				case "ExitToMain":{					
					Scene_Holder.addChildAt(Interface_Hud, 0);	
					Scene_Holder.addChildAt(Drawing_Canvas_Holder, 0);
					if(Page_Selection_Hud.Get_Active()){
						Page_Selection_Hud.Deactivate_Bar();
						Scene_Holder.removeChild(Page_Selection_Hud);
					}
					Save_To_File();
					ExitGame();
					Update_Phase = "Quiting";
					return;
				}break;
				
				case "Update_Stickers":{
					Update_Sticker(Current_Mouse_Position);
					Interface_Hud.Move(Current_Mouse_Position, Last_Mouse_Position);
					return;
				}break;					
				
				case "Painting":{
					
					switch(Interface_Hud.Get_Mode()){						
						case "Brush":{
							if(!soundManager.soundIsPlaying("SFX_Draw_Brush")){
								soundManager.playSound("SFX_Draw_Brush", Costanza.soundVolume);	
							}
						}break;
						case "Crayon":{
							if(!soundManager.soundIsPlaying("SFX_Draw_Crayon")){
								soundManager.playSound("SFX_Draw_Crayon", Costanza.soundVolume);	
							}
						}break;	
						case "Spray":{
							if(!soundManager.soundIsPlaying("SFX_Draw_Spray")){
								soundManager.playSound("SFX_Draw_Spray", Costanza.soundVolume);	
							}
						}break;	
						case "Chalk":{
							if(!soundManager.soundIsPlaying("SFX_Draw_Chalk")){
								soundManager.playSound("SFX_Draw_Chalk", Costanza.soundVolume);	
							}
						}break;	
						case "Eraser":{
							if(!soundManager.soundIsPlaying("SFX_Draw_Eraser")){
								soundManager.playSound("SFX_Draw_Eraser", Costanza.soundVolume);	
							}
						}break;
					}					
					
					var Seperations:int = int(Point.distance(Last_Mouse_Position, Current_Mouse_Position));
					if(Seperations > 10){Seperations = 10;}					
					
					if(Interface_Hud.Get_Mode() == "Spray"){
						if(Seperations > 2){Seperations = 2;}
					}
					
					var Draw_Locations:Point 	= new Point(0, 0);
					for(var X:int = 0; X < Seperations; X++){
						Draw_Locations.x = (Last_Mouse_Position.x + (((Current_Mouse_Position.x - Last_Mouse_Position.x) / Seperations) * X));
						Draw_Locations.y = (Last_Mouse_Position.y + (((Current_Mouse_Position.y - Last_Mouse_Position.y) / Seperations) * X));		
						if(Interface_Hud.Get_Mode() == "Spray"){
							Glitter_Coloring_Page(Draw_Locations);
						}else{
							Paint_Coloring_Page(Draw_Locations);
						}
						
					}				
					
					return;
					
				}break;
				
				case "Updating_Zoom_Move":{
					Update_Move_Zoom(Current_Mouse_Position.x - Last_Mouse_Position.x, Current_Mouse_Position.y - Last_Mouse_Position.y);
					return;
				}break;
			}
			
				
			
			
			
		}
		private function mUp(e:flash.events.MouseEvent):void{
			
			Update_Mouse();		
						
			if(Page_Selection_Hud.Get_Active()){
				
				switch(Page_Selection_Hud.Up_Bar(Current_Mouse_Position)){
					case "Home":{
						Page_Selection_Hud.Deactivate_Bar();
						soundManager.playSound("SFX_Home_Button", Costanza.musicVolume);	
						Scene_Holder.addChild(sLoading);	
						Update_Phase = "ExitPrep";					
						return;
					}break;
					case "Close":{
						soundManager.playSound("SFX_Home_Button", Costanza.musicVolume);	
						Page_Selection_Hud.Deactivate_Bar();
						Scene_Holder.addChildAt(Interface_Hud, 0);	
						Scene_Holder.addChildAt(Drawing_Canvas_Holder, 0);
						Scene_Holder.removeChild(Page_Selection_Hud);
						return;
					}break;
					case "Loading_Page":{
						soundManager.playSound("SFX_Select_Page", Costanza.musicVolume);						
						Scene_Holder.addChild(sLoading);	
						Page_Selection_Hud.Deactivate_Bar();
						Update_Phase = "NewPagePreLoad";
						return;
					}break;
				}
				
			}	
			
			switch(Update_Phase){
				case "Painting":{
					if(soundManager.soundIsPlaying("SFX_Draw_Brush")){soundManager.stopSound("SFX_Draw_Brush");}
					if(soundManager.soundIsPlaying("SFX_Draw_Crayon")){soundManager.stopSound("SFX_Draw_Crayon");}
					if(soundManager.soundIsPlaying("SFX_Draw_Spray")){soundManager.stopSound("SFX_Draw_Spray");}
					if(soundManager.soundIsPlaying("SFX_Draw_Chalk")){soundManager.stopSound("SFX_Draw_Chalk");;}
					if(soundManager.soundIsPlaying("SFX_Draw_Eraser")){soundManager.stopSound("SFX_Draw_Eraser");}
					Paste_Draw_Layer();
					Update_Phase = "Idle";					
					Interface_Hud.Show_Me();	
					Scene_Holder.addChild(Interface_Hud);
					return;
				}break;
				case "Update_Stickers":{
					Drop_Sticker(Current_Mouse_Position);
					Update_Phase = "Idle";	
					Interface_Hud.Hide_Trash();
					return;
				}break;
				case "Updating_Zoom_Move":{	
					Update_Phase = "Idle";
					Interface_Hud.Show_Me();	
					Scene_Holder.addChild(Interface_Hud);					
					return;
				}break;
				case "Scrolling":{
					Update_Phase = "Idle";					
				}break;
				case "Painting_Full":{
					Update_Phase = "Idle";
					return;
				}break;
			}
			
			switch(Interface_Hud.Up(Current_Mouse_Position)){
				
				case "Home":{
					trace("Home");
					if(First_Down != "Home"){return;}
					Scene_Holder.addChild(sLoading);	
					Update_Phase = "ExitPrep";	
					return;
				}break;
				
				case "Page":{
					trace("Page");
					if(First_Down != "Page"){return;}
					Set_Zoom(true);
					
					
					
					Pages_Icons_Assets.Update_Icon(Drawing_Canvas_Holder, (Current_Coloring_Page - 1));
					Page_Selection_Hud.Activate_Bar();
					Scene_Holder.addChild(Page_Selection_Hud);					
					Scene_Holder.removeChild(Interface_Hud);
					Scene_Holder.removeChild(Drawing_Canvas_Holder);
					return;
				}break;
				
				case "Sticker_Scroll":{						
					soundManager.playSound("SFX_Select_Sticker", Costanza.soundVolume);
					Create_Sticker((Interface_Hud.Get_Selected_Sticker() + 1), Current_Mouse_Position);					
					return;										
				}break;
				
				case "ZoomIn":{
					Set_Zoom(false);
					return;
				}break;
				case "ZoomOut":{
					Set_Zoom(true);
					return;
				}break;
				
			}
			
			//trace("(Up Value): " + Interface_Hud.Up(Current_Mouse_Position));			
			
		}
		
		private function BackPressed():void
		{
			if(canTap)
			{
				canTap = false;
				Scene_Holder.addChild(sLoading);
				Update_Phase = "ExitPrep";
			}
		}

		//Multy Touch.
		private function MultyTapperBegin(e:TouchEvent):void{
			
			if(TouchArray.length > 1){return;}
			
			var Touch_ID:Array 	= [e.touchPointID, new Point(e.stageX, e.stageY)];
			TouchArray.push(Touch_ID);
			
			if(TouchArray.length > 1){
				Squeeze_Current = Point.distance(TouchArray[0][1], TouchArray[1][1]);
				Squeeze_Last 	= Squeeze_Current;				
				Rotation_Last 	= Math.atan2((TouchArray[1][1].y - TouchArray[0][1].y), (TouchArray[1][1].x - TouchArray[0][1].x)) * (180 / Math.PI); 				
			}			
			
			if(Interface_Hud.Get_Zoom() == "In"){
				if(TouchArray.length > 1){
					
					if(Update_Phase == "Painting"){						
						if(soundManager.soundIsPlaying("SFX_Draw_Brush")){soundManager.stopSound("SFX_Draw_Brush");}
						if(soundManager.soundIsPlaying("SFX_Draw_Crayon")){soundManager.stopSound("SFX_Draw_Crayon");}
						if(soundManager.soundIsPlaying("SFX_Draw_Spray")){soundManager.stopSound("SFX_Draw_Spray");}
						if(soundManager.soundIsPlaying("SFX_Draw_Chalk")){soundManager.stopSound("SFX_Draw_Chalk");;}
						if(soundManager.soundIsPlaying("SFX_Draw_Eraser")){soundManager.stopSound("SFX_Draw_Eraser");}									
					}else{
						Scene_Holder.removeChild(Interface_Hud);	
					}					
					
					Update_Phase = "Updating_Zoom_Move";
					return;
				}
			}					
			
		}
		private function MultyTapperMoved(e:TouchEvent):void{
			
			for(var X:int = 0; X < TouchArray.length; X++){
				if(TouchArray[X][0] == e.touchPointID){
					TouchArray[X][1].x = e.stageX;
					TouchArray[X][1].y = e.stageY;
				}
			}
			
			Squeeze_Last 	= Squeeze_Current;
			if(TouchArray.length > 1){Squeeze_Current = Point.distance(TouchArray[0][1], TouchArray[1][1]);}
		}

		private function MultyTapperEnded(e:TouchEvent):void{
			var Touch_ID:int = e.touchPointID;
			for(var X:int = 0; X < TouchArray.length; X++){
				if(TouchArray[X][0] == Touch_ID){
					TouchArray.splice(X, 1);
					X--;
				}
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
				
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//Draw Layer.
		private function Create_Draw_Layer(Mask:flash.display.Sprite):void{
			
			Draw_Layer_X 					= Mask.x;
			Draw_Layer_Y 					= Mask.y;
			return;
			Mask.x 							= 0;
			Mask.y							= 0;
			
			Draw_Layer_Holder 				= new flash.display.Sprite();
			Draw_Layer_Mask  				= new flash.display.Sprite();
			Draw_Layer_Mask.addChild(Mask);
			Draw_Layer_Draw  				= new flash.display.Sprite();
			Draw_Layer_Data					= new Bitmap();
			Draw_Layer_Data.bitmapData		= new BitmapData(Mask.width, Mask.height, true, 0x000000);
			
			if(!DeviceInfo.isSlow){
				Draw_Layer_Data.smoothing 	= true;
			}
			
			Draw_Layer_Draw.addChild(Draw_Layer_Data);
			
			Draw_Layer_Draw.cacheAsBitmap 	= true;
			Draw_Layer_Mask.cacheAsBitmap 	= true;
			Draw_Layer_Draw.mask 			= Draw_Layer_Mask;
			
			Draw_Layer_Holder.addChild(Draw_Layer_Draw);
			Draw_Layer_Holder.addChild(Draw_Layer_Mask);
			
			Draw_Layer_Holder.x 			= Draw_Layer_X;
			Draw_Layer_Holder.y 			= Draw_Layer_Y;
			
			//Draw_Layer_Holder.visible 		= false;
			//Draw_Layer_Holder.cacheAsBitmap 	= true;
			//Draw_Layer_Holder.
			
			Drawing_Canvas_Holder.addChildAt(Draw_Layer_Holder, 1);
			
		}
		private function Paste_Draw_Layer(Draw:Boolean = true):void{			
			return;
			if(Draw_Layer_Holder == null){return;}
			
			Drawing_Canvas_Holder.removeChild(Draw_Layer_Holder);
			
			//Draw.
			if(Draw){
				var TM:Matrix = new Matrix();
				TM.translate(Draw_Layer_X, Draw_Layer_Y);
				if(DeviceInfo.isSlow){
					Drawing_Canvas_Bitmap.bitmapData.draw(Draw_Layer_Holder, TM);
				}else{
					Drawing_Canvas_Bitmap.bitmapData.drawWithQuality(Draw_Layer_Holder, TM, null, "normal", null, true, StageQuality.BEST);
				}
			}		
			
			//Reset Mask Layer.
			Draw_Layer_Mask.getChildAt(0).x = Draw_Layer_X;
			Draw_Layer_Mask.getChildAt(0).y = Draw_Layer_Y;
			
			//Clean.
			Draw_Layer_Holder.removeChild(Draw_Layer_Draw);
			Draw_Layer_Holder.removeChild(Draw_Layer_Mask);	
			Draw_Layer_Draw.mask 			= null;
			Draw_Layer_Draw.removeChild(Draw_Layer_Data);			
			Draw_Layer_Data.bitmapData.dispose();
			Draw_Layer_Data 				= null;			
			Draw_Layer_Draw 				= null;			
			Draw_Layer_Mask.removeChildAt(0);
			Draw_Layer_Mask 				= null;
			Draw_Layer_Holder 				= null;
			
		}		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
			
		//Swatches.
		private function Initialize_Swatches():void{			
			
			Swatchs_Holder 	= new Array();
			Swatchs_Holder.push(Core_Assets.Get_Image("CB_Swatch_1"));
			Swatchs_Holder.push(Core_Assets.Get_Image("CB_Swatch_2"));
			Swatchs_Holder.push(Core_Assets.Get_Image("CB_Swatch_3"));
			Swatchs_Holder.push(Core_Assets.Get_Image("CB_Swatch_4"));
			
			Swatchs_Holder.push(Core_Assets.Get_Image("star_1"));
			Swatchs_Holder.push(Core_Assets.Get_Image("star_2"));
			Swatchs_Holder.push(Core_Assets.Get_Image("star_3"));
			
			Swatchs_Holder.push(Core_Assets.Get_Image("Glitter_1"));
			Swatchs_Holder.push(Core_Assets.Get_Image("Glitter_2"));
			
			Selected_Color = Interface_Hud.Get_Selected_Color();			
			Selected_Scale = Interface_Hud.Get_Selected_Scale();		
			
			Set_Swatchs();
			
		}
		private function Dispose_Swatches():void{
			
			Reset_Swatchs();
			
			//Clear Swatches.
			for(var SA:int = 0; SA < Swatchs_Holder.length; SA++){
				Dispose_Image(Swatchs_Holder[SA]);
			}
			Swatchs_Holder.length = 0;	
			
		}
		private function Set_Swatchs():void{
			
			if(Swatchs_Draw_Holder == null){Swatchs_Draw_Holder = new Array();}
			else{Reset_Swatchs();}
			
			var AreColor:uint;
			if(Selected_Color == -1){
				AreColor = 0xffffff;
			}else{
				AreColor = Drawing_Colors[Selected_Color];	 	
			}
			
				
			
			
			for(var SA:int = 0; SA < Swatchs_Holder.length; SA++){
				
				//Set Color.			
				var Color_Transform:ColorTransform		= new ColorTransform();					
				var ScaleValue:Number 					= Selected_Scale;
				
				if((SA < 4)){
					Color_Transform.color 				= AreColor;
					Color_Transform.alphaMultiplier 	= 0.1;
					Color_Transform.alphaOffset 		= 9;	
				}else if((SA >= 4) && (SA < 7)){
					Color_Transform.color 				= AreColor;	
					ScaleValue 							= (ScaleValue - 0.8);
					if(ScaleValue < 0.1){ScaleValue 	= 0.1;}
				}else{
					ScaleValue 							= 0.1;
					Color_Transform.color 				= 0xffffff;	
				}				
				
				Swatchs_Holder[SA].scaleX 				= Swatchs_Holder[SA].scaleY = ScaleValue;		
				var Swatch:Bitmap 						= new Bitmap();
				Swatch.smoothing 						= true;
				Swatch.bitmapData 						= new BitmapData(Swatchs_Holder[SA].width, Swatchs_Holder[SA].height, true, 0x000000);				
				
				var TM:Matrix 							= new Matrix();
				TM.scale(ScaleValue, ScaleValue);
				Swatch.bitmapData.drawWithQuality(Swatchs_Holder[SA], TM, Color_Transform, "normal", null, true, StageQuality.BEST);				
				
				var FillSwatch:Bitmap 					= new Bitmap();
				FillSwatch.smoothing 					= true;
				if(SA >= 7){	
					FillSwatch.bitmapData 				= new BitmapData(Swatch.width, Swatch.height, false, 0xffffff);
				}else{
					FillSwatch.bitmapData 				= new BitmapData(Swatch.width, Swatch.height, false, AreColor);
				}
				
				Swatch.cacheAsBitmap 					= true;
				FillSwatch.cacheAsBitmap 				= true;
				FillSwatch.mask 						= Swatch;
				
				var SuperHolder:flash.display.Sprite 	= new flash.display.Sprite();
				SuperHolder.addChild(FillSwatch);
				SuperHolder.addChild(Swatch);					
				
				if(SA >= 7){					
					var GlowFill:GlowFilter 			= new GlowFilter(0xffffff, 0.3, 8, 8, 2, BitmapFilterQuality.MEDIUM);
					SuperHolder.filters 				= [GlowFill];					
				}
				
				var FinalDraw:Bitmap 					= new Bitmap();
				FinalDraw.smoothing 					= true;
				FinalDraw.bitmapData 					= new BitmapData(SuperHolder.width, SuperHolder.height, true, 0x000000);
				FinalDraw.bitmapData.drawWithQuality(SuperHolder, null, null, "normal", null, true, StageQuality.BEST);
				
				Swatchs_Draw_Holder.push(FinalDraw);
				
				//Clean.
				SuperHolder.removeChild(FillSwatch);
				SuperHolder.removeChild(Swatch);	
				SuperHolder 							= null;
				
				FillSwatch.bitmapData.dispose();
				FillSwatch 								= null;
				
				Swatch.bitmapData.dispose();
				Swatch 									= null;
				
				TM 										= null;
				GlowFill 								= null;
				Color_Transform 						= null;
				
			}			
			
		}
		private function Reset_Swatchs():void{
			for(var X:int = 0; X < Swatchs_Draw_Holder.length; X++){
				Swatchs_Draw_Holder[X].bitmapData.dispose();
				Swatchs_Draw_Holder[X] = null;
			}
			Swatchs_Draw_Holder.length = 0;
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//Coloring Page.
		private function Initialize_Coloring_Page():void{
			
			Drawing_Undos 				= new Array();
			Stickers 					= new Array();
			Current_Coloring_Page 		= 1;
			Next_Coloring_Page			= 1;			
			Canvas 						= new Coloring_Canvas();			
			Selected_Scale  			= 1.0;
			Selected_Alpha  			= 1;
			Selected_Color  			= 0;
			Selected_Layer  			= 0;
			Selected_Swatch 			= 0;		
			
			Drawing_Canvas_Layers				= new Array();
			Drawing_Canvas_Holder 				= new flash.display.Sprite();				
			Drawing_Canvas_Sticker				= new flash.display.Sprite();
			Drawing_Canvas_Bitmap 				= new Bitmap();
			Drawing_Canvas_Bitmap.bitmapData 	= new BitmapData(Full_App_Width, Full_App_Height, false, 0xffffff);
			Drawing_Canvas_Bitmap.smoothing 	= true;
			
		}	
		private function Load_New_Coloring_Page():void{
			Save_To_File();
			Clear_Coloring_Page();
			Current_Coloring_Page = Next_Coloring_Page;
			Load_Canvas_From_File();			
		}
		private function Clear_Coloring_Page():void{
			
			Clear_Glitter_Grid();
			
			Disposing_Undos();
			
			Drawing_Canvas_Holder.removeChild(Canvas.Get_Lines());
			Drawing_Canvas_Holder.removeChild(Drawing_Canvas_Sticker);
			Drawing_Canvas_Holder.removeChild(Drawing_Canvas_Bitmap);
			
			Disposing_Stickers();
			
			Canvas.Dispose_Canvases();
			
			Drawing_Canvas_Bitmap.bitmapData.fillRect(Drawing_Canvas_Bitmap.bitmapData.rect, 0xffffff);			
			
			for(var X:int = 0; X < Drawing_Canvas_Layers.length; X++){
				Dispose_Image(Drawing_Canvas_Layers[X]);
				Drawing_Canvas_Layers.splice(X, 1);
				X--;
			}Drawing_Canvas_Layers.length = 0;	
			
			Scene_Holder.removeChild(Drawing_Canvas_Holder);
			
		}
		private function Load_Canvas_From_File():void{
			
			//Extension.
			var FilePath:String 	= ((Page_Pack + "_Page_" + Current_Coloring_Page.toString()) + ".png");				
			var myFile:File;
			myFile 					= File.applicationStorageDirectory;
			myFile 					= myFile.resolvePath( FilePath );			
			var ba:ByteArray 		= new ByteArray();			
			var stream:FileStream 	= new FileStream();					
		
			if(myFile.exists){					
				
				stream.open(myFile, FileMode.READ);				
				stream.readBytes(ba);	
				stream.close();				
				
				var loader:Loader = new Loader();
				loader.contentLoaderInfo.addEventListener(flash.events.Event.COMPLETE, On_Canvas_Loaded);
				loader.loadBytes(ba);				
				
			}else{	
				Load_External_Stickers();	
			}		
			
			//clear for Leaks.
			ba.clear();
			stream = null;
			myFile = null;
			FilePath = null;
			
		}
		private function On_Canvas_Loaded(e:flash.events.Event):void{
			Drawing_Canvas_Bitmap.bitmapData.draw(Bitmap(e.currentTarget.loader.content).bitmapData);
			Bitmap(e.currentTarget.loader.content).bitmapData.dispose();
			e.currentTarget.loader.contentLoaderInfo.removeEventListener(flash.events.Event.COMPLETE, On_Canvas_Loaded);
			e = null;			
			Load_External_Stickers();
		}
		private function Process_Page_Loaded():void{
			//trace("OK IN");
			Canvas.Load_Page(Page_Pack, Current_Coloring_Page);	
			
			Drawing_Canvas_Holder.addChild(Drawing_Canvas_Bitmap);				
			Drawing_Canvas_Holder.addChild(Canvas.Get_Lines());
			Drawing_Canvas_Holder.addChild(Drawing_Canvas_Sticker);
			Scene_Holder.addChildAt(Drawing_Canvas_Holder, 0);		
			Drawing_Canvas_Holder.visible = true;
			Update_Phase = "Idle";	
			Scene_Holder.removeChild(sLoading);
			
			if(First_Time){
				First_Time = false;
				Page_Selection_Hud.Activate_Bar();
				Scene_Holder.addChild(Page_Selection_Hud);					
				Scene_Holder.removeChild(Interface_Hud);
				Scene_Holder.removeChild(Drawing_Canvas_Holder);
			}
			
		}
		private function Choose_Coloring_Layer(Position:Point):Boolean{
			
			Selected_Layer = Canvas.Select_Layer(Drawing_Canvas_Holder.globalToLocal(Position));
			if(Selected_Layer != -1){					
				Create_Draw_Layer(Canvas.Get_Layer(Selected_Layer));				
				return true;
			}
			
			return false;
			
		}		
		
		
		
		
		private function Glitter_Coloring_Page(Position:Point):void{	
			
			Position 	= Drawing_Canvas_Holder.globalToLocal(Position);	
			Position.x 	= (Position.x - Draw_Layer_X);
			Position.y 	= (Position.y - Draw_Layer_Y);
			
			var Random_Pix:int = Math.floor(Math.random() * 10);			
			if(Random_Pix > 5){
				PixColo.fillRect(PixColo.rect, Get_Color_Alpha(0xE8F1D4));
			}else{
				PixColo.fillRect(PixColo.rect, Get_Color_Alpha(0xE6E8FA));		
			}			
			
			for(var G:int = 0; G < 20; G++){					
				
				var gX:int 				= Position.x + ((Math.random() * Swatchs_Draw_Holder[Selected_Swatch].width) - (Swatchs_Draw_Holder[Selected_Swatch].width / 2));
				var gY:int 				= Position.y + ((Math.random() * Swatchs_Draw_Holder[Selected_Swatch].height) - (Swatchs_Draw_Holder[Selected_Swatch].height / 2));
					
				//here we check if we hit the mask.
				if(Canvas.Layer_Colision(new Point((gX + Draw_Layer_X), (gY + Draw_Layer_Y)), null, Selected_Layer)){	
					
					if((Math.random() * 10) > 9){
						var Random_Star:int = (4 + Math.floor(Math.random() * 5));	
						//if(Random_Star == 6){Random_Star = 8;}
						//if(Random_Star == 5){Random_Star = 7;}
						//if(Random_Star == 4){Random_Star = 8;}
						Drawing_Canvas_Bitmap.bitmapData.copyPixels(Swatchs_Draw_Holder[Random_Star].bitmapData, new Rectangle(0, 0, Swatchs_Draw_Holder[Random_Star].width, Swatchs_Draw_Holder[Random_Star].height), new Point((gX + Draw_Layer_X), (gY + Draw_Layer_Y)), null, null, true);
					}else{
						Drawing_Canvas_Bitmap.bitmapData.copyPixels(PixColo, new Rectangle(0, 0, 1, 1), new Point((gX + Draw_Layer_X), (gY + Draw_Layer_Y)), null, null, true);
					}							
					
					//Add the glitter.
					if(!DeviceInfo.isSlow){
						Edit_Glitter_Grid(new Point((gX + Draw_Layer_X), (gY + Draw_Layer_Y)), 1);
					}
					
				}
				
			}
			
		}
		private function Paint_Coloring_Page(Position:Point):void{		
			
			Position 	= Drawing_Canvas_Holder.globalToLocal(Position);	
			Position.x 	= (Position.x - Draw_Layer_X);
			Position.y 	= (Position.y - Draw_Layer_Y);		
			
			//Layers of draw.
			var Section_Sprite:flash.display.Sprite 	= new flash.display.Sprite();		
			
			var MaskBitmap:Bitmap 						= Canvas.Get_Layer_Section(Selected_Layer, Bitmap(Swatchs_Draw_Holder[Selected_Swatch]).bitmapData.rect, Position);
			var Mask_Sprite:flash.display.Sprite 		= new flash.display.Sprite();
			Mask_Sprite.addChild(MaskBitmap);
			
			var Swatch_Sprite:flash.display.Sprite 		= new flash.display.Sprite();
			Swatch_Sprite.addChild(Swatchs_Draw_Holder[Selected_Swatch]);			
			
			//Set Mask.
			Swatch_Sprite.cacheAsBitmap 	= true;
			Mask_Sprite.cacheAsBitmap 		= true;
			Swatch_Sprite.mask 				= Mask_Sprite;
			
			//Pop master.
			Section_Sprite.addChild(Swatch_Sprite);
			Section_Sprite.addChild(Mask_Sprite);
			
			var BitDraw:BitmapData = new BitmapData(Section_Sprite.width, Section_Sprite.height, true, 0x000000);
			BitDraw.draw(Section_Sprite);
			
			Drawing_Canvas_Bitmap.bitmapData.copyPixels(BitDraw,
														new Rectangle(0, 0, MaskBitmap.width, MaskBitmap.height),
														new Point((Position.x + Draw_Layer_X), (Position.y + Draw_Layer_Y)),
														null, null,
														true);
			
			//if(!DeviceInfo.isSlow){
				//Erase_Glitter_Grid(new Point((Position.x + Draw_Layer_X), (Position.y + Draw_Layer_Y)), Section_Sprite.width);	
			//}
			
			
			Section_Sprite.removeChild(Mask_Sprite);
			Section_Sprite.removeChild(Swatch_Sprite);			
			Swatch_Sprite.mask = null;
			
			Mask_Sprite.removeChild(MaskBitmap);
			Swatch_Sprite.removeChild(Swatchs_Draw_Holder[Selected_Swatch]);			
			
			Section_Sprite 	= null;
			Mask_Sprite 	= null;
			Swatch_Sprite 	= null;
			
			MaskBitmap.bitmapData.dispose();
			MaskBitmap = null;
			
			BitDraw.dispose();
			BitDraw = null;			
			
		}
		private function Paint_Coloring_Full():void{
			
			var Position:Point = new Point(0, 0);			
			
			//Layers of draw.
			var Section_Sprite:flash.display.Sprite 	= new flash.display.Sprite();		
			
			var MaskBitmap:Bitmap 						= Canvas.Get_Layer_Section(Selected_Layer);
			var Mask_Sprite:flash.display.Sprite 		= new flash.display.Sprite();
			Mask_Sprite.addChild(MaskBitmap);
			
			var FillColor:Bitmap 						= new Bitmap();
			FillColor.bitmapData 						= new BitmapData(MaskBitmap.width, MaskBitmap.height, false, Drawing_Colors[Selected_Color]);
			var Swatch_Sprite:flash.display.Sprite 		= new flash.display.Sprite();
			Swatch_Sprite.addChild(FillColor);			
			
			//Set Mask.
			Swatch_Sprite.cacheAsBitmap 	= true;
			Mask_Sprite.cacheAsBitmap 		= true;
			Swatch_Sprite.mask 				= Mask_Sprite;
			
			//Pop master.
			Section_Sprite.addChild(Swatch_Sprite);
			Section_Sprite.addChild(Mask_Sprite);
			
			var BitDraw:BitmapData = new BitmapData(Section_Sprite.width, Section_Sprite.height, true, 0x000000);
			BitDraw.draw(Section_Sprite);
			
			Drawing_Canvas_Bitmap.bitmapData.copyPixels(BitDraw,
				new Rectangle(0, 0, MaskBitmap.width, MaskBitmap.height),
				new Point((Position.x + Draw_Layer_X), (Position.y + Draw_Layer_Y)),
				null, null,
				true);
			
			//Erase_Glitter_Fill_Grid();			
			
			Section_Sprite.removeChild(Mask_Sprite);
			Section_Sprite.removeChild(Swatch_Sprite);			
			Swatch_Sprite.mask = null;
			
			Mask_Sprite.removeChild(MaskBitmap);
			Swatch_Sprite.removeChild(FillColor);			
			
			Section_Sprite 	= null;
			Mask_Sprite 	= null;
			Swatch_Sprite 	= null;
			
			MaskBitmap.bitmapData.dispose();
			MaskBitmap = null;
			
			FillColor.bitmapData.dispose();
			FillColor = null;
			
			BitDraw.dispose();
			BitDraw = null;	
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		//Zoom.
		private function Set_Zoom(Active:Boolean):void{
			
			if(Active){
				Drawing_Canvas_Holder.scaleX = Drawing_Canvas_Holder.scaleY = 1.0;
				Drawing_Canvas_Holder.x = 0;
				Drawing_Canvas_Holder.y = 0;
			}else{
				Drawing_Canvas_Holder.scaleX = Drawing_Canvas_Holder.scaleY = 2.0;	
				Drawing_Canvas_Holder.x = 0;
				Drawing_Canvas_Holder.y = 0;
			}			
			
		}
		private function Update_Move_Zoom(ForceX:int, ForceY:int):void{
			
			Drawing_Canvas_Holder.x += ForceX;
			Drawing_Canvas_Holder.y += ForceY;
			
			if(Drawing_Canvas_Holder.x < (-(1366 * 2) + 1366)){Drawing_Canvas_Holder.x = (-(1366 * 2) + 1366);}
			if(Drawing_Canvas_Holder.y < (-(768 * 2) + 768)){Drawing_Canvas_Holder.y = (-(768 * 2) + 768);}
			if(Drawing_Canvas_Holder.x > 0){Drawing_Canvas_Holder.x  = 0;}
			if(Drawing_Canvas_Holder.y > 0){Drawing_Canvas_Holder.y  = 0;}
			
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//Undos.
		private function Adding_Undo():void{
			
			if(Drawing_Undos.length > 1){
				Drawing_Undos[0].dispose();
				Drawing_Undos.splice(0, 1);
			}
			
			var New_Render:BitmapData = new BitmapData(Full_App_Width, Full_App_Height, false, 0xffffff);
			New_Render.draw(Drawing_Canvas_Bitmap);
			Drawing_Undos.push(New_Render);			
			
		}
		private function Using_Undo():void{
			
			if(Drawing_Undos.length <= 0){return;}
			Drawing_Canvas_Bitmap.bitmapData.draw(Drawing_Undos[(Drawing_Undos.length - 1)]);
			
			Drawing_Undos[(Drawing_Undos.length - 1)].dispose();
			Drawing_Undos.splice((Drawing_Undos.length - 1), 1);
			
		}
		private function Disposing_Undos():void{
			for(var X:int = 0; X < Drawing_Undos.length; X++){				
				BitmapData(Drawing_Undos[X]).dispose();
				Drawing_Undos.splice(X, 1);
				X--;
			}
			Drawing_Undos.length = 0;
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//Stickers.
		private function Create_Sticker(pID:int, Position:Point):void{
			
			var Sticker_Limit:int = 50;
			if(DeviceInfo.isSlow){
				Sticker_Limit = 5;
			}
			
			if(Stickers.length > Sticker_Limit){return;}
			
			Position 								= Scene_Holder.globalToLocal(Position);	
			
			var Sticker_Props:Array 				= new Array();
			var New_Sticker:flash.display.Sprite 	= Sticker_Assets.Get_Image("Sticker_Big_" + pID.toString());
			New_Sticker.getChildAt(0).x 			= -(New_Sticker.getChildAt(0).width / 2);
			New_Sticker.getChildAt(0).y 			= -(New_Sticker.getChildAt(0).height / 2);
			var Active:Boolean 						= false;
			var Off_Loc:Point 						= new Point(0, 0);			
			New_Sticker.scaleX 						= New_Sticker.scaleY = 1.0;			
			New_Sticker.x 							= ((1366 * Costanza.SCALE_FACTOR) / 2);
			New_Sticker.y 							= ((768 * Costanza.SCALE_FACTOR) / 2);
			
			Sticker_Props.push(New_Sticker);
			Sticker_Props.push(Active);
			Sticker_Props.push(Off_Loc);		
			Sticker_Props.push(false);
			Sticker_Props.push(pID);
			Sticker_Props.push(1.0);			
			Sticker_Props.push(new Point(New_Sticker.x, New_Sticker.y));			
			Stickers.push(Sticker_Props);
			Drawing_Canvas_Sticker.addChild(New_Sticker);
			
			//Sticker_Trash.visible = true;
			//CBBH.Edit_Top_Buttons("Sticker");
			
		}
		private function Create_Premade_Sticker(pID:int, Position:Point, Scale:Number):void{
			
			if(Stickers.length > 50){return;}
			
			var Sticker_Props:Array = new Array();
			var New_Sticker:flash.display.Sprite = Sticker_Assets.Get_Image("Sticker_Big_" + pID.toString());
			New_Sticker.getChildAt(0).x = -(New_Sticker.getChildAt(0).width / 2);
			New_Sticker.getChildAt(0).y = -(New_Sticker.getChildAt(0).height / 2);
			var Active:Boolean 	= false;
			var Off_Loc:Point 	= new Point(0, 0);
			
			New_Sticker.scaleX = New_Sticker.scaleY = Scale;
			
			New_Sticker.x = Position.x;
			New_Sticker.y = Position.y;	
			
			Sticker_Props.push(New_Sticker);
			Sticker_Props.push(Active);
			Sticker_Props.push(Off_Loc);
			Sticker_Props.push(false);
			Sticker_Props.push(pID);
			Sticker_Props.push(Scale);
			Sticker_Props.push(new Point(New_Sticker.x, New_Sticker.y));
			
			Stickers.push(Sticker_Props);
			Drawing_Canvas_Sticker.addChild(New_Sticker);
			
			//Sticker_Trash.visible = false;
			//CBBH.Edit_Top_Buttons("Idle");
			
		}
		private function Load_External_Stickers():void{
			
			//Extension.
			var FilePath:String = ((Page_Pack + "_Sticker_" + Current_Coloring_Page.toString()) + ".xml");				
			var myFile:File;
			myFile = File.applicationStorageDirectory;
			myFile = myFile.resolvePath( FilePath );			
			var Sticker_XML:XML;
			var stream:FileStream = new FileStream();
			
			if(myFile.exists){			
				
				stream.open(myFile, FileMode.READ);	
				Sticker_XML = XML(stream.readUTFBytes(stream.bytesAvailable));
				stream.close();	
				Process_Stickers(Sticker_XML);
				
			}else{
				
				Load_Glitter_Grid();
				
			}	
			
			//clear for Leaks.
			stream = null;
			Sticker_XML = null;			
			myFile = null;
			FilePath = null;
			
		}
		private function Process_Stickers(TheXML:XML):void{
			
			for(var X:int = 0; X < TheXML.ThisSticker.length(); X++){
				
				var Index:int = int(TheXML.ThisSticker[X].IdIndex.text());
				var Position:Point = new Point(int(TheXML.ThisSticker[X].pX.text()),
					int(TheXML.ThisSticker[X].pY.text()));
				var Scale:Number = Number(TheXML.ThisSticker[X].cS.text());
				
				Create_Premade_Sticker(Index, new Point(Position.x, Position.y), Scale);			
				
				Position = null;
				
			}
			
			Load_Glitter_Grid();
			
		}
		private function Click_Sticker(Position:Point):Boolean{
			
			for(var X:int = 0; X < Stickers.length; X++){
				if(!Stickers[X][1]){
					if(Stickers[X][0].hitTestPoint(Position.x, Position.y, true)){
						
						Position = Scene_Holder.globalToLocal(Position);
						
						//Sticker_Trash.visible = true;
						//CBBH.Edit_Top_Buttons("Sticker");
						
						Stickers[X][1] 		= true;
						
						Stickers[X][2].x 	= (Position.x - Stickers[X][0].x);
						Stickers[X][2].y 	= (Position.y - Stickers[X][0].y);
						
						Stickers[X][0].x 	= (Position.x - Stickers[X][2].x);
						Stickers[X][0].y 	= (Position.y - Stickers[X][2].y);		
						
						
						//Add Green Square.
						var square:flash.display.Sprite = new flash.display.Sprite();						
						square.graphics.lineStyle(3,0x00ff00);
						square.graphics.drawRect(0,0,Stickers[X][0].getChildAt(0).width, Stickers[X][0].getChildAt(0).height);
						square.x = -(square.width / 2);
						square.y = -(square.height / 2);
						Stickers[X][0].addChildAt(square, 0);
						
						Drawing_Canvas_Sticker.removeChild(Stickers[X][0]);
						Scene_Holder.addChild(Stickers[X][0]);
						return true;
					}				
				}
			}
			return false;
		}	
		private function Update_Sticker(Position:Point):void{
			
			Position = Scene_Holder.globalToLocal(Position);
			
			for(var X:int = 0; X < Stickers.length; X++){
				if(Stickers[X][1]){				
					
					if(TouchArray.length > 1){						
						
						Stickers[X][0].scaleX = Stickers[X][0].scaleY += ((Squeeze_Current - Squeeze_Last) / 100);
						
						var CurrentRotation:Number = Math.atan2((TouchArray[1][1].y - TouchArray[0][1].y), (TouchArray[1][1].x - TouchArray[0][1].x)) * (180 / Math.PI); 
						Stickers[X][0].rotation += (CurrentRotation - Rotation_Last);
						Rotation_Last = CurrentRotation;						
						
						if(Stickers[X][0].scaleX > 2.0){Stickers[X][0].scaleX = Stickers[X][0].scaleY = 2.0;}
						if(Stickers[X][0].scaleX < 0.5){Stickers[X][0].scaleX = Stickers[X][0].scaleY = 0.5;}
						
						Stickers[X][5] 		= Stickers[X][0].scaleX;
						
						Stickers[X][0].x 	= Position.x - Stickers[X][2].x;
						Stickers[X][0].y 	= Position.y - Stickers[X][2].y;
						
						Stickers[X][2].x 	= (Position.x - Stickers[X][0].x);
						Stickers[X][2].y 	= (Position.y - Stickers[X][0].y);
						
						Stickers[X][3] = false;
						
					}else{					
						
						if(Stickers[X][3]){
							Stickers[X][0].x = Position.x;
							Stickers[X][0].y = Position.y;
						}else{
							Stickers[X][0].x = Position.x - Stickers[X][2].x;
							Stickers[X][0].y = Position.y - Stickers[X][2].y;							
						}
						
						Stickers[X][6].x = Stickers[X][0].x;
						Stickers[X][6].y = Stickers[X][0].y;
						
					}
				}
			}
		}
		private function Drop_Sticker(Position:Point):void{
			
			for(var X:int = 0; X < Stickers.length; X++){
				if(Stickers[X][1]){					
					if(Interface_Hud.Get_Current_Up() == "TrashCan"){ //Hit Trash can.
						trace("Delete Sticker");
						Stickers[X][0].removeChildAt(0);
						Scene_Holder.removeChild(Stickers[X][0]);
						Stickers[X][0].getChildAt(0).bitmapData.dispose();
						Stickers[X][0].removeChildAt(0);
						Stickers.splice(X, 1);
						X--;					
						continue;
					}else{
						Stickers[X][1] = false;
						Stickers[X][3] = false;
						Stickers[X][0].removeChildAt(0);
						Scene_Holder.removeChild(Stickers[X][0]);
						Drawing_Canvas_Sticker.addChild(Stickers[X][0]);						
					}					
				}
			}
			
			//Sticker_Trash.visible = false;
			//CBBH.Edit_Top_Buttons("Idle");
			
		}
		private function Disposing_Stickers():void{
			for(var S:int = 0; S < Drawing_Canvas_Sticker.numChildren; S++){				
				Drawing_Canvas_Sticker.removeChildAt(S);
				S--
			}
			for(var X:int = 0; X < Stickers.length; X++){					
				Stickers[X][0].getChildAt(0).bitmapData.dispose();
				Stickers[X][0].removeChildAt(0);
				Stickers.splice(X, 1);
				X--;
			}
			Stickers.length = 0;
		}
		
		
		
		
		
		
		
		
		//Helpers.
		private function Get_Color_Alpha(Col:uint):uint{
			
			//Bit Shift store color single values for editing if needed.
			var r:uint = Col >> 16 & 0xFF;
			var g:uint = Col >>  8 & 0xFF;
			var b:uint = Col >>  0 & 0xFF;
			
			//Random Alpha value cap at 180.
			var a:uint = (Math.random() * 180);
			
			//Final Color Merge.
			var argb:uint = a << 24 | r << 16 | g << 8 | b;			
			
			return argb;
		}
		private function Dispose_Image(sObject:flash.display.Sprite):void{
			if(sObject.numChildren > 1){return;}
			if(sObject.numChildren > 0){
				Bitmap(sObject.getChildAt(0)).bitmapData.dispose();
				sObject.removeChildAt(0);
			}
			sObject = null;
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//Glitter.
		private function Initialize_Glitter_Particles():void{
		
			if(DeviceInfo.isSlow){trace("To Slow"); return;}
			
			GiltGo 		= false;
			GlitCounter = 0;
			Glitter_Map = new Array();
			for(var X:int = 0; X < (1366 * Costanza.SCALE_FACTOR); X++){
				var Collumb:Array = new Array();
				for(var Y:int = 0; Y < (768 * Costanza.SCALE_FACTOR); Y++){
					Collumb.push(0);
				}
				Glitter_Map.push(Collumb);
			}	
			
		}
		private function Load_Glitter_Grid():void{
			
			if(DeviceInfo.isSlow){trace("To Slow"); Process_Page_Loaded(); return;}
			
			var FilePath:String = ((Page_Pack + "_Glitter_" + Current_Coloring_Page.toString()));			
			var myFile:File;
			myFile = File.applicationStorageDirectory;
			myFile = myFile.resolvePath( FilePath );			
			var byteArray:ByteArray = new ByteArray();
			var stream:FileStream = new FileStream();
			
			if(myFile.exists){	
				
				stream.open(myFile, FileMode.READ);					
				stream.readBytes(byteArray);
				stream.close();					
				Glitter_Map = byteArray.readObject() as Array;	
				
				Process_Page_Loaded();
				
			}else{	
				
				Process_Page_Loaded();
				
			}	
			
			
			//clear for Leaks.
			stream = null;
			byteArray = null;			
			myFile = null;
			FilePath = null;
			
		}
		private function Clear_Glitter_Grid():void{
			
			if(DeviceInfo.isSlow){trace("To Slow"); return;}
			
			for(var X:int = 0; X < Glitter_Map.length; X++){
				for(var Y:int = 0; Y < Glitter_Map[X].length; Y++){
					Glitter_Map[X][Y] = 0;
				}
			}
			
			for(var G:int = 0; G < GlitCounter; G++){
				var Name:String = ("glit" +  G.toString());
				if(Drawing_Canvas_Holder.getChildByName(Name) == null){continue;}			
				var rBitmap:Bitmap = Bitmap(Drawing_Canvas_Holder.getChildByName(Name));
				TweenMax.killTweensOf(rBitmap);
				Drawing_Canvas_Holder.removeChild(rBitmap);
				rBitmap.bitmapData.dispose();
				rBitmap = null;
			}
			
			GlitCounter = 0;
			
		}
		private function Edit_Glitter_Grid(Loc:Point, s:int):void{
			
			if(DeviceInfo.isSlow){trace("To Slow"); return;}
			
			if(Loc.x < 0){Loc.x = 0;}
			if(Loc.x >= (1366 * Costanza.SCALE_FACTOR)){Loc.x = (1366 * Costanza.SCALE_FACTOR);}
			if(Loc.y < 0){Loc.y = 0;}
			if(Loc.y >= (768 * Costanza.SCALE_FACTOR)){Loc.y = (768 * Costanza.SCALE_FACTOR);}
			
			Glitter_Map[Math.floor(Loc.x)][Math.floor(Loc.y)] = 1;				
			
		}
		private function Erase_Glitter_Grid(Loc:Point, s:int):void{
			
			return;
			
			if(DeviceInfo.isSlow){trace("To Slow"); return;}
			
			var sX:int = (Loc.x - (s / 2));
			var sY:int = (Loc.y - (s / 2));
			
			for(var X:int = sX; X < (sX + s); X++){
				for(var Y:int = sY; Y < (sY + s); Y++){
					
					if(X < 0){continue;}
					if(X >= (1366 * Costanza.SCALE_FACTOR)){continue;}
					if(Y < 0){continue;}
					if(Y >= (768 * Costanza.SCALE_FACTOR)){continue;}	
					
					//here we check if we hit the mask.
					if(Glitter_Map[X][Y] == 1){
						Glitter_Map[X][Y] = 0;
					}					
					
				}
			}		
		}
		private function Erase_Glitter_Fill_Grid():void{		
			
			return;
			
			var w:int = (Draw_Layer_X + Canvas.Get_Layer_Section(Selected_Layer).width);
			var h:int = (Draw_Layer_Y + Canvas.Get_Layer_Section(Selected_Layer).height);
			
			for(var X:int = Draw_Layer_X; X < w; X++){
				for(var Y:int = Draw_Layer_Y; Y < h; Y++){
					
					if(Glitter_Map[X][Y] == 0){continue;}
					
					if(Glitter_Map[X][Y] == 1){
						if(Canvas.Layer_Colision(new Point(X, Y), null, Selected_Layer)){	
							Glitter_Map[X][Y] = 0;
						}
					}
					
				}
			}
			
		}
		private function Update_Glitter_Grid():void{
			
			if(DeviceInfo.isSlow){trace("To Slow"); return;}
			
			//return;
			if(!GiltGo){GiltGo = true; return;}
			GiltGo = false;
			
			for(var g:int = 0; g < (500* Costanza.SCALE_FACTOR); g++){
				
				var gX:int = Math.floor(Math.random() * (1366 * Costanza.SCALE_FACTOR));
				var gY:int = Math.floor(Math.random() * (768 * Costanza.SCALE_FACTOR));
				
				if(Glitter_Map[gX][gY] == 1){
					//Spawn Glitter.
					Spawn_Glitter(new Point(gX, gY));
				}
				
			}
			
		}
		private function Spawn_Glitter(p:Point):void{
			
			if(DeviceInfo.isSlow){trace("To Slow"); return;}
			
			var bitGlit:Bitmap 	= new Bitmap();
			bitGlit.bitmapData 	= new BitmapData(1, 1, false, 0xFFFFFF);
			bitGlit.scaleX = bitGlit.scaleY = 2.0;
			bitGlit.x 			= p.x;
			bitGlit.y 			= p.y;
			bitGlit.name        = ("glit" +  GlitCounter.toString());
			bitGlit.smoothing	= true;
			Drawing_Canvas_Holder.addChild(bitGlit);			
			GlitCounter++;			
			TweenMax.to(bitGlit, 0.8, {alpha:0.1, scaleX:0.1, scaleY:0.1, y:(p.y - 1), onComplete:Glitter_Complete, onCompleteParams:[bitGlit.name]});			
			
		}
		private function Glitter_Complete(Name:String):void{
			
			if(DeviceInfo.isSlow){trace("To Slow"); return;}
			
			if(Drawing_Canvas_Holder.getChildByName(Name) == null){return;}
			//trace("Clearing");
			var rBitmap:Bitmap = Bitmap(Drawing_Canvas_Holder.getChildByName(Name));
			TweenMax.killTweensOf(rBitmap);
			Drawing_Canvas_Holder.removeChild(rBitmap);
			rBitmap.bitmapData.dispose();
			rBitmap = null;
			
		}
		
		
		
		
		
		
		
		
		//Saving.
		private function Save_To_File():void{
			Pages_Icons_Assets.Update_Icon(Drawing_Canvas_Holder, (Current_Coloring_Page - 1));	
			Save_Glitter_Grid();
			Save_Canvas();
			Save_Stickers();	
			trace("Saving Completed");
		}
		private function Save_Glitter_Grid():void{
			
			if(DeviceInfo.isSlow){trace("To Slow"); return;}
			
			var byteArray:ByteArray = new ByteArray();
			byteArray.writeObject(Glitter_Map);			
			byteArray.position = 0;
			var FilePath:String = ((Page_Pack + "_Glitter_" + Current_Coloring_Page.toString()));
			var myFile:File;
			myFile = File.applicationStorageDirectory;
			myFile = myFile.resolvePath( FilePath );
			var stream:FileStream = new FileStream();
			stream.open( myFile , FileMode.WRITE);
			stream.writeBytes(byteArray, 0, byteArray.length);
			stream.close();
			stream 		= null;
			myFile 		= null;
			FilePath 	= null;			
		}
		private function Save_Canvas():void{
			var png:PNGEncoder = new PNGEncoder();			
			var byteArray:ByteArray = png.encode( Drawing_Canvas_Bitmap.bitmapData );
			var filename:String = ((Page_Pack + "_Page_" + Current_Coloring_Page.toString()) + ".png");
			var file:File = File.applicationStorageDirectory.resolvePath( filename );
			var wr:File = new File( file.nativePath );
			var stream:FileStream = new FileStream();
			stream.open(wr, FileMode.WRITE);
			stream.writeBytes ( byteArray, 0, byteArray.length );
			stream.close();	
			wr = null;
			file = null;
			filename = null;
			png = null;
			byteArray.clear();
			byteArray = null;	
		}
		private function Save_Stickers():void{
			var Sticker_XML:String;			
			Sticker_XML = "<Stickers>\n";			
			for(var X:int = 0; X < Stickers.length; X++){					
				Sticker_XML = Sticker_XML + "<ThisSticker>\n";				
				Sticker_XML = Sticker_XML + "<IdIndex>" + Stickers[X][4]   		+ "</IdIndex>" + "\n";
				Sticker_XML = Sticker_XML + "<pX>"  	+ Stickers[X][0].x 		+ "</pX>"  + "\n";
				Sticker_XML = Sticker_XML + "<pY>"  	+ Stickers[X][0].y 		+ "</pY>"  + "\n";			
				Sticker_XML = Sticker_XML + "<cS>"  	+ Stickers[X][5] 		+ "</cS>"  + "\n";
				Sticker_XML = Sticker_XML + "</ThisSticker>\n";				
			}			
			Sticker_XML = Sticker_XML + "</Stickers>";
			var FilePath:String = ((Page_Pack + "_Sticker_" + Current_Coloring_Page.toString()) + ".xml");
			var myFile:File;
			myFile = File.applicationStorageDirectory;
			myFile = myFile.resolvePath( FilePath );
			var stream:FileStream = new FileStream();
			stream.open( myFile , FileMode.WRITE);
			stream.writeUTFBytes(Sticker_XML.toString());
			stream.close();
			stream 		= null;
			myFile 		= null;
			FilePath 	= null;
			Sticker_XML = null;
		}
		
		
		
		
		
		
		
	}
}