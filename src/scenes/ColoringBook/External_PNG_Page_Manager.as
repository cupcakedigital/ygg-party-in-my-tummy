package scenes.ColoringBook
{
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.ImageLoader;
	import com.greensock.loading.LoaderMax;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.display.StageQuality;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.utils.ByteArray;
	
	import utils.PNGEncoder;

	public class External_PNG_Page_Manager{
		
		private var Master_Loader:LoaderMax;
	
		private var Page_Icons:Array;
		private var Usable_Icons:Array;
		private var CallBack:Function;
		private var PagePrefix:String;
		
		private var MaskIconSaver:flash.display.Sprite;
		
		//Load Page Icons and prep saved colored.
		public function External_PNG_Page_Manager(pCallBack:Function = null){
			Page_Icons			= new Array();
			Usable_Icons		= new Array();
			CallBack			= pCallBack;
			Master_Loader 		= new LoaderMax({onComplete:Loading_Completed, onError:errorHandler});
		}	
		
		public function Prep_Page_Selection_Manager(Core_Assets:External_PNG_Sheet_Manager, Prefix:String):void{
			
			PagePrefix 		= Prefix;
			MaskIconSaver 	= Core_Assets.Get_Image("PageIconMask");
			MaskIconSaver.cacheAsBitmap = true;
			var PageCount:int 		= 1;
			//So Based on Prefix we check for saved page colored.
			for(var X:int = 0; X < PageCount; X++){
				
				var New_Page_Icon_Data:Array 			= new Array();
				
				var Icon_Border:flash.display.Sprite 	= Core_Assets.Get_Image("PageSelGlow");				
				var PageIcon:flash.display.Sprite 		= Core_Assets.Get_Image("CBPageSel_" + Prefix + "_" + (X + 1).toString());	
				var SavedPageIcon:flash.display.Sprite  = new flash.display.Sprite();
				
				New_Page_Icon_Data.push(PageIcon);
				New_Page_Icon_Data.push(SavedPageIcon);
				New_Page_Icon_Data.push(Icon_Border);
				
				var PageSaved_Name:String = ("CBPageIconSaved_" + Prefix + "_" + (X + 1).toString() + ".png");	
				
				Master_Loader.append(new ImageLoader(File.applicationStorageDirectory.resolvePath(PageSaved_Name).url, {container:SavedPageIcon}));				
				//trace(File.applicationStorageDirectory.resolvePath(PageSaved_Name).nativePath);
				if(Core_Assets.Image_Exists("CBPageSel_" + Prefix + "_" + (X + 2).toString())){PageCount = (PageCount + 1);}				
				
				Page_Icons.push(New_Page_Icon_Data);
				
			}
			
			Master_Loader.load(true);
			
		}		
		private function errorHandler(e:LoaderEvent):void{	
			trace("Not Found");
		}
		private function Loading_Completed(e:LoaderEvent):void{	
					
			for(var X:int = 0; X < Page_Icons.length; X++){
				
				var CreatedIcon:flash.display.Sprite = new flash.display.Sprite();
				
				//trace(Page_Icons[X][1].getChildAt(0).width);
				
				//Add image.
				if(Page_Icons[X][1].width != 0){
					CreatedIcon.addChild(Page_Icons[X][1]);
				}else{
					CreatedIcon.addChild(Page_Icons[X][0]);
				}	
				
				//Add Border.
				CreatedIcon.addChild(Page_Icons[X][2]);
				Usable_Icons.push(CreatedIcon);
			}			
			
			//Now tell root we are done.
			if(CallBack != null){CallBack();}			
		}	
		
		
		//save Icon Data to file.
		public function Update_Icon(Data:flash.display.Sprite, PageId:int):void{			
			
			var Wisth:int = 300;
			var heist:int = 226;
			
			
			trace("UPDATE ICONSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS");
			var ScaleX:Number = ((Wisth * Costanza.SCALE_FACTOR) / (1024 * Costanza.SCALE_FACTOR));
			var ScaleY:Number = ((heist * Costanza.SCALE_FACTOR) / (768 * Costanza.SCALE_FACTOR));
			
			//Create new image icon.
			var New_Page_Icon:Bitmap 	= new Bitmap();
			New_Page_Icon.bitmapData 	= new BitmapData((Wisth * Costanza.SCALE_FACTOR), (heist * Costanza.SCALE_FACTOR), true, 0x000000);
			var mat:Matrix 				= new Matrix();
			mat.scale(ScaleX, ScaleY);
			mat.translate(((-171 * Costanza.SCALE_FACTOR) * ScaleX), 0);			
			New_Page_Icon.bitmapData.drawWithQuality(Data, mat, null, null, null, true, StageQuality.BEST);
			//New_Page_Icon.smoothing = true;
			var Final_Draw:Bitmap = new Bitmap();
			
			Final_Draw.bitmapData = new BitmapData(New_Page_Icon.width, New_Page_Icon.height, true, 0x000000);
			Final_Draw.bitmapData.copyPixels(New_Page_Icon.bitmapData, New_Page_Icon.bitmapData.rect, new Point(0, 0), Bitmap(MaskIconSaver.getChildAt(0)).bitmapData, new Point((-2 * Costanza.SCALE_FACTOR), (-2 * Costanza.SCALE_FACTOR)), true);
			//Final_Draw.bitmapData.copyPixels(New_Page_Icon.bitmapData, New_Page_Icon.bitmapData.rect, new Point(0, 0), null, null, true);
			//Final_Draw.smoothing = true;
			New_Page_Icon.bitmapData.dispose();
			New_Page_Icon = null;
			
			
			//If we are using a loaded saved Icon.
			if(Page_Icons[PageId][1].width != 0){
				
				//Remove the currentDisplayed Saved Icon.
				Usable_Icons[PageId].removeChild(Page_Icons[PageId][1]);				
								
				//Clear the stored saved data.
				//Bitmap(Page_Icons[PageId][1].getChildAt(0)).bitmapData.dispose();
				Page_Icons[PageId][1].removeChildAt(0);
				
				//Add the new Made Image to store.
				Page_Icons[PageId][1].addChildAt(Final_Draw, 0);
				
				//Re add the newly made image to the display.
				Usable_Icons[PageId].addChildAt(Page_Icons[PageId][1], 0);	
				
			}else{
				
				//Remove the currentDisplayed default Icon.
				Usable_Icons[PageId].removeChild(Page_Icons[PageId][0]);
				
				if(Page_Icons[PageId][1].width != 0){
					//Add the new Made Image to store.
					Page_Icons[PageId][1].addChild(Final_Draw);
				}else{
					Page_Icons[PageId][1] = new flash.display.Sprite();
					Page_Icons[PageId][1].addChildAt(Final_Draw, 0);
				}				
				
				//Re add the newly made image to the display.
				Usable_Icons[PageId].addChildAt(Page_Icons[PageId][1], 0);
			}
			
			//save to file.
			Save_To_File(Bitmap(Page_Icons[PageId][1].getChildAt(0)).bitmapData, ("CBPageIconSaved_" + PagePrefix + "_" + (PageId + 1).toString() + ".png"));
					
		}
		private function Save_To_File(Data:BitmapData, Name:String):void{			
			
			
			
			//var Finla_Holder:flash.display.Sprite = new flash.display.Sprite();			
			//var ImageHolder:flash.display.Sprite = new flash.display.Sprite();	
			
			//ImageHolder.addChild(Data);
			//ImageHolder.cacheAsBitmap = true;
			//ImageHolder.mask = MaskIconSaver;
			
			//Finla_Holder.addChild(ImageHolder);
			//Finla_Holder.addChild(MaskIconSaver);
			
			//var Final_Draw:BitmapData = new BitmapData(Data.width, Data.height, true, 0x000000);
			//Final_Draw.copyPixels(Data.bitmapData, Data.bitmapData.rect, new Point(0, 0), Bitmap(MaskIconSaver.getChildAt(0)).bitmapData, new Point(0, 0), false);
			
			var png:PNGEncoder 			= new PNGEncoder();			
			var byteArray:ByteArray 	= png.encode(Data);				
			var filename:String 		= Name;	
			var file:File 				= File.applicationStorageDirectory.resolvePath(filename);
			var wr:File 				= new File(file.nativePath);
			var stream:FileStream 		= new FileStream();			
			
			stream.open(wr, FileMode.WRITE);			
			stream.writeBytes(byteArray, 0, byteArray.length);			
			stream.close();	
			
			wr 			= null;
			file 		= null;
			filename 	= null;
			png 		= null;
			
			byteArray.clear();
			byteArray 	= null;			
			
			//Final_Draw.dispose();
			//Finla_Holder.removeChild(ImageHolder);
			//Finla_Holder.removeChild(MaskIconSaver);
			//ImageHolder.removeChild(Data);
			//ImageHolder.mask = null;
			
			//ImageHolder = null;
			//Finla_Holder = null;
			
		}		
		public function Purge_Assets():void{
			
			CallBack = null;
			
			for(var uI:int = 0; uI < Usable_Icons.length; uI++){				
				Usable_Icons[uI].removeChildAt(1);
				Usable_Icons[uI].removeChildAt(0);
				Usable_Icons[uI] = null;			
			}Usable_Icons.length = 0;
			
			for(var pI:int = 0; pI < Page_Icons.length; pI++){				
				
				Bitmap(Page_Icons[pI][0].getChildAt(0)).bitmapData.dispose();
				Page_Icons[pI][0].removeChildAt(0);
				Page_Icons[pI][0] = null;
				
				if(Page_Icons[pI][1].width != 0){
					Page_Icons[pI][1] = null;
				}
				
				Bitmap(Page_Icons[pI][2].getChildAt(0)).bitmapData.dispose();
				Page_Icons[pI][2].removeChildAt(0);
				Page_Icons[pI][2] = null;
				
			}
			
		}
		public function Kill_Me():void{	
			
			Master_Loader.dispose(true);
		}		
		
		
		//Values to use.
		public function getCount():int{return Usable_Icons.length;}
		public function getIcon(Index:int):flash.display.Sprite{return Usable_Icons[Index];}
		
	}
}