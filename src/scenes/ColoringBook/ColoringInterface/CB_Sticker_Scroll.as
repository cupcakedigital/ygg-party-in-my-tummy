package scenes.ColoringBook.ColoringInterface{
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import scenes.ColoringBook.External_PNG_Sheet_Manager;
	
	public class CB_Sticker_Scroll extends Sprite{
		
		//Props.
		private var CurrentSelectedItem:int;
		private var Offset:int;
		private var Draw_Loc:int;
		private var isActive:Boolean;
		private var Colision_Rectangles:Array;
		private var Ids:Array;
		private var Last_X:int;
		
		//Holders.
		private var Master_Holder:flash.display.MovieClip;
		private var Selections_Mask_Holder:flash.display.MovieClip;
		private var Selections_Bar_Holder:flash.display.MovieClip;
		
		//Images.		
		private var Selection_Bitmap:Bitmap;
		
		//Presets.		
		private var pImages:Array;		
		private var Mask:flash.display.Sprite;
		
		//distance check.
		private var First_Postion:Point;
		private var Last_Postion:Point;
		
		private var ScalerSize:Number;
		
		public function CB_Sticker_Scroll(Core_Assets:External_PNG_Sheet_Manager){			
			
			super();
			
			Mask					= Core_Assets.Get_Image("BH_Mask");	
			First_Postion 			= new Point(0, 0);
			Last_Postion 			= new Point(0, 0);			
			Offset 					= 10;			
			var Sticker_Count:int 	= 1;
			var Max_Height:int 		= 0;
			var Max_Widtht:int 		= 0;
			ScalerSize				= 0.2;
			
			Ids 					= new Array();			
			pImages 				= new Array();
			
						
			//Grab available stickers.
			for(var P:int = 0; P < Sticker_Count; P++){	
				
				var nImage:flash.display.Sprite = Core_Assets.Get_Image("Sticker_Small_" + (P + 1).toString());					
				if(nImage.height > Max_Height){Max_Height = nImage.height;}
				Max_Widtht = ((Max_Widtht + (nImage.width * ScalerSize)) + Offset);
				pImages.push(nImage);	
				Ids.push(P);
				if(Core_Assets.Image_Exists("Sticker_Small_" + (P + 2).toString())){Sticker_Count = (Sticker_Count + 1);}
				else{Max_Widtht = (Max_Widtht - Offset);}
				
			}		
			
			Max_Height						= Mask.height;
			isActive 						= true;
			Colision_Rectangles 			= new Array();
			CurrentSelectedItem 			= 0;		
			
			//Draw the stickers.
			Selection_Bitmap 				= new Bitmap();
			Selection_Bitmap.bitmapData 	= new BitmapData(Max_Widtht, Max_Height, true, 0x000000);	
			Selection_Bitmap.y 				= 0;	
			Selection_Bitmap.x 				= 0;				
			Draw_Loc 						= 0;
			
			for(var X:int = 0; X < pImages.length; X++){											
				Build_Selection(pImages[X], Max_Height);				
			}	
			
			Selection_Bitmap.smoothing = true;
			
			//Remove Prefabs.
			for(var I:int = 0; I < pImages.length; I++){
				Bitmap(pImages[I].getChildAt(0)).bitmapData.dispose();
				pImages[I].removeChildAt(0);
				pImages[I] = null;
				pImages.splice(I, 1);
				I--;
			}			
			
			Master_Holder 							= new flash.display.MovieClip();
			Selections_Mask_Holder 					= new flash.display.MovieClip();			
			Selections_Bar_Holder 					= new flash.display.MovieClip();			
			
			Selections_Bar_Holder.addChild(Selection_Bitmap);			
			Selections_Mask_Holder.addChild(Mask);	
			
			Selections_Bar_Holder.cacheAsBitmap 	= true;			
			Selections_Mask_Holder.cacheAsBitmap 	= true;			
			
			Selections_Bar_Holder.mask 				= Selections_Mask_Holder;			
			
			Master_Holder.addChild(Selections_Bar_Holder);	
			Master_Holder.addChild(Selections_Mask_Holder);	
			
			this.x 						= -(Mask.width / 2);
			this.y 						= (13 * Costanza.SCALE_FACTOR);
			this.addChild(Master_Holder);
		}
		
		private function Build_Selection(pI:flash.display.Sprite, Full_Height:int):void{		
						
			var Mat:Matrix = new Matrix();
			Mat.scale(ScalerSize, ScalerSize);
			
			var NewImage:BitmapData = new BitmapData(int(pI.width * ScalerSize), int(pI.height * ScalerSize), true, 0x000000);
			NewImage.draw(pI, Mat);
			
			var dPoint:Point 			= new Point(Draw_Loc, ((Full_Height / 2) - (NewImage.height / 2)));
			var dRectangle:Rectangle 	= new Rectangle(0, 0, NewImage.width, NewImage.height);	
			
			Selection_Bitmap.bitmapData.copyPixels(NewImage, dRectangle, dPoint, null, null, true);			
			Colision_Rectangles.push(new Rectangle((Selection_Bitmap.x + dPoint.x), 0, NewImage.width, Mask.height));
			
			Draw_Loc = (Draw_Loc + NewImage.width + Offset);
			
			NewImage.dispose();
			Mat = null;
			
		}
		public function Dispose_Bar():void{
			
			//Arrays.
			Ids.length 					= 0;			
			Colision_Rectangles.length 	= 0;
			
			//Holders.
			Master_Holder.removeChild(Selections_Mask_Holder);
			Master_Holder.removeChild(Selections_Bar_Holder);
			Master_Holder = null;
			
			Selections_Mask_Holder.removeChild(Mask);
			Selections_Mask_Holder = null;
			
			Selections_Bar_Holder.removeChild(Selection_Bitmap);			
			Selections_Bar_Holder = null;
			
			//Images.		
			Selection_Bitmap.bitmapData.dispose();	
			Selection_Bitmap = null;							
			
			//Mask.
			Bitmap(Mask.getChildAt(0)).bitmapData.dispose();
			Mask.removeChildAt(0);
			Mask = null;
			
		}		
		
		public function Click_Bar(Position:Point):int{	
			
			if(!isActive){return -1;}			
			
			//New Check Recs.
			var Hit_Point:Point = new Point(this.globalToLocal(Position).x, this.globalToLocal(Position).y);
			Hit_Point.x = (Hit_Point.x - Selections_Bar_Holder.x);
			for(var X:int = 0; X < Colision_Rectangles.length; X++){				
				if(Hit_Point.x > Rectangle(Colision_Rectangles[X]).x){
					if(Hit_Point.x < (Rectangle(Colision_Rectangles[X]).x + Rectangle(Colision_Rectangles[X]).width)){
						if(Hit_Point.y > Rectangle(Colision_Rectangles[X]).y){
							if(Hit_Point.y < (Rectangle(Colision_Rectangles[X]).y + Rectangle(Colision_Rectangles[X]).height)){
								First_Postion.x = this.globalToLocal(Position).x;
								First_Postion.y = this.globalToLocal(Position).y;
								CurrentSelectedItem = Ids[X];
								return X;
							}
						}
					}
				}				
			}
			
			return -1;
		}
		public function Up_Bar(Position:Point):int{	
			
			if(!isActive){return -1;}
			
			//New Check Recs.
			var Hit_Point:Point = new Point(this.globalToLocal(Position).x, this.globalToLocal(Position).y);
			Hit_Point.x = (Hit_Point.x - Selections_Bar_Holder.x);
			for(var X:int = 0; X < Colision_Rectangles.length; X++){				
				if(Hit_Point.x > Rectangle(Colision_Rectangles[X]).x){
					if(Hit_Point.x < (Rectangle(Colision_Rectangles[X]).x + Rectangle(Colision_Rectangles[X]).width)){
						if(Hit_Point.y > Rectangle(Colision_Rectangles[X]).y){
							if(Hit_Point.y < (Rectangle(Colision_Rectangles[X]).y + Rectangle(Colision_Rectangles[X]).height)){								
								Last_X = this.globalToLocal(Position).x;
								var distance:int = (Last_X - First_Postion.x);
								if(distance <= 0){distance = -distance;}									
								if(distance < 100){
									CurrentSelectedItem = Ids[X];										
									return CurrentSelectedItem;
								}else{return - 1;}
							}
						}
					}
				}				
			}
			
			return -1;
			
		}
		public function Move_Bar(Force:int, Position:Point):void{				
			
			if(!isActive){return;}
			
			//Scroll the bar.
			Selections_Bar_Holder.x = (Selections_Bar_Holder.x + (Force * 1.0));
			//var Limit:int = -(Selections_Bar_Holder.width - Mask.width - (Mask.width / 2));
			var Limit:int = -(Selection_Bitmap.width - Mask.width);
			//trace(Limit + " " + Selections_Bar_Holder.x);
			
			if(Selections_Bar_Holder.x < Limit){Selections_Bar_Holder.x = Limit;}
			if(Selections_Bar_Holder.x > 0){Selections_Bar_Holder.x = 0;}
			
			Last_Postion.x = this.globalToLocal(Position).x;
			Last_Postion.y = this.globalToLocal(Position).y;
			
		}		
		public function Get_Selection():int{
			return CurrentSelectedItem;
		}
		public function Get_Active():Boolean{
			return isActive;
		}
		public function Deactivate_Bar():void{	
			if(!isActive){return;}
			Master_Holder.visible = false;
			isActive = false;			
		}
		public function Activate_Bar():void{	
			if(isActive){return;}
			Master_Holder.visible = true;
			isActive = true;	
		}
	}
}