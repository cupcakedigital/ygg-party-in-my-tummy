package scenes.ColoringBook.ColoringInterface{
	
	import com.greensock.TweenMax;
	import flash.display.Sprite;
	import flash.geom.Point;	
	import scenes.ColoringBook.External_PNG_Sheet_Manager;
	
	public class CB_HUD_MoreTab extends Sprite{
		
		private var StateStatus:String;
		private var Closed_Position:int;
		private var Opened_Position:int;
		private var Closed_Rotation:int;
		private var Opened_Rotation:int;
		
		private var BackPlate:InterfaceButton;		
		private var Recycle_Button:InterfaceButton;
		private var Undo_Button:InterfaceButton;
		private var Camera_Button:InterfaceButton;
		private var More_Button:InterfaceButton;
		
		//Build.
		public function CB_HUD_MoreTab(Core_Assets:External_PNG_Sheet_Manager){
			
			super();	
			
			StateStatus 		= "closed";
			Closed_Position		= (-205 * Costanza.SCALE_FACTOR);
			Opened_Position		= 0;
			Closed_Rotation		= 0;
			Opened_Rotation		= 180;
			
			BackPlate 			= new InterfaceButton(Core_Assets.Get_Image("TH_Plate"));			
			Recycle_Button 		= new InterfaceButton(Core_Assets.Get_Image("TH_Recycle_Static_BTN"), 	Core_Assets.Get_Image("TH_Recycle_Glow_BTN"));
			Undo_Button 		= new InterfaceButton(Core_Assets.Get_Image("TH_Undo_Static_BTN"), 		Core_Assets.Get_Image("TH_Undo_Glow_BTN"));
			Camera_Button 		= new InterfaceButton(Core_Assets.Get_Image("TH_Camera_Static_BTN"), 	Core_Assets.Get_Image("TH_Camera_Glow_BTN"));
			More_Button 		= new InterfaceButton(Core_Assets.Get_Image("TH_More_BTN_Static"), 	Core_Assets.Get_Image("TH_More_BTN_Glow"));
			
			BackPlate.x 		= (BackPlate.width  / 2); 
			BackPlate.y 		= (BackPlate.height / 2); 
			
			Recycle_Button.x 	= (BackPlate.width / 2);
			Undo_Button.x 		= (BackPlate.width / 2);
			Camera_Button.x 	= (BackPlate.width / 2);
			More_Button.x 		= (BackPlate.width / 2);
			
			Recycle_Button.y	= (30 * Costanza.SCALE_FACTOR);
			Undo_Button.y		= (95 * Costanza.SCALE_FACTOR);
			Camera_Button.y		= (150 * Costanza.SCALE_FACTOR);
			More_Button.y		= (230 * Costanza.SCALE_FACTOR);
			
			this.y 				= Closed_Position;
			this.x 				= (171 * Costanza.SCALE_FACTOR);
			
			this.addChild(BackPlate);
			this.addChild(Recycle_Button);
			this.addChild(Undo_Button);
			this.addChild(Camera_Button);
			this.addChild(More_Button);
			
		}
		
		//Clean.
		public function Destroy():void{
			
			TweenMax.killTweensOf(this);
			TweenMax.killTweensOf(More_Button);	
			
			this.removeChild(BackPlate);
			this.removeChild(Recycle_Button);
			this.removeChild(Undo_Button);
			this.removeChild(Camera_Button);
			this.removeChild(More_Button);
			
			BackPlate.Destroy();
			Recycle_Button.Destroy();
			Undo_Button.Destroy();
			Camera_Button.Destroy();
			More_Button.Destroy();
			
		}		
		
		//Inputs.
		public function Down(MousePosition:Point):String{
			
			if(More_Button.Down(MousePosition)){
				Opperate_Mechanism();				
				return "More";
			}	
			
			if(!BackPlate.Pixel_Colision(MousePosition)){
				if(StateStatus == "opened"){
					Opperate_Mechanism();
				}
				return "Nothing";
			}
			
			if(Recycle_Button.Down(MousePosition)){	return "Recycle";}
			if(Undo_Button.Down(MousePosition)){	return "Undo";}
			if(Camera_Button.Down(MousePosition)){	return "Camera";}			
			
			return "Plate";
		}
		public function Up(MousePosition:Point):String{
			
			Recycle_Button.Reset();
			Undo_Button.Reset();
			Camera_Button.Reset();
			More_Button.Reset();
			
			if(!BackPlate.Pixel_Colision(MousePosition)){return "Nothing";}
			
			if(Recycle_Button.Up(MousePosition)){		return "Recycle";}
			if(Undo_Button.Up(MousePosition)){			return "Undo";}
			if(Camera_Button.Up(MousePosition)){		return "Camera";}			
			if(More_Button.Up(MousePosition)){			return "More";}			
			
			return "Plate";
		}			
		
		//Animate.
		private function Opperate_Mechanism():void{
			switch(StateStatus){
				
				case "opened":{	
					
					TweenMax.killTweensOf(this);			
					TweenMax.to(this, 0.5, {y:Closed_Position});
					
					TweenMax.killTweensOf(More_Button);			
					TweenMax.to(More_Button, 0.5, {rotation:Closed_Rotation});	
					
					StateStatus = "closed";
					
				}break;
				
				case "closed":{
					
					TweenMax.killTweensOf(this);			
					TweenMax.to(this, 0.5, {y:Opened_Position});
					
					TweenMax.killTweensOf(More_Button);			
					TweenMax.to(More_Button, 0.5, {rotation:Opened_Rotation});	
					
					StateStatus = "opened";
					
				}break;
				
			}
		}			
	}
}