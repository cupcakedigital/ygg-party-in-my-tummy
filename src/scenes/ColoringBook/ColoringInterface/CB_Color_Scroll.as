package scenes.ColoringBook.ColoringInterface{
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import scenes.ColoringBook.External_PNG_Sheet_Manager;
	
	public class CB_Color_Scroll extends Sprite{
		
		//Props.
		private var CurrentSelectedItem:int;
		private var Offset:int;				
		private var isActive:Boolean;
		private var Colision_Rectangles:Array;
		
		//Holders.
		private var Master_Holder:flash.display.MovieClip;
		private var Selections_Mask_Holder:flash.display.MovieClip;
		private var Selections_Bar_Holder:flash.display.MovieClip;	
		
		//Images.		
		private var Selection_Bitmap:Bitmap;				
		private var Selection_Mask:Bitmap;		
		
		//New Values.
		private var Selector:flash.display.Sprite;
		private var Fill_Patern:flash.display.Sprite;
		private var Stroke_Patern:flash.display.Sprite;
		private var Mask:flash.display.Sprite;		
		
		public function CB_Color_Scroll(Core_Assets:External_PNG_Sheet_Manager, pColor_List:Vector.<uint>){			
			
			super();				
			
			Fill_Patern 			= Core_Assets.Get_Image("BH_cFill");
			Stroke_Patern 			= Core_Assets.Get_Image("BH_cStroke");
			Selector 				= Core_Assets.Get_Image("BH_Color_Selection_Dot");	
			Mask					= Core_Assets.Get_Image("BH_Mask");	
			
			isActive 				= true;			
			CurrentSelectedItem 	= 7;
			Offset 					= 10;				
			
			//Build the colors.
			var Selector_Width:int 		= (pColor_List.length * Stroke_Patern.width) + ((pColor_List.length - 1) * Offset);				
			Selection_Bitmap 			= new Bitmap();
			Selection_Bitmap.bitmapData = new BitmapData(Selector_Width, Mask.height, true, 0x000000);	
			Selection_Bitmap.y 			= (-1 * Costanza.SCALE_FACTOR);	
			Selection_Bitmap.x			= 0;
			Colision_Rectangles 		= new Array();	
			for(var X:int = 0; X < pColor_List.length; X++){
				Build_Selection((X * Stroke_Patern.width), (Stroke_Patern.height / 2), ((Offset * X) + (Stroke_Patern.width / 2)), pColor_List[X]);		
			}			
			Selection_Bitmap.smoothing 				= true;						
			
			Master_Holder 							= new flash.display.MovieClip();
			Selections_Mask_Holder 					= new flash.display.MovieClip();			
			Selections_Bar_Holder 					= new flash.display.MovieClip();			
			
			Selections_Bar_Holder.addChild(Selection_Bitmap);	
			Selections_Bar_Holder.addChild(Selector);
			Selections_Mask_Holder.addChild(Mask);	
			
			Selections_Bar_Holder.cacheAsBitmap 	= true;			
			Selections_Mask_Holder.cacheAsBitmap 	= true;	
			
			Selections_Bar_Holder.mask 				= Selections_Mask_Holder;			
			
			Master_Holder.addChild(Selections_Bar_Holder);			
			Master_Holder.addChild(Selections_Mask_Holder);
			
			Update_Bar();			
			
			Selections_Bar_Holder.x 	= 0;
			Selector.y 					= (-2 * Costanza.SCALE_FACTOR);
			
			this.x 						= int(-(Mask.width / 2));
			this.y 						= (14 * Costanza.SCALE_FACTOR);
			this.addChild(Master_Holder);
			
		}
		
		private function Build_Selection(pX:int, pY:int, pO:int, pC:uint):void{
			
			var MB:MovieClip 					= new MovieClip();
			MB.addChild(Fill_Patern);
			var CT:ColorTransform 				= new ColorTransform();
			CT.color 							= pC;			
			MB.transform.colorTransform 		= CT; 
			var DC:MovieClip 					= new MovieClip();
			DC.addChild(MB);
			
			var Mat:Matrix 						= new Matrix();
			var Mx:int 							= ((Stroke_Patern.width  - Fill_Patern.width) / 2);
			var My:int 							= ((Stroke_Patern.height - Fill_Patern.height) / 2);
			
			Mat.translate(Mx, My);
			var Draw_BitmapData:BitmapData 		= new BitmapData(Stroke_Patern.width, Stroke_Patern.height, true, 0x000000);
			Draw_BitmapData.draw(DC, Mat);		
			Draw_BitmapData.draw(Stroke_Patern);
			
			var dPoint:Point 					= new Point((pX - (Stroke_Patern.width / 2)) + pO, 0);
			var dRectangle:Rectangle 			= new Rectangle(0, 0, Stroke_Patern.width, Stroke_Patern.height);			
			
			Selection_Bitmap.bitmapData.copyPixels(Draw_BitmapData, dRectangle, dPoint, null, null, true);				
			Colision_Rectangles.push(new Rectangle((Selection_Bitmap.x + dPoint.x), (Selection_Bitmap.y + dPoint.y), Stroke_Patern.width, Stroke_Patern.height));
			
			Draw_BitmapData.dispose();
			Draw_BitmapData 					= null;
			
			DC.removeChildAt(0);
			DC 									= null;
			
			MB.removeChildAt(0);
			MB 									= null;
			
			CT 									= null;
			
		}
		public function Dispose_Bar():void{
			
			//Recs.
			Colision_Rectangles.length = 0;
			
			//This.
			this.removeChild(Master_Holder);			
			
			//Holders.
			Master_Holder.removeChild(Selections_Mask_Holder);
			Master_Holder.removeChild(Selections_Bar_Holder);
			Master_Holder = null;
			
			Selections_Mask_Holder.removeChild(Mask);
			Selections_Mask_Holder = null;
			
			Bitmap(Mask.getChildAt(0)).bitmapData.dispose();
			Mask.removeChildAt(0);
			Mask = null;		
			
			Selections_Bar_Holder.removeChild(Selection_Bitmap);
			Selections_Bar_Holder.removeChild(Selector);
			Selections_Bar_Holder = null;

			Selection_Bitmap.bitmapData.dispose();	
			Selection_Bitmap = null;					
			
			Bitmap(Selector.getChildAt(0)).bitmapData.dispose();
			Selector.removeChildAt(0);
			Selector = null;				
			
			Bitmap(Fill_Patern.getChildAt(0)).bitmapData.dispose();
			Fill_Patern.removeChildAt(0);
			Fill_Patern = null;
			
			Bitmap(Stroke_Patern.getChildAt(0)).bitmapData.dispose();
			Stroke_Patern.removeChildAt(0);
			Stroke_Patern = null;
			
		}		
		
		public function Click_Bar(Position:Point):int{	
			
			if(!isActive){return -1;}
			
			//New Check Recs.
			var Hit_Point:Point = new Point(this.globalToLocal(Position).x, this.globalToLocal(Position).y);
			Hit_Point.x = (Hit_Point.x - Selections_Bar_Holder.x);
			for(var X:int = 0; X < Colision_Rectangles.length; X++){				
				if(Hit_Point.x > Rectangle(Colision_Rectangles[X]).x){
					if(Hit_Point.x < (Rectangle(Colision_Rectangles[X]).x + Rectangle(Colision_Rectangles[X]).width)){
						if(Hit_Point.y > Rectangle(Colision_Rectangles[X]).y){
							if(Hit_Point.y < (Rectangle(Colision_Rectangles[X]).y + Rectangle(Colision_Rectangles[X]).height)){								
								CurrentSelectedItem = X;								
								Update_Bar(false);
								return X;
							}
						}
					}
				}				
			}
			
			return -1;
		}
		public function Update_Bar(Adjust:Boolean = false):void{					
			if(!isActive){return;}	
			if(Adjust){
				Selections_Bar_Holder.x = -((CurrentSelectedItem * Stroke_Patern.width) + (CurrentSelectedItem * Offset)) + (Mask.width / 2) - (Stroke_Patern.width / 2);
				var Limit:int = -(Selection_Bitmap.width - Mask.width);			
				if(Selections_Bar_Holder.x < Limit){Selections_Bar_Holder.x = Limit;}
				if(Selections_Bar_Holder.x > 0){Selections_Bar_Holder.x = 0;}	
			}
			Selector.x = ((((CurrentSelectedItem * Stroke_Patern.width) + (CurrentSelectedItem * Offset)) + (Stroke_Patern.width / 2)) - (Selector.width / 2));	
		}
		public function Move_Bar(Force:int):void{	
			
			if(!isActive){return;}
		
			Selections_Bar_Holder.x = (Selections_Bar_Holder.x + (Force * 1.0));
			var Limit:int = -(Selection_Bitmap.width - Mask.width);
			
			if(Selections_Bar_Holder.x < Limit){Selections_Bar_Holder.x = Limit;}
			if(Selections_Bar_Holder.x > 0){Selections_Bar_Holder.x = 0;}			
			
		}
		public function Get_Selection():int{
			return CurrentSelectedItem;
		}
		public function Get_Active():Boolean{
			return isActive;
		}
		public function Deactivate_Bar():void{	
			if(!isActive){return;}
			Master_Holder.visible 	= false;
			isActive 				= false;			
		}
		public function Activate_Bar(sI:int):void{	
			if(isActive){return;}			
			CurrentSelectedItem 	= sI;
			Master_Holder.visible 	= true;
			isActive 				= true;	
			Update_Bar(false);
		}
		
		
	}
}