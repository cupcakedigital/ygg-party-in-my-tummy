package scenes.ColoringBook.ColoringInterface{
	
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	public class InterfaceButton extends Sprite	{		
		
		private var STATIC_sp:flash.display.Sprite;
		private var ACTIVE_sp:flash.display.Sprite;
		private var isDown:Boolean;
				
		public function InterfaceButton(Static_sp:flash.display.Sprite, Active_sp:flash.display.Sprite = null){
			
			super();	
			
			isDown			= false;
			
			STATIC_sp 		= Static_sp;
			ACTIVE_sp 		= Active_sp;			
			
			STATIC_sp.x 	= -(STATIC_sp.width  / 2);
			STATIC_sp.y 	= -(STATIC_sp.height / 2);
			
			if(Active_sp != null){
				ACTIVE_sp.x 	= -(ACTIVE_sp.width  / 2);
				ACTIVE_sp.y 	= -(ACTIVE_sp.height / 2);
				ACTIVE_sp.alpha = 0;
			}		
			
			this.addChild(STATIC_sp);
			
			if(ACTIVE_sp != null){
				this.addChild(ACTIVE_sp);
			}
			
		}
		public function Destroy():void{
			
			TweenMax.killTweensOf(ACTIVE_sp);
			
			this.removeChild(STATIC_sp);
			if(ACTIVE_sp != null){
				this.removeChild(ACTIVE_sp);
			}
			
			if(ACTIVE_sp != null){
				Bitmap(ACTIVE_sp.getChildAt(0)).bitmapData.dispose();
				ACTIVE_sp.removeChildAt(0);
				ACTIVE_sp = null;
			}
			
			Bitmap(STATIC_sp.getChildAt(0)).bitmapData.dispose();
			STATIC_sp.removeChildAt(0);
			STATIC_sp = null;			
			
		}
		
		//Hit Down.
		public function Down(MousePosition:Point):Boolean{			
			
			if(STATIC_sp.hitTestPoint(MousePosition.x, MousePosition.y, true)){
				
				if(isDown){return false;}
				else{isDown = true;}
				
				Active_Go();
				return true;
			}
			
			return false;
			
		}
		
		//Hit Up.
		public function Up(MousePosition:Point):Boolean{
			
			isDown = false;
			
			if(STATIC_sp.hitTestPoint(MousePosition.x, MousePosition.y, true)){				
				return true;
			}
			
			return false;
			
		}	
		
		//Return to Static state.
		public function Reset():void{
			Active_Kill();
			isDown = false;
		}	
		public function Force_Activate():void{			
			isDown = true;
			Active_Go();			
		}	
		
		//Go Active.
		private function Active_Go():void{	
			if(ACTIVE_sp == null){return;}
			TweenMax.killTweensOf(ACTIVE_sp);
			ACTIVE_sp.alpha = 1.0;
		}
		private function Active_Kill():void{	
			if(ACTIVE_sp == null){return;}
			if(ACTIVE_sp.alpha <= 0.0){return;}
			TweenMax.killTweensOf(ACTIVE_sp);			
			TweenMax.to(ACTIVE_sp, 0.5, {alpha:0, onComplete:Active_Done});			
		}
		private function Active_Done():void{
			if(ACTIVE_sp == null){return;}
			TweenMax.killTweensOf(ACTIVE_sp);
		}	
		
		//Glow.
		public function Set_Glow():void{
			
			var Object_Glow:GlowFilter;
		
			Object_Glow 			= new GlowFilter;			
			Object_Glow.color 		= 0xFFFFFF;
			Object_Glow.alpha 		= 1;
			Object_Glow.blurX 		= 5;
			Object_Glow.blurY 		= 5;
			Object_Glow.strength  	= 10;
			Object_Glow.quality   	= BitmapFilterQuality.HIGH;
			
			STATIC_sp.filters 		= [Object_Glow];
			
		}		
		public function Remove_Glow():void{
			STATIC_sp.filters 		= [];
		}
		
		//Pixel Colision on object.
		public function Pixel_Colision(MousePosition:Point):Boolean{
			
			//Set location.			
			var sX:int =  STATIC_sp.globalToLocal(MousePosition).x;
			var sY:int =  STATIC_sp.globalToLocal(MousePosition).y;
			
			//Are colision testes color.
			var BitmapColision:BitmapData = new BitmapData(1, 1, true, 0x000000);
			
			//Frame of draw.
			var bRec:Rectangle = new Rectangle(0, 0, 1, 1);
			
			//Translation matrix.
			var bMatrix:Matrix = new Matrix();
			bMatrix.translate(-sX, -sY);
			
			//Draw the pixel.
			BitmapColision.draw(STATIC_sp, bMatrix, null, null, bRec, false);		
			
			//Check for colision alpha.
			if(BitmapColision.getPixel(0, 0).toString(16) != "0"){			
				//clean.
				BitmapColision.dispose();
				return true;				
			}else{				
				//Clean.
				BitmapColision.dispose();
				return false;				
			}
			
		}
		
	}
}