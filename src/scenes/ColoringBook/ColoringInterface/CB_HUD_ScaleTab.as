package scenes.ColoringBook.ColoringInterface{
	
	import com.greensock.TweenMax;
	
	import flash.display.Sprite;
	import flash.geom.Point;
	
	import scenes.ColoringBook.External_PNG_Sheet_Manager;
	
	public class CB_HUD_ScaleTab extends Sprite{
		
		private var Active:Boolean;
		private var Selected_Scale:Number;
		
		private var Scale_1:InterfaceButton;			
		private var Scale_2:InterfaceButton;
		private var Scale_3:InterfaceButton;	
		
		public function CB_HUD_ScaleTab(Core_Assets:External_PNG_Sheet_Manager){
			
			super();
			
			Selected_Scale 		= 1.0;
			Active				= false;
			
			Scale_1 			= new InterfaceButton(Core_Assets.Get_Image("SH_Size_1_Static"), Core_Assets.Get_Image("SH_Size_1_Glow"));
			Scale_2 			= new InterfaceButton(Core_Assets.Get_Image("SH_Size_2_Static"), Core_Assets.Get_Image("SH_Size_2_Glow"));
			Scale_3 			= new InterfaceButton(Core_Assets.Get_Image("SH_Size_3_Static"), Core_Assets.Get_Image("SH_Size_3_Glow"));			
			
			Scale_1.x 			= 0;
			Scale_1.y 			= -(Scale_1.height / 2);
			
			Scale_2.x 			= 0;
			Scale_2.y 			= (-(Scale_1.height / 2) * 3);
			
			Scale_3.x 			= 0;
			Scale_3.y 			= (-(Scale_1.height / 2) * 5);
			
			this.addChild(Scale_1);
			this.addChild(Scale_2);
			this.addChild(Scale_3);
			
		}
		public function Destroy():void{
			
			TweenMax.killTweensOf(this);
			
			this.removeChild(Scale_1);
			this.removeChild(Scale_2);
			this.removeChild(Scale_3);
			
			Scale_1.Destroy();
			Scale_2.Destroy();
			Scale_3.Destroy();
			
		}	
		public function Activate():void{
			
			switch(Selected_Scale){
				case 0.5:{
					Scale_1.Force_Activate();
				}break;
				case 1.0:{
					Scale_2.Force_Activate();
				}break;
				case 1.5:{
					Scale_3.Force_Activate();
				}break;
			}
			
			Active = true;
			Show();
			
		}
		public function Deactivate():void{
			Scale_1.Reset();
			Scale_2.Reset();
			Scale_3.Reset();
			Active = false;
			Hide();
		}
		public function checkActivated():Boolean{return Active;}
		
		
		//Inputs.
		public function Hover(MousePosition:Point):String{				
			
			if(Scale_1.Down(MousePosition)){
				Selected_Scale = 0.5;
				Scale_2.Reset();
				Scale_3.Reset();
				return "Scale_1";
			}
			if(Scale_2.Down(MousePosition)){
				Selected_Scale = 1.0;
				Scale_1.Reset();
				Scale_3.Reset();
				return "Scale_2";
			}
			if(Scale_3.Down(MousePosition)){
				Selected_Scale = 1.5;
				Scale_2.Reset();
				Scale_1.Reset();
				return "Scale_3";
			}
			
			return "Nothing";
		}
		
		//Animate.
		public function Show():void{
			TweenMax.killTweensOf(this);
			TweenMax.to(this, 0.2, {alpha:1});
		}
		public function Hide():void{
			TweenMax.killTweensOf(this);
			TweenMax.to(this, 0.2, {alpha:0});
		}
		
		//Return Values.
		public function getScale():Number{return Selected_Scale;}
		
	}
}