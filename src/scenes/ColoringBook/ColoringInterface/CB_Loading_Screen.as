package scenes.ColoringBook.ColoringInterface{
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.display.BitmapData;
	
	public class CB_Loading_Screen extends Sprite{
		
		private var Make_Text:CB_Text_Maker;		
		private var Loading_Image:Bitmap;
		private var Loading_BG:Bitmap;
		
		public function CB_Loading_Screen(){
			super();
			
			//Init text creator.
			Make_Text 				= new CB_Text_Maker();
			Loading_Image 			= Make_Text.Create_a_Word("Loading.", 0xfaa118, "Tastic", 70, 0xffffff);
			Loading_Image.smoothing = true
			Loading_Image.x 		= (((1366 * Costanza.SCALE_FACTOR) / 2) - (Loading_Image.width / 2));
			Loading_Image.y 		= (((768 * Costanza.SCALE_FACTOR) / 2) - (Loading_Image.height / 2));
			Loading_Image.visible   = true;
			
			Loading_BG = new Bitmap();
			Loading_BG.bitmapData = new BitmapData((1366 * Costanza.SCALE_FACTOR), (768 * Costanza.SCALE_FACTOR), false, 0x000000);
			
			this.addChild(Loading_BG);
			this.addChild(Loading_Image);
			
		}
		public function Destroy():void{
			
			Make_Text = null;
			
			this.removeChild(Loading_BG);
			this.removeChild(Loading_Image);
			
			Loading_Image.bitmapData.dispose();
			Loading_Image = null;
			
			Loading_BG.bitmapData.dispose();
			Loading_BG = null;
			
		}
		
	}
}