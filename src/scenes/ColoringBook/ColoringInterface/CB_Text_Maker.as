package scenes.ColoringBook.ColoringInterface
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.text.*;
	import flash.filters.GlowFilter;
	import flash.filters.BitmapFilterQuality;
	import flash.display.StageQuality;
	
	public class CB_Text_Maker{		
		
		//Glow.
		private var Text_Glow:GlowFilter;
		
		public function CB_Text_Maker(){
			super();		
		}		
		
		public  function Create_a_Word(The_Text:String, The_Color:uint, T_Font:String, TF_Size:int, Glow:uint):Bitmap{
			
			TF_Size		= (TF_Size * Costanza.SCALE_FACTOR);
			Glow 		= 0xfaa118;
			The_Color 	= 0xffffff;
			
			Set_Glows(Glow);
			
			The_Text = "  " + The_Text + " ";
			
			var Tfield:TextField    	= new TextField();
			Tfield.embedFonts 			= true;
			
			var Tformat:TextFormat  	= new TextFormat();
			Tformat.font 				= "BURBANK BIG REGULAR";
					
			//Foramtion of Text.				
			Tformat.size 				= TF_Size;
			Tformat.align 				= TextFormatAlign.LEFT;				
			
			Tfield.defaultTextFormat 	= Tformat;		
			Tfield.selectable 			= false;				
			Tfield.textColor  			= The_Color;	
			Tfield.filters 		    	= [Text_Glow];	
			
			Tfield.text 				= The_Text;	
			Tfield.autoSize 			= TextFieldAutoSize.LEFT;
						
			var Word_Image:Bitmap 		= new Bitmap();
			Word_Image.x 				= 0;
			Word_Image.y 				= 0;
			Word_Image.smoothing		= false;
			Word_Image.bitmapData 		= new BitmapData((Tfield.width + 6), (Tfield.height + 6), true, 0x00000000);			
			Word_Image.bitmapData.drawWithQuality(Tfield, null, null, null, null, true, StageQuality.BEST);
			
			return Word_Image;
			
		}
		private function Set_Glows(Color:uint):void{
			
			Text_Glow 			= null;			
			Text_Glow 			= new GlowFilter;			
			Text_Glow.color 	= Color;
			Text_Glow.alpha 	= 1;
			Text_Glow.blurX 	= 2;
			Text_Glow.blurY 	= 2;
			Text_Glow.strength  = 3;
			Text_Glow.quality   = BitmapFilterQuality.HIGH;
			
		}
		
		
	}
}