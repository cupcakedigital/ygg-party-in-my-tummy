package scenes.ColoringBook.ColoringInterface{
	
	import com.greensock.TweenMax;
	
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.geom.Point;
	
	import scenes.ColoringBook.External_PNG_Sheet_Manager;
	
	public class CB_HUD_BottomTab extends Sprite{		
		
		//Props.
		private var StateStatus:String;
		private var UpdateMode:String;
		private var Closed_Position:int;
		private var Opened_Position:int;	
		
		//Text
		private var Make_Text:CB_Text_Maker;
		//private var Home_Text_Image:Bitmap;
		//private var Page_Text_Image:Bitmap;
		
		//Buttons.
		private var BackPlate:InterfaceButton;			
		private var Home_Button:InterfaceButton;
		private var Page_Button:InterfaceButton;	
		//private var Tool_Selector:InterfaceButton;	
		private var Sticker_Button:InterfaceButton;
		private var Brush_Button:InterfaceButton;
		private var Bucket_Button:InterfaceButton;
		private var Crayon_Button:InterfaceButton;
		private var Spray_Button:InterfaceButton;
		private var Chalk_Button:InterfaceButton;
		private var Eraser_Button:InterfaceButton;
		
		//Scroll Bars.
		private var Color_Selector:CB_Color_Scroll;
		private var Sticker_Selector:CB_Sticker_Scroll;
		private var ScaleTool:CB_HUD_ScaleTab;
		
		public function CB_HUD_BottomTab(Core_Assets:External_PNG_Sheet_Manager, Color_List:Vector.<uint>){
			
			super();
			
			StateStatus 		= "opened";
			UpdateMode			= "none";
			Closed_Position		= (768 * Costanza.SCALE_FACTOR) - ( CONFIG::MARKET == "nabi" ? Costanza.NABI_OFFSET : 0 );
			Opened_Position		= (618 * Costanza.SCALE_FACTOR) - ( CONFIG::MARKET == "nabi" ? Costanza.NABI_OFFSET : 0 );
			Make_Text 			= new CB_Text_Maker();
			//Home_Text_Image 	= Make_Text.Create_a_Word("Home",  0xfaa118, "Tastic", 28, 0xffffff);
			//Page_Text_Image 	= Make_Text.Create_a_Word("Pages", 0xfaa118, "Tastic", 28, 0xffffff);
			
			//Tool_Selector		= new InterfaceButton(Core_Assets.Get_Image("BH_Tool_Selection"));
			BackPlate 			= new InterfaceButton(Core_Assets.Get_Image("BH_Plate"));
			Home_Button 		= new InterfaceButton(Core_Assets.Get_Image("BH_Home_BTN_Static"), 	Core_Assets.Get_Image("BH_Home_BTN_Glow"));
			Page_Button 		= new InterfaceButton(Core_Assets.Get_Image("BH_Page_BTN_Static"), 	Core_Assets.Get_Image("BH_Page_BTN_Glow"));			
			Sticker_Button		= new InterfaceButton(Core_Assets.Get_Image("BH_Sticker_BTN_Static"), 	Core_Assets.Get_Image("BH_Sticker_BTN_Glow"));		
			Brush_Button		= new InterfaceButton(Core_Assets.Get_Image("BH_Brush_BTN_Static"), 	Core_Assets.Get_Image("BH_Brush_BTN_Glow"));		
			Bucket_Button		= new InterfaceButton(Core_Assets.Get_Image("BH_Bucke_BTN_Static"), 	Core_Assets.Get_Image("BH_Bucke_BTN_Glow"));		
			Crayon_Button		= new InterfaceButton(Core_Assets.Get_Image("BH_Crayon_BTN_Static"), 	Core_Assets.Get_Image("BH_Crayon_BTN_Glow"));		
			Spray_Button		= new InterfaceButton(Core_Assets.Get_Image("BH_Spray_BTN_Static"), 	Core_Assets.Get_Image("BH_Spray_BTN_Glow"));		
			Chalk_Button		= new InterfaceButton(Core_Assets.Get_Image("BH_Chalk_BTN_Static"), 	Core_Assets.Get_Image("BH_Chalk_BTN_Glow"));		
			Eraser_Button		= new InterfaceButton(Core_Assets.Get_Image("BH_Eraser_BTN_Static"), 	Core_Assets.Get_Image("BH_Eraser_BTN_Glow"));	
			
			Color_Selector 		= new CB_Color_Scroll(Core_Assets, Color_List);
			Sticker_Selector	= new CB_Sticker_Scroll(Core_Assets);
			
			ScaleTool			= new CB_HUD_ScaleTab(Core_Assets);
			
			BackPlate.x 		= 0;
			BackPlate.y 		= (BackPlate.height / 2);
			
			Home_Button.x 		= (-450 * Costanza.SCALE_FACTOR);
			Home_Button.y 		= BackPlate.y;
			
			//Home_Text_Image.x   = (int(Home_Button.x - (Home_Text_Image.width / 2)) - (5 * Costanza.SCALE_FACTOR));
			//Home_Text_Image.y   = int(Home_Button.y + (35 * Costanza.SCALE_FACTOR));
			//Home_Text_Image.smoothing = true;
			
			Page_Button.x 		= (450 * Costanza.SCALE_FACTOR);
			Page_Button.y 		= BackPlate.y;
			
			//Page_Text_Image.x   = (int(Page_Button.x - (Page_Text_Image.width / 2)) + (1 * Costanza.SCALE_FACTOR));
			//Page_Text_Image.y   = int(Page_Button.y + (35 * Costanza.SCALE_FACTOR));
			//Page_Text_Image.smoothing = true;
			
			var itemYloc:int	= (165 * Costanza.SCALE_FACTOR);
			var itemXMul:int	= (110 * Costanza.SCALE_FACTOR);
			
			//Tool_Selector.y 	= ((150 * Costanza.SCALE_FACTOR) - (Tool_Selector.height / 2));
			
			Sticker_Button.x 	= ((-328 * Costanza.SCALE_FACTOR) + (0 * itemXMul));
			Sticker_Button.y 	= (itemYloc - (Sticker_Button.height / 2));			
			
			Brush_Button.x 		= ((-328 * Costanza.SCALE_FACTOR) + (1 * itemXMul));
			Brush_Button.y 		= (itemYloc - (Brush_Button.height / 2));	
			
			Crayon_Button.x 	= ((-328 * Costanza.SCALE_FACTOR) + (2 * itemXMul)) - 15;
			Crayon_Button.y 	= (itemYloc - (Crayon_Button.height / 2));
			
			Chalk_Button.x 		= ((-328 * Costanza.SCALE_FACTOR) + (3 * itemXMul)) - 24;
			Chalk_Button.y 		= (itemYloc - (Chalk_Button.height / 2));
			
			Spray_Button.x 		= ((-328 * Costanza.SCALE_FACTOR) + (4 * itemXMul)) - 30;
			Spray_Button.y 		= (itemYloc - (Spray_Button.height / 2));
			
			Bucket_Button.x 	= ((-328 * Costanza.SCALE_FACTOR) + (5 * itemXMul)) - 19;
			Bucket_Button.y 	= (itemYloc - (Bucket_Button.height / 2));						
			
			Eraser_Button.x 	= ((-328 * Costanza.SCALE_FACTOR) + (6 * itemXMul)) - 15;
			Eraser_Button.y 	= (itemYloc - (Eraser_Button.height / 2));	
			
			ScaleTool.x 			= 0;
			ScaleTool.y 			= (75 * Costanza.SCALE_FACTOR);
			
			Sticker_Selector.Deactivate_Bar();
			
			this.x = ((1366 * Costanza.SCALE_FACTOR) / 2);
			this.y = Opened_Position;
			
			this.addChild(BackPlate);
			this.addChild(Home_Button);
			this.addChild(Page_Button);
			
			//this.addChild(Tool_Selector);
			this.addChild(Sticker_Button);
			this.addChild(Brush_Button);
			this.addChild(Bucket_Button);
			this.addChild(Crayon_Button);
			this.addChild(Spray_Button);
			this.addChild(Chalk_Button);
			this.addChild(Eraser_Button);
			//this.addChild(Home_Text_Image);
			//this.addChild(Page_Text_Image);
			
			this.addChild(Color_Selector);	
			Set_Options_Glows("Brush");	
			Brush_Button.Force_Activate();
			
			if(ScaleTool.checkActivated()){	
				ScaleTool.Deactivate();
				this.removeChild(ScaleTool);
			}UpdateMode = "none";
			
		}
		public function Destroy():void{
			
			TweenMax.killTweensOf(this);
			
			//Items.
			this.removeChild(BackPlate);
			this.removeChild(Home_Button);
			this.removeChild(Page_Button);	
			//this.removeChild(Tool_Selector);
			this.removeChild(Sticker_Button);
			this.removeChild(Brush_Button);
			this.removeChild(Bucket_Button);
			this.removeChild(Crayon_Button);
			this.removeChild(Spray_Button);
			this.removeChild(Chalk_Button);
			this.removeChild(Eraser_Button);
			
			//Scaler
			if(ScaleTool.checkActivated()){				
				this.removeChild(ScaleTool);
			}ScaleTool.Destroy();			
			
			//Scroll bars.
			if(Sticker_Selector.Get_Active()){
				Sticker_Selector.Deactivate_Bar();
				this.removeChild(Sticker_Selector);
			}Sticker_Selector.Dispose_Bar();
			
			if(Color_Selector.Get_Active()){
				Color_Selector.Deactivate_Bar();
				this.removeChild(Color_Selector);
			}Color_Selector.Dispose_Bar();
			
			BackPlate.Destroy();
			Home_Button.Destroy();
			Page_Button.Destroy();
			//Tool_Selector.Destroy();
			Sticker_Button.Destroy();
			Brush_Button.Destroy();
			Bucket_Button.Destroy();
			Crayon_Button.Destroy();
			Spray_Button.Destroy();
			Chalk_Button.Destroy();
			Eraser_Button.Destroy();
			
			Make_Text = null;
			//Home_Text_Image.bitmapData.dispose();
			//Page_Text_Image.bitmapData.dispose();
			//Home_Text_Image = null;
			//Page_Text_Image = null;
			
		}		
		
		//Inputs.
		public function Down(MousePosition:Point):String{
			
			UpdateMode = "none";
			
			if(ScaleTool.checkActivated()){	
				var Sel:String = ScaleTool.Hover(MousePosition);
				ScaleTool.Deactivate();
				this.removeChild(ScaleTool);
				if(Sel != "Nothing"){return Sel;}
			}
			
			if(!BackPlate.Pixel_Colision(MousePosition)){return "Nothing";}
			
			if(Home_Button.Down(MousePosition)){					return "Home";}
			if(Page_Button.Down(MousePosition)){					return "Page";}
			if(Sticker_Button.Up(MousePosition)){		Set_Options_Glows("Sticker"); 	return "Sticker";}
			if(Brush_Button.Up(MousePosition)){			Set_Options_Glows("Brush"); 	return "Brush";}
			if(Bucket_Button.Up(MousePosition)){		Set_Options_Glows("Bucket"); 	return "Bucket";}
			if(Crayon_Button.Up(MousePosition)){		Set_Options_Glows("Crayon"); 	return "Crayon";}
			if(Spray_Button.Up(MousePosition)){			Set_Options_Glows("Spray"); 	return "Spray";}
			if(Chalk_Button.Up(MousePosition)){			Set_Options_Glows("Chalk"); 	return "Chalk";}
			if(Eraser_Button.Up(MousePosition)){		Set_Options_Glows("Eraser"); 	return "Eraser";}
					
			if(Color_Selector.Click_Bar(MousePosition) != -1){ 		UpdateMode = "Color"; return "Color_Scroll";}	
			if(Sticker_Selector.Click_Bar(MousePosition) != -1){ 	UpdateMode = "Sticker"; return "Sticker_Scroll";}
			
			return "Plate";
		}
		public function Up(MousePosition:Point):String{
			
			//Scaler
			//if(ScaleTool.checkActivated()){	
				//ScaleTool.Deactivate();
				//this.removeChild(ScaleTool);
			//}
			
			if(StateStatus == "closed"){return "Nothing";}
			
			UpdateMode = "none";
			
			Home_Button.Reset();
			Page_Button.Reset();
			
			if(!BackPlate.Pixel_Colision(MousePosition)){		    return "Nothing";}			
			if(Home_Button.Up(MousePosition)){						return "Home";}
			if(Page_Button.Up(MousePosition)){						return "Page";}
			if(Sticker_Button.Down(MousePosition)){					return "Sticker";}
			if(Brush_Button.Down(MousePosition)){					return "Brush";}
			if(Bucket_Button.Down(MousePosition)){					return "Bucket";}
			if(Crayon_Button.Down(MousePosition)){					return "Crayon";}
			if(Spray_Button.Down(MousePosition)){					return "Spray";}
			if(Chalk_Button.Down(MousePosition)){					return "Chalk";}
			if(Eraser_Button.Down(MousePosition)){					return "Eraser";}	
			if(Sticker_Selector.Up_Bar(MousePosition) != -1){ 		return "Sticker_Scroll";}
			
			return "Plate";
			
		}
		public function Move(MousePosition:Point, LastPosition:Point):String{			
			
			switch(UpdateMode){
				
				case "Sticker":{
					Sticker_Selector.Move_Bar((MousePosition.x - LastPosition.x), MousePosition);
					return "Scrolling_Stickers";
				}break;
				
				case "Color":{
					Color_Selector.Move_Bar((MousePosition.x - LastPosition.x));
					return "Scrolling_Colors";
				}break;
				
				case "Scale":{
					
					//if(!ScaleTool.checkActivated()){	
						//this.addChild(ScaleTool);
						//ScaleTool.Activate();
					//}
					
					//return ScaleTool.Hover(MousePosition);
					
				}break;
				
			}			
			
			return "Nothing";
			
		}		
		
		//Tools manipulations.
		private function Set_Options_Glows(Mode:String):void{
			
			Reset_Options_Glows();		
			
			if(Sticker_Selector.Get_Active()){
				Sticker_Selector.Deactivate_Bar();
				this.removeChild(Sticker_Selector);
			}
			
			if(Color_Selector.Get_Active()){
				Color_Selector.Deactivate_Bar();
				this.removeChild(Color_Selector);
			}			
			
			switch(Mode){
				case "Sticker":{
					//Tool_Selector.x = Sticker_Button.x;
					//Sticker_Button.Set_Glow();
					Sticker_Selector.Activate_Bar();
					this.addChild(Sticker_Selector);
				}break;
				case "Brush":{
					
					//Tool_Selector.x = Brush_Button.x;
					//Brush_Button.Set_Glow();
					Color_Selector.Activate_Bar(Color_Selector.Get_Selection());
					this.addChild(Color_Selector);
					
					//Show Scaler.					
					ScaleTool.x = Brush_Button.x;		
					this.addChild(ScaleTool);
					ScaleTool.Activate();
					UpdateMode = "Scale";
					
				}break;
				case "Bucket":{
					
					//Tool_Selector.x = Bucket_Button.x;
					//Bucket_Button.Set_Glow();
					Color_Selector.Activate_Bar(Color_Selector.Get_Selection());
					this.addChild(Color_Selector);
					
				}break;
				case "Crayon":{
					
					//Tool_Selector.x = Crayon_Button.x;
					//Crayon_Button.Set_Glow();
					Color_Selector.Activate_Bar(Color_Selector.Get_Selection());
					this.addChild(Color_Selector);
					
					//Show Scaler.					
					ScaleTool.x = Crayon_Button.x;	
					this.addChild(ScaleTool);
					ScaleTool.Activate();			
					UpdateMode = "Scale";
					
				}break;
				case "Spray":{
					
					//Tool_Selector.x = Spray_Button.x;
					//Spray_Button.Set_Glow();
					Color_Selector.Activate_Bar(Color_Selector.Get_Selection());
					this.addChild(Color_Selector);
					
					//Show Scaler.					
					ScaleTool.x = Spray_Button.x;
					this.addChild(ScaleTool);
					ScaleTool.Activate();
					UpdateMode = "Scale";
					
				}break;
				case "Chalk":{
					
					//Tool_Selector.x = Chalk_Button.x;
					//Chalk_Button.Set_Glow();
					Color_Selector.Activate_Bar(Color_Selector.Get_Selection());
					this.addChild(Color_Selector);
					
					//Show Scaler.					
					ScaleTool.x = Chalk_Button.x;
					this.addChild(ScaleTool);
					ScaleTool.Activate();
					UpdateMode = "Scale";
					
				}break;
				case "Eraser":{
					
					//Tool_Selector.x = Eraser_Button.x;
					//Eraser_Button.Set_Glow();					
					ScaleTool.x = Eraser_Button.x;		
					this.addChild(ScaleTool);
					ScaleTool.Activate();
					UpdateMode = "Scale";
				}break;
			}
			
			
			
		}
		private function Reset_Options_Glows():void{
			
			Sticker_Button.Reset();
			Brush_Button.Reset();
			Bucket_Button.Reset();
			Crayon_Button.Reset();
			Spray_Button.Reset();
			Chalk_Button.Reset();
			Eraser_Button.Reset();
			
		}			
		
		//Animate.
		public function Opperate_Mechanism():void{
			return;
			switch(StateStatus){
				
				case "opened":{	
					
					TweenMax.killTweensOf(this);			
					TweenMax.to(this, 0.5, {y:Closed_Position});
					
					StateStatus = "closed";
					
				}break;
				
				case "closed":{
					
					TweenMax.killTweensOf(this);			
					TweenMax.to(this, 0.5, {y:Opened_Position});
					
					StateStatus = "opened";
					
				}break;
				
			}
		}	
		
		public function Visible_Scrolls():void{
			ScaleTool.alpha 			= 1.0;
			Sticker_Selector.alpha 		= 1.0;
			Color_Selector.alpha 		= 1.0;
			ScaleTool.visible 			= true;	
			Sticker_Selector.visible 	= true;
			Color_Selector.visible 		= true;			
		}
		
		//Return Values.
		public function GetData(Mode:String):int{			
			
			switch(Mode){
				
				case "Sticker":{
					return Sticker_Selector.Get_Selection();
				}break;
				
				case "Color":{
					return Color_Selector.Get_Selection();
				}break;
				
				case "Scale":{
					return ScaleTool.getScale();
				}break;
				
			}			
			
			return -1;
			
		}
		
	}
}