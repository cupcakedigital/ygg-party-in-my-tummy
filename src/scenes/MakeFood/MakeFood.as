package scenes.MakeFood
{
	import com.cupcake.App;
	import com.greensock.TweenMax;
	
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.geom.Point;
	import flash.system.Capabilities;
	
	import dragonBones.animation.WorldClock;
	import dragonBones.factorys.StarlingFactory;
	
	import feathers.controls.Button;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.extensions.SoundManager;
	import starling.utils.AssetManager;

	public class MakeFood extends Sprite
	{
		public static const LOAD_LOBBY:String = "loadLobby";
		public static const SET_CHARACTER:String = "setCharacter";
		public static const LOAD_ROOM:String = "loadRoom";

		// Standard display
		private var stageWidth:int;
		private var stageHeight:int;

		private var soundManager:SoundManager;

		//character and food information
		private var characterString:String;
		private var foodItemString:String;

		// Dragon Bones
		private var factory:StarlingFactory;
		private var armatures:Array = [];
		private var armatureClips:Array = [];
		private var finalFoods:Array = [];

		// Scene Objects
		private var sceneRoot:Sprite;
		private var bg:Image;
		private var backButton:Button;
		private var nextButton:Button;
		private var assets:AssetManager;
		private var appDir:File = File.applicationDirectory;

		private var arm:Image;
		private var armFoodPosition:Quad;
		private var armSprite:Sprite;

		private var hitBox:Quad;
		
		private var currentFood:Sprite = new Sprite();

		private var phase:int = 0;
		private var phaseOffset:int = 0;
		//Main foods
		private var startPnt:Point = new Point(0,0);
		private var dragging:Boolean = false;
		private var tweening:Boolean = false;

		//Seasonings
		private var startPntSeasoning:Point = new Point(0,0);
		private var draggingSeasoning:Boolean = false;
		private var tweeningSeasoning:Boolean = false;

		//soy sauce specific
		private var canApplySauce:Boolean = false;
		private var soySauceArray:Array;

		private var overlaySeasoning:Image;
		
		private var assetsLoaded:Boolean = false;

		public function MakeFood(charString:String = "Brobee", foodItem:String = "Sandwich")
		{
			addEventListener(Root.DISPOSE,  disposeScene);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.ACTIVATE,screenActivated);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.DEACTIVATE,screenDeactivated);

			characterString = charString;
			foodItemString = foodItem;
			
			if(foodItemString == "Fruitsalad"){foodItemString = "FruitSalad";}
			if(foodItemString == "Noodles"){foodItemString = "Noodle";}

			// Create new asset manager and load content
			assets = new AssetManager(Costanza.SCALE_FACTOR);
			assets.useMipMaps = Costanza.mipmapsEnabled;
			assets.verbose = Capabilities.isDebugger;
			assets.enqueue(
				appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR + "x/FoodRoom/DJLance_Hand.png"),
				appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR + "x/FoodRoom/" + foodItemString + "/" + foodItemString + characterString + ".png"),
				appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR + "x/FoodRoom/" + foodItemString + "/db"),
				appDir.resolvePath("audio/BRoomSounds/" + characterString),
				appDir.resolvePath("audio/BRoomSounds/Global"),
				appDir.resolvePath("audio/Food_Names/" + foodItemString),
				appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Bursts/" + characterString + "_Burst.png")
			);

			assets.loadQueue(function onProgress(ratio:Number):void
			{
				if (ratio == 1){
					assetsLoaded = true;
					trace("finished loading");
					addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrameHandler);
					addObjects();
				};
			});
		}
		
		private function screenActivated(e:flash.events.Event):void
		{
			trace("----------------------------SCENE ACTIVATED-----------------------------");
			if(!assetsLoaded)
			{
				assets.enqueue(
					appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR + "x/FoodRoom/DJLance_Hand.png"),
					appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR + "x/FoodRoom/" + foodItemString + "/" + foodItemString + characterString + ".png"),
					appDir.resolvePath("textures/"+ Costanza.SCALE_FACTOR + "x/FoodRoom/" + foodItemString + "/db"),
					appDir.resolvePath("audio/BRoomSounds/" + characterString),
					appDir.resolvePath("audio/BRoomSounds/Global"),
					appDir.resolvePath("audio/Food_Names/" + foodItemString),
					appDir.resolvePath("textures/" + Costanza.SCALE_FACTOR + "x/Bursts/" + characterString + "_Burst.png")
				);
				assets.loadQueue(function onProgress(ratio:Number):void
				{
					if (ratio == 1){
						assetsLoaded = true;
						trace("finished loading character");
						addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrameHandler);
						addObjects();
					};
					
				});
			}
		}
		
		private function screenDeactivated(e:flash.events.Event):void
		{
			trace("----------------------------SCENE DEACTIVATED-----------------------------");
			if(!assetsLoaded)
			{
				assets.purgeQueue();
			}
		}

		private function addObjects():void
		{
			// Create main scene root

			sceneRoot = new Sprite();
			sceneRoot.x = Costanza.STAGE_OFFSET;

			addChild(sceneRoot);
			trace(foodItemString + characterString);
			var bg:Image = new Image(assets.getTexture(foodItemString + characterString));
			bg.x = Costanza.STAGE_OFFSET * -1;
			sceneRoot.addChild(bg);

			soundManager = SoundManager.getInstance();

			//sfx
			soundManager.addSound("Food_Pop", assets.getSound("Food_Pop"));
			soundManager.addSound("ChopSticks", assets.getSound("ChopSticks"));
			soundManager.addSound("Glass_Tap1", assets.getSound("Glass_Tap1"));
			soundManager.addSound("Glass_Tap2", assets.getSound("Glass_Tap2"));
			soundManager.addSound("Glass_Tap3", assets.getSound("Glass_Tap3"));
			soundManager.addSound("Parmesan", assets.getSound("Parmesan"));
			soundManager.addSound("Salt", assets.getSound("Salt"));
			soundManager.addSound("SoyaSauce", assets.getSound("SoyaSauce"));
			soundManager.addSound("Tomatoes", assets.getSound("Tomatoes"));
			
			soundManager.addSound("Music",assets.getSound("Music"));
			soundManager.playSound("Music",Costanza.musicVolume,999);
			
			soundManager.addSound("Burst", Root.assets.getSound("Burst"));

			if(characterString == "Brobee" || characterString == "Foffa")
			{
				soundManager.addSound("Stir", assets.getSound("Stir"));
			}
			else if(characterString == "Muno")
			{
				soundManager.addSound("Tap", assets.getSound("Tap"));
				soundManager.addSound("Salt", assets.getSound("Salt"));
			}
			else if(characterString == "Plex")
			{
				soundManager.addSound("Salt", assets.getSound("Salt"));
			}

			//From here we need to do custom loading based on who is being played
			switch(foodItemString)
			{
				case "":
				case "Sandwich":
					LoadSandwich();
					break;
				case "Yogurt":
					LoadYogurt();
					break;
				case "Noodle":
					LoadNoodles();
					break;
				case "Pizza":
					LoadPizza();
					break;
				case "Sushi":
					LoadSushi();
					break;
				case "FruitSalad":
					LoadFruitSalad();
					break;
				case "Salad":
					LoadSalad();
					break;
			}

			App.backPressed.add(BackPressed);
			// Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED,true);

			addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
		}

		//Load the Sandwich
		private function LoadSandwich():void
		{
			currentFood = new MakeSandwich(assets,soundManager,characterString);
			sceneRoot.addChild(currentFood);
		}

		//Load the Yogurt
		private function LoadYogurt():void
		{
			currentFood = new MakeYogurt(assets,soundManager,characterString);
			sceneRoot.addChild(currentFood);
		}

		//Load the Noodles
		private function LoadNoodles():void
		{
			currentFood = new MakeNoodles(assets,soundManager,characterString);
			sceneRoot.addChild(currentFood);
		}

		//Load the Pizza
		private function LoadPizza():void
		{
			currentFood = new MakePizza(assets,soundManager,characterString);
			sceneRoot.addChild(currentFood);
		}

		//Load the Sushi
		private function LoadSushi():void
		{
			currentFood = new MakeSushi(assets,soundManager,characterString);
			sceneRoot.addChild(currentFood);
		}

		private function LoadFruitSalad():void
		{
			currentFood = new MakeFruitSalad(assets,soundManager,characterString);
			sceneRoot.addChild(currentFood);
		}

		private function LoadSalad():void
		{
			currentFood = new MakeSalad(assets,soundManager,characterString);
			sceneRoot.addChild(currentFood);
		}

		private function onAddedToStage(evt:TimerEvent):void
		{
			// Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED,true);
		}

		private function onButtonTriggered():void
		{
			//dispatchEventWith(LOAD_LOBBY, true);
			dispatchEventWith(SET_CHARACTER, true, characterString);
			dispatchEventWith(LOAD_ROOM, true, 0);
		}

		private function onButtonTriggeredNext():void
		{
			dispatchEventWith(SET_CHARACTER, true, characterString);
			dispatchEventWith(LOAD_ROOM, true, 2);
		}
		
		private function BackPressed():void
		{
			dispatchEventWith("loadLobby", true);
		}

		private function onEnterFrameHandler(evt:EnterFrameEvent):void
		{
			WorldClock.clock.advanceTime(-1);
		}

		public function disposeScene():void
		{
			assets.removeTexture("texture" + foodItemString,true);
			App.backPressed.remove(BackPressed);
			TweenMax.killAll(true);
			var len:int = armatures.length;
			for (var i:int = 0; i < len; i++)
			{
				WorldClock.clock.remove(armatures[i]);
				armatures[i].dispose();
			}
			armatures.length = 0;
			sceneRoot.removeChildren(0,sceneRoot.numChildren-1,true);
			
			soundManager.stopAllSounds();
			
			assets.dispose();
		}
	}
}