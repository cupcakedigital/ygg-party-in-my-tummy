package scenes.MakeFood
{
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	
	import dragonBones.Armature;
	import dragonBones.animation.WorldClock;
	import dragonBones.events.FrameEvent;
	import dragonBones.factorys.StarlingFactory;
	import dragonBones.objects.ObjectDataParser;
	import dragonBones.objects.SkeletonData;
	import dragonBones.textures.StarlingTextureAtlas;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.PDParticleSystem;
	import starling.extensions.SoundManager;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.deg2rad;
	
	import utils.ParticleSystemJP.ParticleEmitterJP;
	
	public class MakePizza extends Sprite
	{
		public static const LOAD_ROOM:String = "loadRoom";
		
		private var phase:int = 0;
		private var phaseOffset:int = 0;

		//quad Hitbox
		private var hitBox:Quad;

		// Dragon Bones
		private var factory:StarlingFactory;
		private var armatures:Array = [];
		private var armatureClips:Array = [];
		private var finalFoods:Array = [];

		private var bTomatoArmature:Armature;
		private var bTomatoClip:Sprite;

		private var sTomatoArmature:Armature;
		private var sTomatoClip:Sprite;

		private var parmesanArmature:Armature;
		private var parmesanClip:Sprite;

		//DJ Arm
		private var arm:Image;
		private var armFoodPosition:Quad;
		private var armSprite:Sprite;

		private var startPnt:Point = new Point(0,0);
		private var dragging:Boolean = false;
		private var tweening:Boolean = false;

		//Seasonings
		private var parmStartPnt:Point = new Point(0,0);

		//overlay images
		private var overlayParmesan:Image;

		// Particle effects
		private var ParticlesA:PDParticleSystem;
		
		private var soundmanager:SoundManager;
		
		private var shakeTap:Boolean = false;
		
		private var particle:ParticleEmitterJP;
		
		private var particlesArray:Array = new Array();
		
		private var starBurst:Image;

		public function MakePizza(assets:AssetManager,sm:SoundManager,charStr)
		{
			soundmanager = sm;
			soundmanager.addSound("Food_0", assets.getSound("Food_0"));
			soundmanager.addSound("Food_1", assets.getSound("Food_1"));
			soundmanager.addSound("Food_2", assets.getSound("Food_2"));
			soundmanager.addSound("Food_3", assets.getSound("Food_3"));
			// DragonBones Setup
			factory = new StarlingFactory();

			var skeletonData:SkeletonData = ObjectDataParser.parseSkeletonData(assets.getObject("skeletonPizza"));
			factory.addSkeletonData(skeletonData);

			var texture:Texture = assets.getTexture("texturePizza");
			var textureAtlas:StarlingTextureAtlas = new StarlingTextureAtlas(texture, assets.getObject("texturePizza"),true);
			factory.addTextureAtlas(textureAtlas);

			armSprite = new Sprite();
			arm = new Image(assets.getTexture("DJLance_Hand"));
			armSprite.addChild(arm);

			armFoodPosition = new Quad(25,25,0xffffff);
			armFoodPosition.x = arm.width/4;
			armFoodPosition.y = arm.height/2;
			armFoodPosition.visible = false;

			armSprite.addChild(armFoodPosition);

			armSprite.pivotX = armSprite.width/2;
			armSprite.pivotY = armSprite.height/2;
			armSprite.x = Costanza.STAGE_WIDTH + armSprite.width/2 - Costanza.STAGE_OFFSET;
			armSprite.y = Costanza.STAGE_HEIGHT/2;

			addChild(armSprite);
			TweenMax.delayedCall(3, TweenArm,[true]);

			//hitbox for Pasta
			hitBox = new Quad(300,300);
			hitBox.x = 315;
			hitBox.y = 280;
			hitBox.alpha = 0;
			addChild(hitBox);

			sTomatoArmature = factory.buildArmature("Characters/FoodTomatoSmall");
			sTomatoArmature.animation.gotoAndPlay("idle");
			WorldClock.clock.add(sTomatoArmature);
			sTomatoArmature.display.x = 350;
			sTomatoArmature.display.y = 100;
			sTomatoClip = sTomatoArmature.display as Sprite;
			sTomatoClip.name = "0";
			addChild(sTomatoClip);
			sTomatoClip.addEventListener(TouchEvent.TOUCH, TapSmallTomato);

			bTomatoArmature = factory.buildArmature("Characters/FoodTomatoBig");
			bTomatoArmature.animation.gotoAndPlay("idle");
			WorldClock.clock.add(bTomatoArmature);
			bTomatoArmature.display.x = 180;
			bTomatoArmature.display.y = 150;
			bTomatoClip = bTomatoArmature.display as Sprite;
			bTomatoClip.name = "1";
			addChild(bTomatoClip);
			bTomatoClip.addEventListener(TouchEvent.TOUCH, TapBigTomato);

			//Parm
			parmesanArmature = factory.buildArmature("Characters/parmaseanShaker");
			parmesanArmature.animation.gotoAndPlay("idle");
			WorldClock.clock.add(parmesanArmature);
			parmesanArmature.display.x = 870;
			parmesanArmature.display.y = 150;
			parmesanClip = parmesanArmature.display as Sprite;
			parmesanClip.name = "2";
			addChild(parmesanClip);
			parmesanClip.addEventListener(TouchEvent.TOUCH, DragParmesan);
			parmesanArmature.addEventListener(FrameEvent.MOVEMENT_FRAME_EVENT, ParmesanEventHandler);
			parmStartPnt.setTo(870, 150);
			
			starBurst = new Image(assets.getTexture(charStr + "_Burst"));
			
			starBurst.pivotX = starBurst.width/2;
			starBurst.pivotY = starBurst.height/2;
			starBurst.x = Costanza.STAGE_WIDTH/2 - Costanza.STAGE_OFFSET;
			starBurst.y = Costanza.STAGE_HEIGHT/2;
			
			starBurst.scaleX = 0;
			starBurst.scaleY = 0;
			
			for(var i:int = 1; i <= 21; i++)
			{
				particlesArray.push(new Image(Root.assets.getTexture("ygg-confetti_000" + i)));
			}
			
			particle = new ParticleEmitterJP(particlesArray,false);
			
			particle.x = Costanza.STAGE_WIDTH/2 - Costanza.STAGE_OFFSET;
			particle.y = Costanza.STAGE_HEIGHT/2;

			//------------------------------
			//draggable food items
			//------------------------------
			//Sauce
			LoadInitialFoodItem("Characters/FoodSauce", "0");

			//Cheese
			LoadInitialFoodItem("Characters/FoodCheese", "1");

			//GreenPepper
			LoadInitialFoodItem("Characters/FoodGreenPepper", "2");

			//Mushroom
			LoadInitialFoodItem("Characters/FoodMushroom", "3");

			//---------------------------
			//final foods on the sandwich
			//---------------------------
			//Sauce
			LoadFinalFoodItem("Characters/PizzaSauce", 0);

			//Cheese
			LoadFinalFoodItem("Characters/PizzaCheese", 1);

			//GreenPepper
			LoadFinalFoodItem("Characters/PizzaGreenPepper", 2);

			//Mushroom
			LoadFinalFoodItem("Characters/PizzaMushrooms", 3);
			
			addChild(particle);
			addChild(starBurst);
			particle.touchable = starBurst.touchable = false;

			//Parmesean
			overlayParmesan = new Image(assets.getTexture("PARM_CHEESE"));
			overlayParmesan.x = (hitBox.x + hitBox.width/2) - overlayParmesan.width/2;
			overlayParmesan.y = (hitBox.y + hitBox.height/2) - overlayParmesan.height/2;
			overlayParmesan.alpha = 0;
			addChildAt(overlayParmesan, 6);

			armSprite.rotation = deg2rad(45);

			ParticlesA = new PDParticleSystem(assets.getXml("particle"), assets.getTexture("parmesan_particle"));
			Starling.juggler.add(ParticlesA);
			addChild(ParticlesA);
			
		}

		private function LoadInitialFoodItem(location:String, name:String):void
		{
			var tmpFood:Armature;
			var tmpFoodClip:Sprite;

			tmpFood = factory.buildArmature(location);
			WorldClock.clock.add(tmpFood);
			armatures.push(tmpFood);
			tmpFood.display.x = armSprite.x;
			tmpFood.display.y = armSprite.y - tmpFood.display.height/2;

			tmpFoodClip = tmpFood.display as Sprite;
			tmpFoodClip.y += tmpFoodClip.height/2;
			tmpFoodClip.rotation = deg2rad(45);
			tmpFoodClip.name = name;

			addChild(tmpFoodClip);
			tmpFood.addEventListener(FrameEvent.MOVEMENT_FRAME_EVENT, FrameEventHandler);

			armatureClips.push(tmpFoodClip);
		}

		private function LoadFinalFoodItem(location:String, value:int):void
		{
			var tmpFood:Armature;
			var tmpFoodClip:Sprite;
			tmpFood = factory.buildArmature(location);
			WorldClock.clock.add(tmpFood);

			tmpFood.display.x = hitBox.x + hitBox.width/2;
			tmpFood.display.y = hitBox.y + hitBox.height/2;
			tmpFoodClip = tmpFood.display as Sprite;
			addChildAt(tmpFoodClip, 2 + value);
			tmpFoodClip.visible = false;

			armatures.push(tmpFood);
			finalFoods.push(tmpFoodClip);
			phaseOffset++;
		}

		private function TapBigTomato(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.BEGAN);
			
			if(touches.length != 1){trace("too long" + touches.length);return;}
			
			var touch:Touch = touches[0];
			if(touch == null){return;}
			if(touch.phase == TouchPhase.BEGAN)
			{
				if(bTomatoArmature.animation.movementID == "idle")
				{
					soundmanager.playSound("Tomatoes",Costanza.soundVolume);
					bTomatoArmature.animation.gotoAndPlay("start");
				}
			}
		}

		private function TapSmallTomato(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.BEGAN);
			
			if(touches.length != 1){trace("too long" + touches.length);return;}
			
			var touch:Touch = touches[0];
			if(touch == null){return;}
			if(touch.phase == TouchPhase.BEGAN)
			{
				if(sTomatoArmature.animation.movementID == "idle")
				{
					soundmanager.playSound("Tomatoes",Costanza.soundVolume);
					sTomatoArmature.animation.gotoAndPlay("start");
				}
			}
		}

		private function TweenArm(start:Boolean=false):void
		{
			if(start)
			{
				TweenMax.to(armSprite, 1, {x:armSprite.x-600 - Costanza.STAGE_OFFSET, rotation:0});
				TweenMax.to(armatureClips[phase], 1, {x:armatureClips[phase].x-700 - Costanza.STAGE_OFFSET, rotation:0, onComplete:DoneArmTween});
			}
			else
			{
				TweenMax.to(armSprite, 1, {x:Costanza.STAGE_WIDTH + armSprite.width/2 - Costanza.STAGE_OFFSET, rotation:deg2rad(45), onComplete:TweenArm, onCompleteParams:[true]});
			}
		}

		private function RemoveArm():void
		{
			TweenMax.to(armSprite, 1, {x:armSprite.x+600, rotation:deg2rad(45)});
		}

		private function DoneArmTween():void
		{
			armatureClips[phase].addEventListener(TouchEvent.TOUCH, DragFood);
		}

		private function FrameEventHandler(evt:FrameEvent):void 
		{
			switch(evt.frameLabel)
			{
				case "done":
					armatures[phase + phaseOffset].animation.gotoAndPlay("start");
					TweenMax.delayedCall(.4,showFood,[phase]);
					soundmanager.playSound("Food_Pop",Costanza.soundVolume);

					function showFood(num:int):void
					{
						finalFoods[num].visible = true;
						armatureClips[num].visible = false;

						phase++;
						if(phase >= finalFoods.length)
						{
							GameDone();
							RemoveArm();
						}
						else
							TweenArm();
					}
					break;
			}
		}

		private function DragFood(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(stage);
			
			for each (var touch:Touch in touches)
			{
				//if(touch == null){return;}
				var spr:Sprite = e.currentTarget as Sprite;
				
				switch(touch.phase)
				{
					case TouchPhase.BEGAN:
					{
						if(!tweening)
						{
							startPnt.setTo(spr.x,spr.y);
							dragging = true;
							spr.x = touch.globalX - Costanza.STAGE_OFFSET;
							spr.y = touch.globalY;
						}
						break;
					}
					case TouchPhase.MOVED:
					{
						if(dragging)
						{
							spr.x = touch.globalX - Costanza.STAGE_OFFSET;
							spr.y = touch.globalY;
						}
						break;
					}
					case TouchPhase.ENDED:
					{
						if(dragging && spr.bounds.intersects(hitBox.bounds))
						{
							soundmanager.playSound("Food_" + spr.name,Costanza.soundVolume); 
							dragging = false;
							TweenMax.to(spr,.5,{x:hitBox.x+ hitBox.width/2, y:hitBox.y + hitBox.height/2});
							
							armatures[phase].animation.gotoAndPlay("yeah");
							spr.removeEventListener(TouchEvent.TOUCH, DragFood);
							
						}
						else if(dragging)
						{
							TweenMax.to(spr,.5,{x:startPnt.x, y:startPnt.y,onComplete:SetTweening});
							dragging = false;
							tweening = true;
						}
						break;
					}
				}
			}
		}

		private function ParmesanEventHandler(evt:FrameEvent):void
		{
			switch(evt.frameLabel)
			{
				case "doneShaking":
				{
					parmesanArmature.animation.gotoAndPlay("idle");
					TweenMax.to(parmesanClip, .5,{ x:parmStartPnt.x, y:parmStartPnt.y, onComplete:ParmesanFinished});
					break;
				}
			}
		}

		private function DragParmesan(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(stage);
			
			for each (var touch:Touch in touches)
			{
				if(touch == null){return;}
	
				switch(touch.phase)
				{
					case TouchPhase.BEGAN:
					{
						parmesanClip.x = touch.globalX - Costanza.STAGE_OFFSET;
						parmesanClip.y = touch.globalY;
						break;
					}
					case TouchPhase.MOVED:
					{
						parmesanClip.x = touch.globalX - Costanza.STAGE_OFFSET;
						parmesanClip.y = touch.globalY;
						break;
					}
					case TouchPhase.ENDED:
					{
						if(parmesanArmature.animation.movementID == "idle")
						{
							soundmanager.playSound("Parmesan",Costanza.soundVolume);
							parmesanClip.removeEventListener(TouchEvent.TOUCH, DragParmesan);
							parmesanArmature.animation.gotoAndPlay("shake");
	
							TweenMax.delayedCall(0.3,SprinkleParmesan,[parmesanClip.x-30, parmesanClip.y+70, 0.5]);
							if(parmesanClip.bounds.intersects(hitBox.bounds))
							{
								if(overlayParmesan != null)
								{
									TweenMax.to(overlayParmesan, 2, { alpha:1 });
								}
							}
							shakeTap = false;
						}
						break;
					}
				}
			}
		}

		private function SprinkleParmesan(x:int, y:int, length:Number):void
		{
			ParticlesA.emitterX = x;
			ParticlesA.emitterY = y;
			ParticlesA.start(length);
		}

		private function ParmesanFinished():void
		{
			parmesanClip.addEventListener(TouchEvent.TOUCH, DragParmesan);
		}

		private function GameDone():void
		{
			soundmanager.playSound("Burst",Costanza.soundVolume);
			particle.CreateBurst(70,500,500,5,true,true);
			TweenMax.delayedCall(.2,tweenBurst);
			TweenMax.delayedCall(5,nextPage);
		}
		
		private function tweenBurst():void
		{
			TweenMax.to(starBurst,1,{scaleX:10,scaleY:10, alpha:0});
		}
		
		private function nextPage():void
		{
			soundmanager.getSoundChannel("Music").stop();
			dispatchEventWith(LOAD_ROOM, true, 2);
		}

		private function SetTweening():void
		{
			tweening = false;
		}
		public function destroy():void
		{
			Starling.juggler.purge();
			this.removeChildren(0,this.numChildren-1,true);
			factory.dispose();
		}
	}
}