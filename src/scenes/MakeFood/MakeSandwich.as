package scenes.MakeFood
{
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	
	import dragonBones.Armature;
	import dragonBones.animation.WorldClock;
	import dragonBones.events.FrameEvent;
	import dragonBones.factorys.StarlingFactory;
	import dragonBones.objects.ObjectDataParser;
	import dragonBones.objects.SkeletonData;
	import dragonBones.textures.StarlingTextureAtlas;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.Color;
	import starling.utils.deg2rad;
	
	import utils.ParticleSystemJP.ParticleEmitterJP;
	
	public class MakeSandwich extends Sprite
	{
		public static const LOAD_ROOM:String = "loadRoom";
		
		private var phase:int = 0;
		private var phaseOffset:int = 0;

		//quad Hitbox
		private var hitBox:Quad;

		// Dragon Bones
		private var factory:StarlingFactory;
		private var armatures:Array = [];
		private var armatureClips:Array = [];
		private var finalFoods:Array = [];
		
		private var particle:ParticleEmitterJP;
		
		private var particlesArray:Array = new Array();
		
		private var starBurst:Image;

		private var mustardArmature:Armature;
		private var mustardClip:Sprite;

		private var faceArmature:Armature;
		private var faceClip:Sprite;

		//DJ Arm
		private var arm:Image;
		private var armFoodPosition:Quad;
		private var armSprite:Sprite;

		private var startPnt:Point = new Point(0,0);
		private var dragging:Boolean = false;
		private var tweening:Boolean = false;
		
		private var soundmanager:SoundManager;
		
		private var yogurtTap:int = 1;

		public function MakeSandwich(assets:AssetManager,sm:SoundManager,charStr)
		{
			soundmanager = sm;
			soundmanager.addSound("Food_0", assets.getSound("Food_0"));
			soundmanager.addSound("Food_1", assets.getSound("Food_1"));
			soundmanager.addSound("Food_2", assets.getSound("Food_2"));
			soundmanager.addSound("Food_3", assets.getSound("Food_3"));
			soundmanager.addSound("Food_4", assets.getSound("Food_4"));
			// DragonBones Setup
			factory = new StarlingFactory();

			var skeletonData:SkeletonData = ObjectDataParser.parseSkeletonData(assets.getObject("skeletonSandwich"));
			factory.addSkeletonData(skeletonData);

			var texture:Texture = assets.getTexture("textureSandwich");
			var textureAtlas:StarlingTextureAtlas = new StarlingTextureAtlas(texture, assets.getObject("textureSandwich"),true);
			factory.addTextureAtlas(textureAtlas);

			armSprite = new Sprite();
			arm = new Image(assets.getTexture("DJLance_Hand"));
			armSprite.addChild(arm);
			
			armFoodPosition = new Quad(25,25,0xffffff);
			armFoodPosition.x = arm.width/4;
			armFoodPosition.y = arm.height/2;
			armFoodPosition.visible = false;
			
			armSprite.addChild(armFoodPosition);
			
			armSprite.pivotX = armSprite.width/2;
			armSprite.pivotY = armSprite.height/2;
			armSprite.x = Costanza.STAGE_WIDTH + armSprite.width/2 - Costanza.STAGE_OFFSET;
			armSprite.y = Costanza.STAGE_HEIGHT/2;

			addChild(armSprite);

			//hitbox for sammich
			hitBox = new Quad(300,300);
			hitBox.x = 360;
			hitBox.y = 220;
			hitBox.color = Color.RED;
			hitBox.alpha = 0;
			addChild(hitBox);

			mustardArmature = factory.buildArmature("Characters/Mustard");
			mustardArmature.animation.gotoAndPlay("face1");
			WorldClock.clock.add(mustardArmature);
			mustardArmature.display.x = 980;
			mustardArmature.display.y = 150;
			mustardClip = mustardArmature.display as Sprite;
			mustardClip.name = "0";
			addChild(mustardClip);
			mustardClip.addEventListener(TouchEvent.TOUCH, TapMustard);
			
			starBurst = new Image(assets.getTexture(charStr + "_Burst"));
			
			starBurst.pivotX = starBurst.width/2;
			starBurst.pivotY = starBurst.height/2;
			starBurst.x = Costanza.STAGE_WIDTH/2 - Costanza.STAGE_OFFSET;
			starBurst.y = Costanza.STAGE_HEIGHT/2;
			
			starBurst.scaleX = 0;
			starBurst.scaleY = 0;
			
			for(var i:int = 1; i <= 21; i++)
			{
				trace("ygg-confetti_000" + i);
				particlesArray.push(new Image(Root.assets.getTexture("ygg-confetti_000" + i)));
			}
			
			particle = new ParticleEmitterJP(particlesArray,false);
			
			particle.x = Costanza.STAGE_WIDTH/2 - Costanza.STAGE_OFFSET;
			particle.y = Costanza.STAGE_HEIGHT/2;

			//------------------------------
			//draggable sandwich items
			//------------------------------
			//CHEESE
			LoadInitialFoodItem("Characters/FoodCheese", "0");

			//TURKEY
			LoadInitialFoodItem("Characters/TurkeySlices", "1");

			//LETTUCE
			LoadInitialFoodItem("Characters/Lettuce", "2");

			//TOMATO
			LoadInitialFoodItem("Characters/Tomato", "3");

			//BREAD
			LoadInitialFoodItem("Characters/Bread", "4");

			//---------------------------
			//final foods on the sandwich
			//---------------------------
			//CHEESE
			LoadFinalFoodItem("Characters/Toppings_Cheese_bl", 0);

			//TURKEY
			LoadFinalFoodItem("Characters/Toppings_Turkey_bl", 1);

			//LETTUCE
			LoadFinalFoodItem("Characters/Toppings_Lettuce_bl", 2);

			//TOMATO
			LoadFinalFoodItem("Characters/Topping_Tomatoes_bl", 3);

			//BREAD
			LoadFinalFoodItem("Characters/Toppings_bread_bl", 4);
			
			addChild(particle);
			addChild(starBurst);
			
			starBurst.touchable = false;
			particle.touchable = false;

			armSprite.rotation = deg2rad(45);

			//face for sammich
			faceArmature = factory.buildArmature("Characters/Sandwich_Face");
			WorldClock.clock.add(faceArmature);
			armatures.push(faceArmature);
			faceClip = faceArmature.display as Sprite;
			faceClip.x = hitBox.x + faceClip.width/2 + 20;
			faceClip.y = hitBox.y + faceClip.height + 20;
			faceClip.name = "4";
			addChild(faceClip);
			faceClip.visible = false;
			faceArmature.addEventListener(FrameEvent.MOVEMENT_FRAME_EVENT, FrameEventHandler);

			TweenMax.delayedCall(3, TweenArm,[true]);
		
		}

		private function LoadInitialFoodItem(location:String, name:String):void
		{
			var tmpFood:Armature;
			var tmpFoodClip:Sprite;

			tmpFood = factory.buildArmature(location);
			WorldClock.clock.add(tmpFood);
			armatures.push(tmpFood);
			tmpFood.display.x = armSprite.x;
			tmpFood.display.y = armSprite.y - tmpFood.display.height/2;

			tmpFoodClip = tmpFood.display as Sprite;
			tmpFoodClip.y += tmpFoodClip.height/2;
			tmpFoodClip.rotation = deg2rad(45);
			tmpFoodClip.name = name;

			addChild(tmpFoodClip);
			tmpFood.addEventListener(FrameEvent.MOVEMENT_FRAME_EVENT, FrameEventHandler);

			armatureClips.push(tmpFoodClip);
		}

		private function LoadFinalFoodItem(location:String, value:int):void
		{
			var tmpFood:Armature;
			var tmpFoodClip:Sprite;
			tmpFood = factory.buildArmature(location);
			WorldClock.clock.add(tmpFood);

			tmpFood.display.x = hitBox.x + hitBox.width/2;
			tmpFood.display.y = hitBox.y + hitBox.height/2;
			tmpFoodClip = tmpFood.display as Sprite;
			addChildAt(tmpFoodClip, 2 + value);
			tmpFoodClip.visible = false;

			armatures.push(tmpFood);
			finalFoods.push(tmpFoodClip);
			phaseOffset++;
		}

		private function TapMustard(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.BEGAN);
			
			if(touches.length != 1){trace("too long" + touches.length);return;}
			
			var touch:Touch = touches[0];
			var spr:Sprite = e.currentTarget as Sprite;
			if(touch == null){return;}
			if(touch.phase == TouchPhase.BEGAN)
			{
				if(!soundmanager.soundIsPlaying("Glass_Tap1") && !soundmanager.soundIsPlaying("Glass_Tap2") && !soundmanager.soundIsPlaying("Glass_Tap3"))
				{
					soundmanager.playSound("Glass_Tap" + yogurtTap,Costanza.soundVolume);
					yogurtTap++;
					if(yogurtTap > 3){yogurtTap = 1;}
					if(mustardArmature.animation.movementID == "face1")
					{
						mustardArmature.animation.gotoAndPlay("face2");
					}
					else if(mustardArmature.animation.movementID == "face2")
					{
						mustardArmature.animation.gotoAndPlay("face3");
					}
					else if(mustardArmature.animation.movementID == "face3")
					{
						mustardArmature.animation.gotoAndPlay("stir");
					}
					else if(mustardArmature.animation.movementID == "stir")
					{
						mustardArmature.animation.gotoAndPlay("face1");
					}
				}
			}
		}

		public function TweenArm(start:Boolean=false):void
		{
			if(start)
			{
				TweenMax.to(armSprite, 1, {x:armSprite.x-600 - Costanza.STAGE_OFFSET, rotation:0});
				TweenMax.to(armatureClips[phase], 1, {x:armatureClips[phase].x-700 - Costanza.STAGE_OFFSET, rotation:0, onComplete:DoneArmTween});
			}
			else
			{
				TweenMax.to(armSprite, 1, {x:Costanza.STAGE_WIDTH + armSprite.width/2 - Costanza.STAGE_OFFSET, rotation:deg2rad(45), onComplete:TweenArm, onCompleteParams:[true]});
			}
		}

		private function RemoveArm():void
		{
			TweenMax.to(armSprite, 1, {x:armSprite.x+600, rotation:deg2rad(45)});
		}

		private function DoneArmTween():void
		{
			armatureClips[phase].addEventListener(TouchEvent.TOUCH, DragFood);
		}

		private function FrameEventHandler(evt:FrameEvent):void 
		{
			switch(evt.frameLabel)
			{
				case "done":
					
					armatures[phase + phaseOffset].animation.gotoAndPlay("start");
					TweenMax.delayedCall(.4,showFood,[phase]);
					soundmanager.playSound("Food_Pop",Costanza.soundVolume);
					
					function showFood(num:int):void
					{
						finalFoods[num].visible = true;
						armatureClips[num].visible = false;
						
						phase++;
						if(phase >= finalFoods.length)
						{
							trace("this");
							GameDone();
							RemoveArm();
						}
						else
							TweenArm();
					}
					break;
			}
		}

		private function DragFood(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(stage);
			
			for each (var touch:Touch in touches)
			{
				if(touch == null){return;}
				var spr:Sprite = e.currentTarget as Sprite;
				
				switch(touch.phase)
				{
					case TouchPhase.BEGAN:
					{
						if(!tweening)
						{
							startPnt.setTo(spr.x,spr.y);
							dragging = true;
							spr.x = touch.globalX - Costanza.STAGE_OFFSET;
							spr.y = touch.globalY;
						}
						break;
					}
					case TouchPhase.MOVED:
					{
						if(dragging)
						{
							spr.x = touch.globalX - Costanza.STAGE_OFFSET;
							spr.y = touch.globalY;
						}
						break;
					}
					case TouchPhase.ENDED:
					{
						if(dragging && spr.bounds.intersects(hitBox.bounds))
						{
							dragging = false;
							TweenMax.to(spr,.5,{x:hitBox.x+ hitBox.width/2, y:hitBox.y + hitBox.height/2});
							soundmanager.playSound("Food_" + spr.name,Costanza.voiceVolume);
							armatures[phase].animation.gotoAndPlay("yeah");
							spr.removeEventListener(TouchEvent.TOUCH, DragFood);
							
						}
						else if(dragging)
						{
							TweenMax.to(spr,.5,{x:startPnt.x, y:startPnt.y,onComplete:SetTweening});
							dragging = false;
							tweening = true;
						}
						break;
					}
				}
			}
		}

		private function GameDone():void
		{
			faceClip.visible = true;
			faceArmature.animation.gotoAndPlay("awake");
			trace("CALLED");
			soundmanager.playSound("Burst",Costanza.soundVolume);
			particle.CreateBurst(70,500,500,5,true,true);
			TweenMax.delayedCall(.2,tweenBurst);
			TweenMax.delayedCall(5,nextPage);
		}
		
		private function tweenBurst():void
		{
			TweenMax.to(starBurst,1,{scaleX:10,scaleY:10, alpha:0});
		}
		
		private function nextPage():void
		{
			soundmanager.getSoundChannel("Music").stop();
			dispatchEventWith(LOAD_ROOM, true, 2);
		}

		private function SetTweening():void
		{
			tweening = false;
		}
		public function destroy():void
		{
			Starling.juggler.purge();
			this.removeChildren(0,this.numChildren-1,true);
			factory.dispose();
		}
	}
}