package scenes.MakeFood
{
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	
	import dragonBones.Armature;
	import dragonBones.animation.WorldClock;
	import dragonBones.events.FrameEvent;
	import dragonBones.factorys.StarlingFactory;
	import dragonBones.objects.ObjectDataParser;
	import dragonBones.objects.SkeletonData;
	import dragonBones.textures.StarlingTextureAtlas;
	
	import scenes.Characters.CharacterRoom;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.PDParticleSystem;
	import starling.extensions.SoundManager;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.deg2rad;
	
	import utils.ParticleSystemJP.ParticleEmitterJP;
	
	public class MakeNoodles extends Sprite
	{
		public static const LOAD_ROOM:String = "loadRoom";
		
		private var phase:int = 0;
		private var phaseOffset:int = 0;
		
		//quad Hitbox
		private var hitBox:Quad;
		
		// Dragon Bones
		private var factory:StarlingFactory;
		private var armatures:Array = [];
		private var armatureClips:Array = [];
		private var finalFoods:Array = [];
		
		private var chopStickArmature:Armature;
		private var chopStickClip:Sprite;
		
		private var saltArmature:Armature;
		private var saltClip:Sprite;
		private var pepperArmature:Armature;
		private var pepperClip:Sprite;

		//DJ Arm
		private var arm:Image;
		private var armFoodPosition:Quad;
		private var armSprite:Sprite;

		private var startPnt:Point = new Point(0,0);
		private var dragging:Boolean = false;
		private var tweening:Boolean = false;

		//Seasonings
		private var saltStartPnt:Point = new Point(0,0);
		private var pepperStartPnt:Point = new Point(0,0);

		// Particle effects
		private var ParticlesA:PDParticleSystem;
		private var ParticlesB:PDParticleSystem;
		
		private var soundmanager:SoundManager;
		
		private var shakeTap:Boolean = false;
		
		private var particle:ParticleEmitterJP;
		
		private var particlesArray:Array = new Array();
		
		private var starBurst:Image;

		public function MakeNoodles(assets:AssetManager,sm:SoundManager,charStr)
		{
			soundmanager = sm;
			soundmanager.addSound("Food_0", assets.getSound("Food_0"));
			soundmanager.addSound("Food_1", assets.getSound("Food_1"));
			soundmanager.addSound("Food_2", assets.getSound("Food_2"));
			soundmanager.addSound("Food_3", assets.getSound("Food_3"));
			// DragonBones Setup
			factory = new StarlingFactory();

			var skeletonData:SkeletonData = ObjectDataParser.parseSkeletonData(assets.getObject("skeletonNoodle"));
			factory.addSkeletonData(skeletonData);

			var texture:Texture = assets.getTexture("textureNoodle");
			var textureAtlas:StarlingTextureAtlas = new StarlingTextureAtlas(texture, assets.getObject("textureNoodle"),true);
			factory.addTextureAtlas(textureAtlas);

			armSprite = new Sprite();
			arm = new Image(assets.getTexture("DJLance_Hand"));
			armSprite.addChild(arm);

			armFoodPosition = new Quad(25,25,0xffffff);
			armFoodPosition.x = arm.width/4;
			armFoodPosition.y = arm.height/2;
			armFoodPosition.visible = false;

			armSprite.addChild(armFoodPosition);

			armSprite.pivotX = armSprite.width/2;
			armSprite.pivotY = armSprite.height/2;
			armSprite.x = Costanza.STAGE_WIDTH + armSprite.width/2 - Costanza.STAGE_OFFSET;
			armSprite.y = Costanza.STAGE_HEIGHT/2;

			//hitbox for Pasta
			hitBox = new Quad(300,300);
			hitBox.x = 390;
			hitBox.y = 305;
			hitBox.alpha = 0;
			addChild(hitBox);

			//CHOPSTICKS
			chopStickArmature = factory.buildArmature("Characters/ChopSticks");
			chopStickArmature.animation.gotoAndPlay("idle");
			WorldClock.clock.add(chopStickArmature);
			chopStickArmature.display.x = 930;
			chopStickArmature.display.y = 350;
			chopStickClip = chopStickArmature.display as Sprite;
			chopStickClip.rotation = deg2rad(40);
			chopStickClip.name = "0";
			addChildAt(chopStickClip, 1);
			chopStickClip.addEventListener(TouchEvent.TOUCH, TapSticks);

			//PEPPER
			pepperArmature = factory.buildArmature("Characters/Pepper");
			pepperArmature.animation.gotoAndPlay("idle");
			WorldClock.clock.add(pepperArmature);
			pepperArmature.display.x = 920;
			pepperArmature.display.y = 660;
			pepperClip = pepperArmature.display as Sprite;
			pepperClip.name = "1";
			addChildAt(pepperClip, 2);
			pepperClip.addEventListener(TouchEvent.TOUCH, DragPepper);
			pepperArmature.addEventListener(FrameEvent.MOVEMENT_FRAME_EVENT, PepperEventHandler);
			pepperStartPnt.setTo(920,660);

			//SALT
			saltArmature = factory.buildArmature("Characters/Salt");
			saltArmature.animation.gotoAndPlay("idle");
			WorldClock.clock.add(saltArmature);
			saltArmature.display.x = 1070;
			saltArmature.display.y = 465;
			saltClip = saltArmature.display as Sprite;
			saltClip.name = "2";
			addChildAt(saltClip, 2);
			saltClip.addEventListener(TouchEvent.TOUCH, DragSalt);
			saltArmature.addEventListener(FrameEvent.MOVEMENT_FRAME_EVENT, SaltEventHandler);
			saltStartPnt.setTo(1070, 465);

			addChild(armSprite);
			
			trace(CharacterRoom + "_Burst");
			starBurst = new Image(assets.getTexture(charStr + "_Burst"));
			
			starBurst.pivotX = starBurst.width/2;
			starBurst.pivotY = starBurst.height/2;
			starBurst.x = Costanza.STAGE_WIDTH/2 - Costanza.STAGE_OFFSET;
			starBurst.y = Costanza.STAGE_HEIGHT/2;
			
			starBurst.scaleX = 0;
			starBurst.scaleY = 0;
			
			for(var i:int = 1; i <= 21; i++)
			{
				trace("ygg-confetti_000" + i);
				particlesArray.push(new Image(Root.assets.getTexture("ygg-confetti_000" + i)));
			}
			
			particle = new ParticleEmitterJP(particlesArray,false);
			
			particle.x = Costanza.STAGE_WIDTH/2 - Costanza.STAGE_OFFSET;
			particle.y = Costanza.STAGE_HEIGHT/2;

			//------------------------------
			//draggable food items
			//------------------------------
			//Pasta
			LoadInitialFoodItem("Characters/Pasta", "0");

			//Chicken
			LoadInitialFoodItem("Characters/ChickenLeg", "1");

			//Cheese
			LoadInitialFoodItem("Characters/Cheese", "2");

			//Broccoli
			LoadInitialFoodItem("Characters/Broccoli", "3");

			//---------------------------
			//final foods on the sandwich
			//---------------------------
			//Pasta
			LoadFinalFoodItem("Characters/Noodles", 0);

			//Chicken
			LoadFinalFoodItem("Characters/ChickenChunks", 1);

			//Cheese
			LoadFinalFoodItem("Characters/CheeseShredded", 2);

			//Broccoli
			LoadFinalFoodItem("Characters/BroccoliPieces", 3);
			
			addChild(starBurst);
			addChild(particle);
			
			starBurst.touchable = false;
			particle.touchable = false;

			armSprite.rotation = deg2rad(45);

			ParticlesA = new PDParticleSystem(assets.getXml("particlePepper"), assets.getTexture("PEPPER"));
			Starling.juggler.add(ParticlesA);
			addChild(ParticlesA);
			ParticlesB = new PDParticleSystem(assets.getXml("particleSalt"), assets.getTexture("SALT"));
			Starling.juggler.add(ParticlesB);
			addChild(ParticlesB);

			TweenMax.delayedCall(3, TweenArm,[true]);
		}
		
		private function LoadInitialFoodItem(location:String, name:String):void
		{
			var tmpFood:Armature;
			var tmpFoodClip:Sprite;
			
			tmpFood = factory.buildArmature(location);
			WorldClock.clock.add(tmpFood);
			armatures.push(tmpFood);
			tmpFood.display.x = armSprite.x;
			tmpFood.display.y = armSprite.y - tmpFood.display.height/2;
			
			tmpFoodClip = tmpFood.display as Sprite;
			tmpFoodClip.y += tmpFoodClip.height/2;
			tmpFoodClip.rotation = deg2rad(45);
			tmpFoodClip.name = name;
			
			addChild(tmpFoodClip);
			tmpFood.addEventListener(FrameEvent.MOVEMENT_FRAME_EVENT, FrameEventHandler);
			
			armatureClips.push(tmpFoodClip);
		}
		
		private function LoadFinalFoodItem(location:String, value:int):void
		{
			var tmpFood:Armature;
			var tmpFoodClip:Sprite;
			tmpFood = factory.buildArmature(location);
			WorldClock.clock.add(tmpFood);
			
			tmpFood.display.x = hitBox.x + hitBox.width/2;
			tmpFood.display.y = hitBox.y + hitBox.height/2;
			tmpFoodClip = tmpFood.display as Sprite;
			addChildAt(tmpFoodClip, 2 + value);
			tmpFoodClip.visible = false;
			
			armatures.push(tmpFood);
			finalFoods.push(tmpFoodClip);
			phaseOffset++;
		}
		
		private function TapSticks(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.BEGAN);
			
			if(touches.length != 1){trace("too long" + touches.length);return;}
			
			var touch:Touch = touches[0];
			if(touch == null){return;}
			if(touch.phase == TouchPhase.BEGAN)
			{
				trace("here");
				if(chopStickArmature.animation.movementID == "idle")
				{
					chopStickArmature.animation.gotoAndPlay("tap");
					soundmanager.playSound("ChopSticks",Costanza.soundVolume);
				}
			}
		}
		
		private function TweenArm(start:Boolean=false):void
		{
			if(start)
			{
				TweenMax.to(armSprite, 1, {x:armSprite.x-600 - Costanza.STAGE_OFFSET, rotation:0});
				TweenMax.to(armatureClips[phase], 1, {x:armatureClips[phase].x-700 - Costanza.STAGE_OFFSET, rotation:0, onComplete:DoneArmTween});
			}
			else
			{
				TweenMax.to(armSprite, 1, {x:Costanza.STAGE_WIDTH + armSprite.width/2 - Costanza.STAGE_OFFSET, rotation:deg2rad(45), onComplete:TweenArm, onCompleteParams:[true]});
			}
		}
		
		private function RemoveArm():void
		{
			TweenMax.to(armSprite, 1, {x:armSprite.x+600, rotation:deg2rad(45)});
		}
		
		private function DoneArmTween():void
		{
			armatureClips[phase].addEventListener(TouchEvent.TOUCH, DragFood);
		}
		
		private function FrameEventHandler(evt:FrameEvent):void 
		{
			switch(evt.frameLabel)
			{
				case "done":
					
					armatures[phase + phaseOffset].animation.gotoAndPlay("start");
					TweenMax.delayedCall(.4,showFood,[phase]);
					soundmanager.playSound("Food_Pop",Costanza.soundVolume);
					
					function showFood(num:int):void
					{
						finalFoods[num].visible = true;
						armatureClips[num].visible = false;
						
						phase++;
						if(phase >= finalFoods.length)
						{
							trace("this");
							GameDone();
							RemoveArm();
						}
						else
							TweenArm();
					}
					break;
			}
		}
		
		private function DragFood(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(stage);
			
			for each (var touch:Touch in touches)
			{
				//if(touch == null){return;}
				var spr:Sprite = e.currentTarget as Sprite;
				
				switch(touch.phase)
				{
					case TouchPhase.BEGAN:
					{
						if(!tweening)
						{
							startPnt.setTo(spr.x,spr.y);
							dragging = true;
							spr.x = touch.globalX - Costanza.STAGE_OFFSET;
							spr.y = touch.globalY;
						}
						break;
					}
					case TouchPhase.MOVED:
					{
						if(dragging)
						{
							spr.x = touch.globalX - Costanza.STAGE_OFFSET;
							spr.y = touch.globalY;
						}
						break;
					}
					case TouchPhase.ENDED:
					{
						if(dragging && spr.bounds.intersects(hitBox.bounds))
						{
							dragging = false;
							TweenMax.to(spr,.5,{x:hitBox.x+ hitBox.width/2, y:hitBox.y + hitBox.height/2});
							
							soundmanager.playSound("Food_" + spr.name,Costanza.voiceVolume);
							armatures[phase].animation.gotoAndPlay("yeah");
							spr.removeEventListener(TouchEvent.TOUCH, DragFood);
							
						}
						else if(dragging)
						{
							TweenMax.to(spr,.5,{x:startPnt.x, y:startPnt.y,onComplete:SetTweening});
							dragging = false;
							tweening = true;
						}
						break;
					}
				}
			}
		}

		private function SaltEventHandler(evt:FrameEvent):void
		{
			switch(evt.frameLabel)
			{
				case "doneShaking":
				{
					saltArmature.animation.gotoAndPlay("idle");
					TweenMax.to(saltClip, .5,{ x:saltStartPnt.x, y:saltStartPnt.y, onComplete:saltFinished});
					break;
				}
			}
		}

		private function PepperEventHandler(evt:FrameEvent):void
		{
			switch(evt.frameLabel)
			{
				case "doneShaking":
				{
					pepperArmature.animation.gotoAndPlay("idle");
					TweenMax.to(pepperClip, .5,{ x:pepperStartPnt.x, y:pepperStartPnt.y, onComplete:pepperFinished});
					break;
				}
			}
		}

		private function DragSalt(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(stage);
			
			for each (var touch:Touch in touches)
			{
				//if(touch == null){return;}
	
				switch(touch.phase)
				{
					case TouchPhase.BEGAN:
					{
						saltClip.x = touch.globalX - Costanza.STAGE_OFFSET;
						saltClip.y = touch.globalY;
						break;
					}
					case TouchPhase.MOVED:
					{
						saltClip.x = touch.globalX - Costanza.STAGE_OFFSET;
						saltClip.y = touch.globalY;
						break;
					}
					case TouchPhase.ENDED:
					{
						if(saltArmature.animation.movementID == "idle")
						{
							soundmanager.playSound("Salt",Costanza.soundVolume);
							saltClip.removeEventListener(TouchEvent.TOUCH, DragSalt);
							saltArmature.animation.gotoAndPlay("shake");
	
							TweenMax.delayedCall(0.3,SprinkleSalt,[saltClip.x-30, saltClip.y+70, 1]);
						}
						break;
					}
				}
			}
		}
		
		private function DragPepper(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(stage);
			
			for each (var touch:Touch in touches)
			{
				//if(touch == null){return;}
				
				switch(touch.phase)
				{
					case TouchPhase.MOVED:
					{
						shakeTap = true;
						pepperClip.x = touch.globalX - Costanza.STAGE_OFFSET;
						pepperClip.y = touch.globalY;
						break;
					}
					case TouchPhase.ENDED:
					{
						if(pepperArmature.animation.movementID == "idle" && shakeTap)
						{
							soundmanager.playSound("Salt",Costanza.soundVolume);
							pepperClip.removeEventListener(TouchEvent.TOUCH, DragSalt);
							pepperArmature.animation.gotoAndPlay("shake");
	
							TweenMax.delayedCall(0.3,DashPepper,[pepperClip.x-30, pepperClip.y+70, 1]);
							shakeTap = false;
						}
						break;
					}
				}
			}
		}

		private function SprinkleSalt(x:int, y:int, length:Number):void
		{
			ParticlesB.emitterX = x;
			ParticlesB.emitterY = y;
			ParticlesB.start(length);
			//soundManager.playSound("Salt");
		}

		private function DashPepper(x:int, y:int, length:Number):void
		{
			ParticlesA.emitterX = x;
			ParticlesA.emitterY = y;
			ParticlesA.start(length);
			//soundManager.playSound("Salt");
		}

		private function saltFinished():void
		{
			saltClip.addEventListener(TouchEvent.TOUCH, DragSalt);
		}

		private function pepperFinished():void
		{
			pepperClip.addEventListener(TouchEvent.TOUCH, DragPepper);
		}

		private function GameDone():void
		{
			trace("CALLED");
			soundmanager.playSound("Burst",Costanza.soundVolume);
			particle.CreateBurst(70,500,500,5,true,true);
			TweenMax.delayedCall(.2,tweenBurst);
			TweenMax.delayedCall(5,nextPage);
		}
		
		private function tweenBurst():void
		{
			TweenMax.to(starBurst,1,{scaleX:10,scaleY:10, alpha:0});
		}
		
		private function nextPage():void
		{
			soundmanager.getSoundChannel("Music").stop();
			dispatchEventWith(LOAD_ROOM, true, 2);
		}

		private function SetTweening():void
		{
			tweening = false;
		}
		
		public function destroy():void
		{
			Starling.juggler.purge();
			this.removeChildren(0,this.numChildren-1,true);
			factory.dispose();
		}
	}
}