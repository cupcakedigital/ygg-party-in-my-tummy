package scenes.MakeFood
{
	import com.greensock.TweenMax;
	
	import flash.geom.Point;
	
	import dragonBones.Armature;
	import dragonBones.animation.WorldClock;
	import dragonBones.events.FrameEvent;
	import dragonBones.factorys.StarlingFactory;
	import dragonBones.objects.ObjectDataParser;
	import dragonBones.objects.SkeletonData;
	import dragonBones.textures.StarlingTextureAtlas;
	
	import scenes.Characters.CharacterRoom;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.PDParticleSystem;
	import starling.extensions.SoundManager;
	import starling.textures.Texture;
	import starling.utils.AssetManager;
	import starling.utils.Color;
	import starling.utils.deg2rad;
	
	import utils.ParticleSystemJP.ParticleEmitterJP;
	
	public class MakeSushi extends Sprite
	{
		public static const LOAD_ROOM:String = "loadRoom";
		
		private var phase:int = 0;
		private var phaseOffset:int = 0;

		//quad Hitbox
		private var hitBox:Quad;

		// Dragon Bones
		private var factory:StarlingFactory;
		private var armatures:Array = [];
		private var armatureClips:Array = [];
		private var finalFoods:Array = [];

		private var soysauceArmature:Armature;
		private var soysauceClip:Sprite;
		
		private var particle:ParticleEmitterJP;
		
		private var particlesArray:Array = new Array();
		
		private var starBurst:Image;

		//DJ Arm
		private var arm:Image;
		private var armFoodPosition:Quad;
		private var armSprite:Sprite;

		private var startPnt:Point = new Point(0,0);
		private var dragging:Boolean = false;
		private var tweening:Boolean = false;

		//Seasonings
		private var soyStartPnt:Point = new Point(0,0);
		
		//soy sauce specific
		private var canApplySauce:Boolean = false;
		private var soySauceArray:Array;

		//overlay images
		private var overlaySoysauce:Image;

		// Particle effects
		private var ParticlesA:PDParticleSystem;
		
		private var soundmanager:SoundManager;
		
		private var shakeDrag:Boolean = false;

		public function MakeSushi(assets:AssetManager,sm:SoundManager,charStr)
		{
			soundmanager = sm;
			soundmanager.addSound("Food_0", assets.getSound("Food_0"));
			soundmanager.addSound("Food_1", assets.getSound("Food_1"));
			soundmanager.addSound("Food_2", assets.getSound("Food_2"));
			soundmanager.addSound("Food_3", assets.getSound("Food_3"));
			// DragonBones Setup
			factory = new StarlingFactory();

			var skeletonData:SkeletonData = ObjectDataParser.parseSkeletonData(assets.getObject("skeletonSushi"));
			factory.addSkeletonData(skeletonData);

			var texture:Texture = assets.getTexture("textureSushi");
			var textureAtlas:StarlingTextureAtlas = new StarlingTextureAtlas(texture, assets.getObject("textureSushi"),true);
			factory.addTextureAtlas(textureAtlas);

			armSprite = new Sprite();
			arm = new Image(assets.getTexture("DJLance_Hand"));
			armSprite.addChild(arm);

			armFoodPosition = new Quad(25,25,0xffffff);
			armFoodPosition.x = arm.width/4;
			armFoodPosition.y = arm.height/2;
			armFoodPosition.visible = false;

			armSprite.addChild(armFoodPosition);

			armSprite.pivotX = armSprite.width/2;
			armSprite.pivotY = armSprite.height/2;
			armSprite.x = Costanza.STAGE_WIDTH + armSprite.width/2 - Costanza.STAGE_OFFSET;
			armSprite.y = Costanza.STAGE_HEIGHT/2;

			addChild(armSprite);

			var tmpFood:Sprite;
			//hitbox for Pasta
			hitBox = new Quad(300,300);
			hitBox.x = 320;
			hitBox.y = 190;
			hitBox.color = Color.RED;
			hitBox.alpha = 0;
			addChild(hitBox);

			//soy sauce
			soysauceArmature = factory.buildArmature("Characters/SoySauce");
			soysauceArmature.animation.gotoAndPlay("idle");
			WorldClock.clock.add(soysauceArmature);
			soysauceArmature.display.x = 680;
			soysauceArmature.display.y = 580;
			soysauceClip = soysauceArmature.display as Sprite;
			soysauceClip.name = "0";
			addChild(soysauceClip);
			soysauceClip.addEventListener(TouchEvent.TOUCH, DragSoysauce);
			soysauceArmature.addEventListener(FrameEvent.MOVEMENT_FRAME_EVENT, SoysauceEventHandler);
			soyStartPnt.setTo(680, 580);
			
			starBurst = new Image(assets.getTexture(charStr + "_Burst"));
			
			starBurst.pivotX = starBurst.width/2;
			starBurst.pivotY = starBurst.height/2;
			starBurst.x = Costanza.STAGE_WIDTH/2 - Costanza.STAGE_OFFSET;
			starBurst.y = Costanza.STAGE_HEIGHT/2;
			
			starBurst.scaleX = 0;
			starBurst.scaleY = 0;
			
			for(var i:int = 1; i <= 21; i++)
			{
				trace("ygg-confetti_000" + i);
				particlesArray.push(new Image(Root.assets.getTexture("ygg-confetti_000" + i)));
			}
			
			particle = new ParticleEmitterJP(particlesArray,false);
			
			particle.x = Costanza.STAGE_WIDTH/2 - Costanza.STAGE_OFFSET;
			particle.y = Costanza.STAGE_HEIGHT/2;

			//------------------------------
			//draggable sandwich items
			//------------------------------
			//Seaweed
			LoadInitialFoodItem("Characters/Seaweed", "0");

			//Rice
			LoadInitialFoodItem("Characters/Rice", "1");

			//Avocado
			LoadInitialFoodItem("Characters/Avacodo", "2");

			//Fish
			LoadInitialFoodItem("Characters/Fish", "3");

			//---------------------------
			//final foods on the sandwich
			//---------------------------
			//Seaweed
			LoadFinalFoodItem("Characters/SeaweedPieces", 0);

			//Rice
			LoadFinalFoodItem("Characters/RicePieces", 1);
			tmpFood = finalFoods[1];
			tmpFood.x -= 5;
			tmpFood.y -= 40;

			//Avocado
			LoadFinalFoodItem("Characters/AvacodoPieces", 2);
			tmpFood = finalFoods[2];
			tmpFood.x -= 225;
			tmpFood.y -= 225;

			//Fish
			LoadFinalFoodItem("Characters/FishPieces", 3);
			tmpFood = finalFoods[3];
			tmpFood.x += 80;
			tmpFood.y += 25;
			
			addChild(particle);
			addChild(starBurst);
			
			starBurst.touchable = false;
			particle.touchable = false;

			armSprite.rotation = deg2rad(45);

			//load all 6 of the soysauces into an array
			//this is a gross solution
			soySauceArray = new Array();
			overlaySoysauce = new Image(assets.getTexture("SOYSPLASH"));
			//top left
			LoadSoya(new Image(overlaySoysauce.texture), 450, 100);
			//top right
			LoadSoya(new Image(overlaySoysauce.texture), 610, 170);
			//mid left
			LoadSoya(new Image(overlaySoysauce.texture), 350, 220);
			//mid right
			LoadSoya(new Image(overlaySoysauce.texture), 500, 300);
			//bottom left
			LoadSoya(new Image(overlaySoysauce.texture), 230, 350);
			//bottom right
			LoadSoya(new Image(overlaySoysauce.texture), 395, 430);

			ParticlesA = new PDParticleSystem(assets.getXml("particle"), assets.getTexture("soy_particle"));
			Starling.juggler.add(ParticlesA);
			addChild(ParticlesA);

			TweenMax.delayedCall(3, TweenArm,[true]);
		}
		
		private function LoadInitialFoodItem(location:String, name:String):void
		{
			var tmpFood:Armature;
			var tmpFoodClip:Sprite;

			tmpFood = factory.buildArmature(location);
			WorldClock.clock.add(tmpFood);
			armatures.push(tmpFood);
			tmpFood.display.x = armSprite.x;
			tmpFood.display.y = armSprite.y - tmpFood.display.height/2;

			tmpFoodClip = tmpFood.display as Sprite;
			tmpFoodClip.y += tmpFoodClip.height/2;
			tmpFoodClip.rotation = deg2rad(45);
			tmpFoodClip.name = name;

			addChild(tmpFoodClip);
			tmpFood.addEventListener(FrameEvent.MOVEMENT_FRAME_EVENT, FrameEventHandler);

			armatureClips.push(tmpFoodClip);
		}
		
		private function LoadFinalFoodItem(location:String, value:int):void
		{
			var tmpFood:Armature;
			var tmpFoodClip:Sprite;
			tmpFood = factory.buildArmature(location);
			WorldClock.clock.add(tmpFood);

			tmpFood.display.x = hitBox.x + hitBox.width/2;
			tmpFood.display.y = hitBox.y + hitBox.height/2;
			tmpFoodClip = tmpFood.display as Sprite;
			addChildAt(tmpFoodClip, 2 + value);
			tmpFoodClip.visible = false;

			armatures.push(tmpFood);
			finalFoods.push(tmpFoodClip);
			phaseOffset++;
		}

		private function LoadSoya(image:Image, x:int, y:int):void
		{
			image.x = x;
			image.y = y;
			image.alpha = 0;
			addChildAt(image, 6);
			soySauceArray.push(image);
		}

		private function TweenArm(start:Boolean=false):void
		{
			if(start)
			{
				TweenMax.to(armSprite, 1, {x:armSprite.x-600 - Costanza.STAGE_OFFSET, rotation:0});
				TweenMax.to(armatureClips[phase], 1, {x:armatureClips[phase].x-700 - Costanza.STAGE_OFFSET, rotation:0, onComplete:DoneArmTween});
			}
			else
			{
				TweenMax.to(armSprite, 1, {x:Costanza.STAGE_WIDTH + armSprite.width/2 - Costanza.STAGE_OFFSET, rotation:deg2rad(45), onComplete:TweenArm, onCompleteParams:[true]});
			}
		}
		
		private function RemoveArm():void
		{
			TweenMax.to(armSprite, 1, {x:armSprite.x+600, rotation:deg2rad(45)});
		}
		
		private function DoneArmTween():void
		{
			armatureClips[phase].addEventListener(TouchEvent.TOUCH, DragFood);
		}
		
		private function FrameEventHandler(evt:FrameEvent):void 
		{
			switch(evt.frameLabel)
			{
				case "done":
					armatures[phase + phaseOffset].animation.gotoAndPlay("start");
					TweenMax.delayedCall(.4,showFood,[phase]);
					soundmanager.playSound("Food_Pop",Costanza.soundVolume);

					function showFood(num:int):void
					{
						finalFoods[num].visible = true;
						armatureClips[num].visible = false;

						phase++;
						if(phase >= finalFoods.length)
						{
							trace("this");
							GameDone();
							RemoveArm();
						}
						else
							TweenArm();
					}
					break;
			}
		}

		private function DragFood(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(stage);
			
			for each (var touch:Touch in touches)
			{
				if(touch == null){return;}
				var spr:Sprite = e.currentTarget as Sprite;
				
				switch(touch.phase)
				{
					case TouchPhase.BEGAN:
					{
						if(!tweening)
						{
							startPnt.setTo(spr.x,spr.y);
							dragging = true;
							spr.x = touch.globalX - Costanza.STAGE_OFFSET;
							spr.y = touch.globalY;
						}
						break;
					}
					case TouchPhase.MOVED:
					{
						if(dragging)
						{
							spr.x = touch.globalX - Costanza.STAGE_OFFSET;
							spr.y = touch.globalY;
						}
						break;
					}
					case TouchPhase.ENDED:
					{
						if(dragging && spr.bounds.intersects(hitBox.bounds))
						{
							dragging = false;
							TweenMax.to(spr,.5,{x:hitBox.x+ hitBox.width/2, y:hitBox.y + hitBox.height/2});
							soundmanager.playSound("Food_" + spr.name,Costanza.voiceVolume);
							armatures[phase].animation.gotoAndPlay("yeah");
							spr.removeEventListener(TouchEvent.TOUCH, DragFood);
							
						}
						else if(dragging)
						{
							TweenMax.to(spr,.5,{x:startPnt.x, y:startPnt.y,onComplete:SetTweening});
							dragging = false;
							tweening = true;
						}
						break;
					}
				}
			}
		}

		private function SoysauceEventHandler(evt:FrameEvent):void
		{
			switch(evt.frameLabel)
			{
				case "doneShaking":
				{
					soysauceArmature.animation.gotoAndPlay("idle");
					TweenMax.to(soysauceClip, .5,{ x:soyStartPnt.x, y:soyStartPnt.y, onComplete:SoysauceFinished});

					if(soysauceClip.bounds.intersects(hitBox.bounds))
					{
						//special case for Toodee's soy sauce, since it's 6 instances of the soy sauce
						//we only want it to apply once the food is complete, or it'll look stupid
						if(canApplySauce)
						{
							for(var i:int = 0; i < soySauceArray.length; i++)
							{
								TweenMax.to(soySauceArray[i], 2, { alpha:1 });
							}
						}
					}
					break;
				}
			}
		}

		private function DragSoysauce(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(stage);

			for each (var touch:Touch in touches)
			{
				if(touch == null){return;}
				
				switch(touch.phase)
				{
					case TouchPhase.MOVED:
					{
						shakeDrag = true;
						soysauceClip.x = touch.globalX - Costanza.STAGE_OFFSET;
						soysauceClip.y = touch.globalY;
						break;
					}
					case TouchPhase.ENDED:
					{
						if(soysauceArmature.animation.movementID == "idle" && shakeDrag)
						{
							soundmanager.playSound("SoyaSauce",Costanza.soundVolume);
							soysauceClip.removeEventListener(TouchEvent.TOUCH, DragSoysauce);
							soysauceArmature.animation.gotoAndPlay("shake");
	
							TweenMax.delayedCall(0.3,SprinkleSoysauce,[soysauceClip.x-155, soysauceClip.y-10, 0.5]);
							if(soysauceClip.bounds.intersects(hitBox.bounds))
							{
								if(overlaySoysauce != null)
								{
									TweenMax.to(overlaySoysauce, 2, { alpha:1 });
								}
							}
							shakeDrag = false;
						}
						break;
					}
				}
			}
		}

		private function SprinkleSoysauce(x:int, y:int, length:Number):void
		{
			ParticlesA.emitterX = x;
			ParticlesA.emitterY = y;
			ParticlesA.start(length);
		}

		private function SoysauceFinished():void
		{
			soysauceClip.addEventListener(TouchEvent.TOUCH, DragSoysauce);
		}
		
		private function GameDone():void
		{
			trace("CALLED");
			canApplySauce = true;
			soundmanager.playSound("Burst",Costanza.soundVolume);
			particle.CreateBurst(70,500,500,5,true,true);
			TweenMax.delayedCall(.2,tweenBurst);
			TweenMax.delayedCall(5,nextPage);
		}
		
		private function tweenBurst():void
		{
			TweenMax.to(starBurst,1,{scaleX:10,scaleY:10, alpha:0});
		}
		
		private function nextPage():void
		{
			soundmanager.getSoundChannel("Music").stop();
			dispatchEventWith(LOAD_ROOM, true, 2);
		}
		
		private function SetTweening():void
		{
			tweening = false;
		}
		public function destroy():void
		{
			Starling.juggler.purge();
			this.removeChildren(0,this.numChildren-1,true);
			factory.dispose();
		}
	}
}