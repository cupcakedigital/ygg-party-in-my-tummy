package scenes{
	import com.cupcake.Utils;
	import com.greensock.TweenMax;
	
	import flash.desktop.NativeApplication;
	import flash.desktop.SystemIdleMode;
	import flash.events.Event;
	import flash.events.MediaEvent;
	import flash.filesystem.File;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.extensions.SoundManager;
	import starling.utils.AssetManager;
	
	import utils.AIRMobileVideo;

	public class Splash_Video extends starling.display.Sprite{
		
		
		
		// Assets
		private var assets:AssetManager;
		private var soundManager:SoundManager;
		private var appDir:File = File.applicationDirectory;

		private var sceneRoot:starling.display.Sprite;
		private var stageVideoPlayer:AIRMobileVideo;
		private var playing:Boolean = true;	
		
		private var playIndex:int = 0;
		private var videoFiles:Vector.<String>;
		private var videoWidths:Vector.<String>;

		public function Splash_Video(dummy:String="")
		{
			videoFiles = Utils.CreateStringVector(Costanza.VIDEO_FILES);
			videoWidths = Utils.CreateStringVector(Costanza.VIDEO_WIDTHS);
			addEventListener(Root.DISPOSE, disposeScene);
			addObjects();
		}
		
		private function addObjects():void{			

			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.KEEP_AWAKE;
			
			soundManager = SoundManager.getInstance();
			sceneRoot = new starling.display.Sprite();
			addChild(sceneRoot);
			
			// try playing a video - we have to make stage3D invisible in order to see the StageVideo below
			Starling.current.stage3D.visible = false;
			//create video player
			stageVideoPlayer = new AIRMobileVideo(Starling.current.nativeStage, DonePlaying);			
			// Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED,true);			
			addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);			
			TweenMax.delayedCall(1, PlayNextVideo);			
			Starling.current.nativeStage.addEventListener(flash.events.Event.ACTIVATE, focusGained);
		}
		
		private function focusGained(e:flash.events.Event):void{		
			if(!Starling.current.stage3D.visible){
				Starling.current.start();
			}
		}		
		
		private function DonePlaying():void
		{
			//stageVideoPlayer.stopVideo();
			playing = false;
			playIndex++;
			
			if ( playIndex >= videoFiles.length )
			{
				stageVideoPlayer.destroy();
				stageVideoPlayer = null;
				
				Starling.current.stage3D.visible = true;
				dispatchEventWith(Root.LOAD_MENU, true);
			}
			else { TweenMax.delayedCall(1, PlayNextVideo); }
		}	
		
		private function PlayNextVideo():void
			{ stageVideoPlayer.playVideo( "videos/" + videoFiles[ playIndex ] + ".flv", videoWidths[ playIndex ] ); playing = true; }
			
		private function onAddedToStage(event:starling.events.Event):void
		{
			// Finished loading assets
			dispatchEventWith(Root.SCENE_LOADED,true);
		}
		
		private function disposeScene():void
		{
			NativeApplication.nativeApplication.systemIdleMode = SystemIdleMode.NORMAL;		
			sceneRoot.removeChildren(0,sceneRoot.numChildren-1,true);
		}
	}
}