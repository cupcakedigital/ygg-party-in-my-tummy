package utils
{
	public class MathHelpers
	{
		
		public function MathHelpers():void{
			
		}
		
		public static function DistanceTwoPoints(x1:Number, x2:Number,  y1:Number, y2:Number):
			Number {
			var dx:Number = x1-x2;
			var dy:Number = y1-y2;
			return Math.sqrt(dx * dx + dy * dy);
		}
		
		public static function PointDirection(x1:Number, y1:Number, x2:Number, y2:Number):Number 
		{
			var angle:Number = Math.atan2(y2 - y1, x2 - x1) * (180 / Math.PI);
			return angle < 0 ? angle + 360 : angle;
		}
		
		public static function randomIntRange(minNum:Number, maxNum:Number):Number
		{
			return (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum);
		}
		
		public static function randomNumberRange(start:Number, end:Number):Number
		{
			return start + (Math.random() * (end - start));
		}
	}
}