package utils
{
	import com.reyco1.physinjector.PhysInjector;
	import com.reyco1.physinjector.data.PhysicsObject;
	import com.reyco1.physinjector.data.PhysicsProperties;
	
	import flash.geom.Point;
	
	import Box2D.Common.Math.b2Vec2;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.extensions.SoundManager;
	
	import utils.ParticleSystemJP.ParticleEmitterJP;

	public class BubbleScreen extends Sprite
	{
		private var physics:PhysInjector;
		
		private var particle:ParticleEmitterJP;
		
		private var particlesArray:Array = new Array();
		
		private var floor:Quad;
		private var ceiling:Quad;
		private var leftWall:Quad;
		private var rightWall:Quad;
		
		private var soundManager:SoundManager;
		
		private var bubbCount:int = 1;
		private var bubblePositions:Array = [new Point(400,100),new Point(600,200),new Point(500,350),new Point(400,500),new Point(600,600),new Point(800,300)];
		
		private var bubbleArray:Array = [];
		private var bubblePhysArray:Array = [];
		
		public function BubbleScreen()
		{
			soundManager = SoundManager.getInstance();
			
			soundManager.addSound("bubble tone 1",Root.assets.getSound("bubble tone 1"));
			soundManager.addSound("bubble tone 2",Root.assets.getSound("bubble tone 2"));
			soundManager.addSound("bubble tone 3",Root.assets.getSound("bubble tone 3"));
			soundManager.addSound("bubble tone 4",Root.assets.getSound("bubble tone 4"));
			soundManager.addSound("bubble tone 5",Root.assets.getSound("bubble tone 5"));
			soundManager.addSound("bubble tone 6",Root.assets.getSound("bubble tone 6"));
			soundManager.addSound("bubble tone 7",Root.assets.getSound("bubble tone 7"));
			soundManager.addSound("bubble tone 8",Root.assets.getSound("bubble tone 8"));
			
			PhysInjector.STARLING = true;
			var bg:Image = new Image(Root.assets.getTexture("Bubbles_BG"));
			this.addChild(bg);
			
			particle = new ParticleEmitterJP(new Array(),false);
			this.addChild(particle);
			particle.touchable = false;
		}
		
		public function start():void
		{
			physics = new PhysInjector(Starling.current.nativeStage,new b2Vec2(0,0),false);
			
			//----------
			//MAKE WALLS
			//----------
			floor = new Quad(Costanza.STAGE_WIDTH, 50,0xffff00);
			floor.y = Costanza.STAGE_HEIGHT;
			this.addChild(floor);
			var floorObject:PhysicsObject = physics.injectPhysics(floor, PhysInjector.SQUARE, new PhysicsProperties({isDynamic:false,linearDamping:0, friction:0, restitution:1}));
			
			ceiling = new Quad(Costanza.STAGE_WIDTH, 50,0xff0000);
			ceiling.y = -ceiling.height;
			this.addChild(ceiling);
			var ceilingObject:PhysicsObject = physics.injectPhysics(ceiling, PhysInjector.SQUARE, new PhysicsProperties({isDynamic:false,linearDamping:0, friction:0, restitution:1}));
			
			leftWall = new Quad(50, Costanza.STAGE_HEIGHT,0xff00ff);
			leftWall.x -= leftWall.width;
			this.addChild(leftWall);
			var leftWallObject:PhysicsObject = physics.injectPhysics(leftWall, PhysInjector.SQUARE, new PhysicsProperties({isDynamic:false,linearDamping:0, friction:0, restitution:1}));
			
			rightWall = new Quad(50, Costanza.STAGE_HEIGHT,0xffffff);
			rightWall.x = Costanza.STAGE_WIDTH;
			this.addChild(rightWall);
			var rightWallObject:PhysicsObject = physics.injectPhysics(rightWall, PhysInjector.SQUARE, new PhysicsProperties({isDynamic:false,linearDamping:0, friction:0, restitution:1}));
			leftWall.alpha = rightWall.alpha = floor.alpha = ceiling.alpha = 0;
			
			
			for(var i:int = 0; i < 6; i++)
			{
				var bubble:MovieClip = new MovieClip(Root.assets.getTextures("Bubble_"));
				Starling.juggler.add(bubble);
				bubble.stop();
				bubble.currentFrame = 0;
				
				bubble.x = bubblePositions[i].x;
				bubble.y = bubblePositions[i].y;
				
				bubble.fps = 15;
				bubble.pivotX = bubble.width/2;
				bubble.pivotY = bubble.height/2;
				bubble.addEventListener(TouchEvent.TOUCH, tapBubble);
				bubble.addEventListener(Event.ENTER_FRAME, clearBubble);
				
				bubbleArray.push(bubble);
				this.addChild(bubble);
				
				var bubblePhys:PhysicsObject = physics.injectPhysics(bubble,PhysInjector.CIRCLE, new PhysicsProperties({isDynamic:true,friction:.2,restitution:.5,linearVelocity:new b2Vec2(MathHelpers.randomIntRange(-3,3),MathHelpers.randomIntRange(-3,3))}));
				bubblePhysArray.push(bubblePhys);
			}
			
			bubblePhysArray.push(leftWall,rightWallObject,floorObject,ceilingObject);
			
			addEventListener(Event.ENTER_FRAME, onUpdate);
		}
		
		private function clearBubble(e:Event):void
		{
			if((e.currentTarget as MovieClip).currentFrame == 5)
			{
				var i:int = int((e.currentTarget as MovieClip).name);	
				(e.currentTarget as MovieClip).removeEventListener(Event.COMPLETE,clearBubble);
				(e.currentTarget as MovieClip).stop();
				(e.currentTarget as MovieClip).visible = (e.currentTarget as MovieClip).touchable = false;
			}
		}
		
		private function tapBubble(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.BEGAN);
			
			if(touches.length != 1){return;}
			
			var touch:Touch = touches[0];
			if(touch == null){return;}
			var mc:MovieClip = e.currentTarget as MovieClip;
			var index:int = int(mc.name);
			
			if(touch.phase == TouchPhase.BEGAN)
			{
				bubbCount++;
				if(bubbCount > 8){bubbCount = 1;}
				soundManager.playSound("bubble tone " + bubbCount);
				particle.x = mc.x;
				particle.y = mc.y;
				
				if(particlesArray.length > 0){particlesArray.splice(0,1);}
				particlesArray.push(new Image(Root.assets.getTexture("ygg-confetti_000" + MathHelpers.randomIntRange(1,21))));
				particle.setArray(particlesArray);
				
				particle.CreateBurst(5,300,300,.3,true,true);
				
				//physics.removePhysics((e.currentTarget as MovieClip),false);
				mc.play();
				mc.removeEventListener(TouchEvent.TOUCH,tapBubble);
			}
		}
		
		protected function onUpdate(event:Event):void
		{
			physics.update();
		}
		
		public function clearScreen():void
		{
			removeEventListener(Event.ENTER_FRAME, onUpdate);
			
			//Starling.juggler.purge();
			for(var i:int = 0; i < bubbleArray.length; i++)
			{
				Starling.juggler.remove(bubbleArray[i]);
				physics.removePhysics(bubbleArray[i],true);
			}
			physics.removePhysics(floor,true);
			physics.removePhysics(ceiling,true);
			physics.removePhysics(rightWall,true);
			physics.removePhysics(leftWall,true);
			
			for(var j:int = 0; j < bubblePhysArray.length; j++)
			{
				//(bubblePhysArray[j] as PhysicsObject).physicsProperties.linearVelocity = null;
				if((bubblePhysArray[j] as PhysicsObject) != null)
				{
					(bubblePhysArray[j] as PhysicsObject).dispose();
				}
			}
			
			bubbleArray = [];
			bubblePhysArray = [];
			
			this.removeChildren(0,this.numChildren-1,true);
			
			physics.dispose();
			physics = null;
			this.parent.removeChild(this,true);
		}
	}
}