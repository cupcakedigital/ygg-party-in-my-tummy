package
{
    import com.cupcake.DeviceInfo;
    import com.greensock.TimelineMax;
    import com.greensock.TweenMax;
    
    import flash.desktop.NativeApplication;
    import flash.display.BitmapData;
    import flash.events.Event;
    import flash.filesystem.File;
    import flash.system.System;
    
    import scenes.Lobby;
    import scenes.Menu;
    import scenes.Splash_Video;
    import scenes.Characters.CharacterRoom;
    import scenes.CleanUp.CleanUp;
    import scenes.ColoringBook.Coloring_Book_Scene;
    import scenes.FeedCharacter.FeedingRoom;
    import scenes.MakeFood.MakeFood;
    import scenes.Matching.MatchingRoom;
    import scenes.Video.Video;
    
    import starling.core.Starling;
    import starling.display.Button;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.events.Event;
    import starling.extensions.SoundManager;
    import starling.filters.WarpFilter;
    import starling.textures.Texture;
    import starling.utils.AssetManager;
    
    import utils.BubbleScreen;
    import utils.ProgressBar;

    /** The Root class is the topmost display object in your game. 
	 *  It is a general scene switcher and will contain any globally accessable items liek menus and transition screens.
     *  It listens for bubbled events from the scenes to trigger events.
	 *  Keep this class rather lightweight: it controls the high level behaviour of your game. */
    public class Root extends Sprite
    {
        private static var sAssets:AssetManager;
		public static var currentAssetsProxy:AssetManager;
		
        private var mSceneContainer:Sprite;
		private var mActiveScene:Sprite;
        private var soundManager:SoundManager;
        private var currentCharacter:String;
		private var currentFood:String;

		private var splashTimeline:TimelineMax;
		
		private var splashTexture:Image;
		private var splashWubbzy:Image;
		private var splashLicensorImage:Button;
		private var splashCupcakeImage:Button;
		
		private var transitionImage:Image;
		private var transitionTexture:Texture;
		private var tranData:BitmapData;
		private var tranWarpFilter:WarpFilter;
		
		public static const DISPOSE:String		= "dispose";
		public static const LOAD_LOBBY:String	= "loadLobby";
		public static const LOAD_MENU:String	= "loadMenuFirst";
		public static const SCENE_LOADED:String = "sceneLoaded";

		private var bubbleScreen:BubbleScreen;
		private var bgTexture:Texture;
		
		private var assetsLoaded:Boolean		= false;
		private var assetsLoading:Boolean		= false;
		
		private var progressBar:ProgressBar;
		
		private var appDir:File					= File.applicationDirectory;
        
        public function Root(charString:String = "")
        {
            addEventListener(Menu.START_GAME,		onStartGame);
			addEventListener(Lobby.SET_CHARACTER,	onSetCharacter);
			addEventListener(Lobby.LOAD_ROOM,		onLoadRoom);
			addEventListener(Lobby.LOAD_MENU, 		showMenu);
			addEventListener(LOAD_MENU,				onLoadMenuFirst);
			addEventListener(LOAD_LOBBY,			onLoadLobby);
			addEventListener(SCENE_LOADED,			onSceneLoaded);
			
            // not more to do here -- Startup will call "start" immediately.
        }
        
        public function start(background:Texture, assets:AssetManager):void
        {
			// Initialize UI Theme (Feathers,  need full tempalte for use)
			// new MetalWorksMobileTheme();
			
			//temporary until core fix. when app loses focus while loading assets it crashes
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.ACTIVATE,screenActivated);
			NativeApplication.nativeApplication.addEventListener(flash.events.Event.DEACTIVATE,screenDeactivated);
			
			// the asset manager is saved as a static variable; this allows us to easily access
            // all the assets from everywhere by simply calling "Root.assets"
            sAssets = assets; 
			// The sound manager is instanced here
			soundManager = SoundManager.getInstance();
            // The background is passed into this method for two reasons:
            // 
            // 1) we need it right away, otherwise we have an empty frame
            // 2) the Startup class can decide on the right image, depending on the device.
			splashWubbzy = new Image(background);
			
			
			addChild(splashWubbzy);
            // The AssetManager contains all the raw asset data, but has not created the textures
            // yet. This takes some time (the assets might be loaded from disk or even via the
            // network), during which we display a progress indicator. 	
            progressBar = new ProgressBar(175, 20);
            progressBar.x = (Costanza.STAGE_WIDTH/2)  - (progressBar.width/2);
            progressBar.y = Costanza.STAGE_HEIGHT * 0.85;
            addChild(progressBar);
            
			assetsLoading = true;
            assets.loadQueue(function onProgress(ratio:Number):void
            {
                progressBar.ratio = ratio;
                
                // a progress bar should always show the 100% for a while,
                // so we show the main menu only after a short delay. 
                if (ratio == 1){
					// Enque audio
                    Starling.juggler.delayCall(function():void
                    {
						assetsLoaded = true;
						progressBar.removeFromParent(true);
						assetsDoneLoading();
						
                    }, 0.15);
				};
            });
        }
		
		private function assetsDoneLoading():void
		{
			NativeApplication.nativeApplication.removeEventListener(flash.events.Event.ACTIVATE,screenActivated);
			NativeApplication.nativeApplication.removeEventListener(flash.events.Event.DEACTIVATE,screenDeactivated);
			
			trace("Loading Menu");
			splashLicensorImage = new Button(Root.assets.getTexture("dhx_splash"));
			splashLicensorImage.scaleWhenDown = 1;
			splashLicensorImage.alpha = 0;
			addChild(splashLicensorImage);
			
			/*splashCupcakeImage = new Button(Root.assets.getTexture("cupcakedigital_splash"));
			splashCupcakeImage.alpha = 0;
			splashCupcakeImage.scaleWhenDown = 1;
			addChild(splashCupcakeImage);*/
			
			//create the timeline and add an onComplete call to myFunction when the timeline completes
			splashTimeline = new TimelineMax({onComplete:loadMenu});
			
			//add a tween
			splashTimeline.append(new TweenMax(splashWubbzy, 0.5, {alpha:0,delay:0.5, onComplete:removeTextures, onCompleteParams:[splashWubbzy,true]}));
			//splashTimeline.append(new TweenMax(splashCupcakeImage, 0.5, {alpha:1}));
			//splashTimeline.append(new TweenMax(splashCupcakeImage, 0.5, {alpha:0,delay:1, onComplete:removeChild, onCompleteParams:[splashCupcakeImage,true]}));
			splashTimeline.append(new TweenMax(splashLicensorImage, 0.5, {alpha:1}));
			splashTimeline.append(new TweenMax(splashLicensorImage, 0.5, {alpha:0,delay:1, onComplete:removeChild, onCompleteParams:[splashLicensorImage,true]}));
			
			addEventListener(starling.events.Event.TRIGGERED, skipLoaders);
			
			function skipLoaders(evt:starling.events.Event):void{
				
				var target:Button = evt.currentTarget as Button;
				splashTimeline.timeScale += 0.5;
				
			}
		}
		
		private function screenActivated(e:flash.events.Event):void
		{
			trace("----------------------------SCENE ACTIVATED-----------------------------");
			if(!assetsLoaded && !assetsLoading)
			{
				assets.enqueue(
					appDir.resolvePath("textures/"+Costanza.SCALE_FACTOR +"x/promo"),
					appDir.resolvePath("textures/"+Costanza.SCALE_FACTOR +"x/Global"),
					appDir.resolvePath("textures/"+Costanza.SCALE_FACTOR +"x/Bubbles"),
					appDir.resolvePath("audio/music"),
					appDir.resolvePath("fonts")
				);
				assets.loadQueue(function onProgress(ratio:Number):void
				{
					if ( CONFIG::MARKET == "itunes" ) { Starling.current.nativeStage.autoOrients = true; }
					progressBar.ratio = ratio;
					
					// a progress bar should always show the 100% for a while,
					// so we show the main menu only after a short delay. 
					if (ratio == 1){
						// Enque audio
						Starling.juggler.delayCall(function():void
						{
							assetsLoaded = true;
							progressBar.removeFromParent(true);
							assetsDoneLoading();
							
						}, 0.15);
					};
				});
			}
		}
		
		private function screenDeactivated(e:flash.events.Event):void
		{
			trace("----------------------------SCENE DEACTIVATED-----------------------------");
			if( !assetsLoaded ) { assets.purgeQueue(); assetsLoading = false; }
		}
		
		private function removeTextures(img:Image,tf:Boolean):void { removeChild(img,tf); }
		
		private function loadMenu():void{
			
			mSceneContainer = new Sprite();
			addChild(mSceneContainer);
			
			transitionImage = new Image(Root.assets.getTexture("yogabba_splash"));
			transitionImage.alpha = 0;
			transitionImage.touchable = false;
			addChild(transitionImage);
			
			if ( DeviceInfo.platform == DeviceInfo.PLATFORM_DESKTOP )
				{ showScene(Menu,"",false); }
			else { showSceneStatic(Splash_Video); }
			
			// now would be a good time for a clean-up 
			System.pauseForGCIfCollectionImminent(0);
			System.gc();
		}
		
		
		public function transitionScene(bubbles:Boolean = false):void{
			
			if(bubbles)
			{
				bubbleScreen = new BubbleScreen();
				bubbleScreen.alpha = 0;
				addChild(bubbleScreen);
				
				bubbleScreen.x = 0;
				bubbleScreen.y = 0;
				bubbleScreen.touchable = true;
				
				TweenMax.to(bubbleScreen,0.15,{alpha:1});
				bubbleScreen.start();
			}
			else
			{
				transitionImage.x = 0;
				transitionImage.y = 0;
				transitionImage.alpha = 0;
				transitionImage.touchable = true;
				
				TweenMax.to(transitionImage,0.15,{alpha:1});
			}
			
		}
		
		private function onSceneLoaded(event:starling.events.Event):void{
			
			trace("SCENE LOADED");
			
			if(transitionImage.alpha > 0){TweenMax.to(transitionImage,0.2,{alpha:0, delay:0.4, onComplete:hideTransition});}
			else if(bubbleScreen && bubbleScreen.alpha > 0){TweenMax.to(bubbleScreen,0.2,{alpha:0, delay:0.4, onComplete:hideTransition, onCompleteParams:[true]});}
			function hideTransition(bubb:Boolean=false):void{
				transitionImage.touchable = false;
				if(bubb && bubbleScreen){bubbleScreen.touchable = false; bubbleScreen.clearScreen(); bubbleScreen = null;}
			}
		}
		
		
		
		private function onSetCharacter(event:starling.events.Event, obj:Object):void
		{
			trace(obj.charString);
			currentCharacter = obj.charString;
			currentFood = obj.foodString;
			trace("Current Character Now " + currentCharacter);
		};
		
        private function onLoadRoom(event:starling.events.Event, room:int = 0):void
        {
            trace("Switching Scenes");
			
			switch(room)
			{
				case 0://character room
				{
					showScene(CharacterRoom, currentCharacter, true, currentFood, true);
					break;
				}
				case 1://food making room
				{
					showScene(MakeFood, currentCharacter, true, currentFood, true);
					break;
				}
				case 2://feeding room
				{
					showScene(FeedingRoom, currentCharacter, true, currentFood, true);
					break;
				}
				case 3://matching room
				{
					showScene(MatchingRoom, currentCharacter, true, currentFood, true);
					break;
				}
				case 4://end cleanup room
				{
					showScene(CleanUp, currentCharacter, true, currentFood, true);
					break;
				}
				case 5://coloring
				{
					showScene(Coloring_Book_Scene,"",true,"",false);
					break;
				}
				case 6:
				{
					showScene(Video,"",true,"",false);
					break;
				}
			}
        }
        
        private function onStartGame(event:starling.events.Event, gameMode:String):void
        {
            trace("Game starts! Mode: " + gameMode);
            showScene(Lobby);
        }
        
		private function onLoadLobby(event:starling.events.Event):void
		{
			trace("Loading Main Lobby");
			showScene(Lobby);
		}
		
		private function onLoadMenuFirst(event:starling.events.Event):void
		{
			trace("Loading Main Lobby");
			showScene(Menu,"",false);
		}
		
		private function showMenu(event:starling.events.Event):void
		{
			trace("Loading Main Lobby");
			showScene(Menu);
		}
		
        public function showScene(screen:Class, char:String = "", transition:Boolean = true, food:String = "",bubbleTransition:Boolean = false):void
        {
			trace(char + food);
			if(transition){
				transitionScene(bubbleTransition);
			};
			
			Starling.juggler.delayCall(function():void
			{
				if (mActiveScene){
					mActiveScene.broadcastEventWith(DISPOSE);
					mActiveScene.removeFromParent(true)
				};
				mActiveScene = new screen(char,food);
				mSceneContainer.addChild(mActiveScene);
			},0.2);
			
        }
		
		public function showSceneStatic(screen:Class):void
		{
			Starling.juggler.delayCall(function():void
			{
				if (mActiveScene){
					mActiveScene.broadcastEventWith(DISPOSE);
					mActiveScene.removeFromParent(true)
				};
				mActiveScene = new screen();
				mSceneContainer.addChild(mActiveScene);
			},0.2);
		}

        public static function get assets():AssetManager { return sAssets; }
    }
}